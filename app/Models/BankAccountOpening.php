<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BankAccountOpening extends Model {

    protected $table = 'bank_account_opening';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

   
    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function pagesearch( $Nextpage = 0, $search = NULL) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $limit = " LIMIT " . $Nextpage . " , " . '20 ;';
        $query = "select $this->table.*, users.email from $this->table JOIN users ON users.id = $this->table.user_id ";
        $results = DB::select($query);
        if (!empty($search)) {
            foreach ($search as $value) {
                //do something with your $key and $value;
                $query.=" and $this->table." . $value;
            }
        }

        return DB::select($query . $limit);
    }

}
