<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class KYC extends Model {

    protected $table = 'kyc';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

   

    public function pagesearch( $Nextpage = 0) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $userdata = KYC::limit(20)
                ->offset($Nextpage)
                ->get();
        return $userdata;
    }

}
