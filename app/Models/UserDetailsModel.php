<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserDetailsModel extends Model {

    protected $table = 'users_details';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

}
