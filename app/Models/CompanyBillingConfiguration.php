<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyBillingConfiguration extends Model {

    protected $table = 'company_billing_configuration';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;
    
    public function getallbilling($Nextpage = 0) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $billingdata = CompanyBillingConfiguration::limit(20)
                ->offset($Nextpage)
                ->get();
        return $billingdata;
    }

    public function allbillingcount() {
        $billingcount = CompanyBillingConfiguration::all()
                ->count();
//        die('com'.$companycount);
        return $billingcount;
    }

    public  function company()
    {
        return $this->hasOne('App\Models\CompanyModel','company_id','company_id');
    }

    public function hourlyrate($company_id, $current_date){
        $query = CompanyBillingConfiguration::select("hourly_rate")
                ->where("cycle_start_date", "<=", $current_date)
                ->where("company_id", $company_id)
                ->orderBy("id", "desc")
                ->take(1)
                ->get();
        return $query[0]['hourly_rate'];
    }

}
