<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyBankStatements extends Model {

    protected $table = 'company_bank_statements';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

    public function company() {
        return $this->hasOne('App\Models\CompanyModel', 'company_id', 'company_id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'uploaded_by');
    }

    public function pagesearch($company = 0, $Nextpage = O, $search = NULL) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $limit= " LIMIT " . $Nextpage . " , " . '20 ;';
        $query = "select * from company_bank_statements JOIN users ON users.id = company_bank_statements.uploaded_by where company_id=" . $company ;
        $results = DB::select($query);
        if (!empty($search)) {
            foreach ($search as $value) {
                //do something with your $key and $value;
                 $query.=" and ".$value;
            }
        }
      
        return DB::select($query.$limit);
    }

}
