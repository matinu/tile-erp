<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyContactPersonModel extends Model {

    protected $table = 'company_contact_person';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

    public function pagesearch($company = 0, $Nextpage = 0, $search = NULL) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $limit = " LIMIT " . $Nextpage . " , " . '20 ;';
        $query = "select * from $this->table where company_id=" . $company;
        $results = DB::select($query);
        if (!empty($search)) {
            foreach ($search as $value) {
                //do something with your $key and $value;
                $query.=" and " . $value;
            }
        }

        return DB::select($query . $limit);
    }

}
