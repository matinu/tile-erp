<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Country extends Model {

    protected $table = 'country';
    protected $primaryKey = 'country_id';
    public $timestamps = FALSE;



}
