<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LoginModel extends Model {

    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

    public function checkuservalidation($input) {
        $user = LoginModel::join("users_details", "users.id", "=", "users_details.users_id")
                ->where('users.email', '=', $input['email'])
                ->where('users.password', '=', md5($input['password']))
                ->get();
        return $user;
    }

    public function adduser($input) {
//        echo '<pre>';
//        print_r($input);
//        $user = new LoginModel();
//        $user->name = $input['name'];
//        $user->email = $input['email'];
//        $user->password = md5($input['password']);
//        $user->created_at = Carbon::now();
//        $user->updated_at = Carbon::now();
//        if ($user->save()) {
//            $userdata = $user;
//            echo '<pre>';
//            print_r($userdata);
//        }

        $usersdata['name'] = $input['name'];
        $usersdata['email'] = $input['email'];
        $usersdata['password'] = md5($input['password']);
        $usersdata['created_at'] = Carbon::now();
        $usersdata['updated_at'] = Carbon::now();
        $id = DB::table('users')->insertGetId($usersdata);

        $detailsdata['users_id'] = $id;
        $detailsdata['cnumber'] = $input['contact'];
        $detailsdata['dob'] = $input['dob'];
        $detailsid = DB::table('users_details')->insertGetId($detailsdata);
        return $detailsid;
    }

    public function validateemail($userdetailsid) {
        $user = UserDetailsModel :: find($userdetailsid);
        $user->status = 1;
        $user->save();
    }

}
