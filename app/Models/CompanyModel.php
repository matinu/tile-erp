<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyModel extends Model {

    protected $table = 'company';
    protected $primaryKey = 'company_id';
    public $timestamps = FALSE;

    public function getallcompany($Nextpage = 0) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $companydata = CompanyModel::limit(20)
                ->offset($Nextpage)
                ->get();
        return $companydata;
    }

    public function allcompanycount() {
        $companycount = CompanyModel::all()
                ->count();
//        die('com'.$companycount);
        return $companycount;
    }

    public function contactPersons() {
        return $this->hasMany('App\Models\CompanyContactPersonModel', 'company_id', 'company_id');
    }

    public function documents() {
        return $this->hasMany('App\Models\CompanyDocumentModel', 'company_id', 'company_id');
    }

    public function templates() {
        return $this->hasMany('App\Models\CompanyTemplate', 'company_id', 'company_id');
    }

    public function statements() {
        return $this->hasMany('App\Models\CompanyBankStatements', 'company_id', 'company_id');
    }

    public function compliance() {
        return $this->hasMany('App\Models\CompanyComplianceReportModel', 'company_id', 'company_id');
    }

    public function incorporation() {
        return $this->hasMany('App\Models\CompanyIncorporationModel', 'company_id', 'company_id');
    }

    public function correspondences() {
        return $this->hasMany('App\Models\CompanyCorrespondenceModel', 'company_id', 'company_id');
    }

    public function invoices() {
        return $this->hasMany('App\Models\CompanyCreditInvoice', 'company_id', 'company_id');
    }

    public function taxreturn() {
        return $this->hasMany('App\Models\CompanyTaxReturn', 'company_id', 'company_id');
    }

    public function financialstatements() {
        return $this->hasMany('App\Models\CompanyFinancialStatements', 'company_id', 'company_id');
    }

}
