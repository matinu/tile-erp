<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserModel extends Model {

    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

    public function getalluserdata($Nextpage = 0) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $userdata = UserModel::join("users_details", "users.id", "=", "users_details.users_id")
                ->limit(20)
                ->offset($Nextpage)
                ->get();
        return $userdata;
    }

    public function allusercount() {
        $usercount = UserModel::join("users_details", "users.id", "=", "users_details.users_id")
                ->count();
        return $usercount;
    }

    public function getuserbyid($userid) {
        $userdata = UserModel::join("users_details", "users.id", "=", "users_details.users_id")
                ->where("users.id", "=", $userid)
                ->get();
        return $userdata;
    }

    public function deleteuser($userid) {
        UserModel::where('id', '=', $userid)->delete();
        UserDetailsModel::where('users_id', '=', $userid)->delete();
    }

    public function edituser($inputs) {
        $user = UserModel :: find($inputs['id']);
        $user->name = $inputs['name'];
        $user->save();
//die;
//        $userdetails = UserDetailsModel :: where('user_id', '=', $inputs['id'])->get();
//        $userdetails->cnumber = $inputs['contact'];
//        $userdetails->dob = $inputs['dob'];
//        $userdetails->save();
//        $user = UserModel :: find($inputs['id']);
//        $user->name = $inputs['name'];
//        $user->save();

        $userdetails = UserDetailsModel :: where('users_id', '=', $inputs['id'])->get();
        $userdetails[0]->cnumber = $inputs['contact'];
        $userdetails[0]->dob = $inputs['dob'];
        $userdetails[0]->save();
    }

}
