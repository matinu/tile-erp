<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyCreditInvoice extends Model {

    protected $table = 'company_credit_invoice';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

    public function company() {
        return $this->hasOne('App\Models\CompanyModel', 'company_id', 'company_id');
    }

    public function getinvoicedetails($company_id) {
        return CompanyCreditInvoice::where("company_id", $company_id)
                        ->where("invoice_status", "pending")
                        ->get();
    }

    public function getinvoicedetailsById($id) {
        return CompanyCreditInvoice::find($id);
    }

    public function changeinvoicestatus($id) {
        $data = CompanyCreditInvoice::find($id);
        $data['invoice_status'] = 'processing';
        $data->save();
    }

    public function pagesearch($company = 0, $Nextpage = 0, $search = NULL) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $limit = " LIMIT " . $Nextpage . " , " . '20 ;';
        $query = "select * from $this->table where company_id=" . $company;
        $results = DB::select($query);
        if (!empty($search)) {
            foreach ($search as $value) {
                //do something with your $key and $value;
                $query.=" and " . $value;
            }
        }

        return DB::select($query . $limit);
    }

}
