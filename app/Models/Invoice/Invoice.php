<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Invoice extends Model {

    //
    protected $table = 'invoice';
    protected $primaryKey = 'invoice_id';

    
    public static function getInvoiceDetails($id = NULL) {
        if ($id == Null) {
            return NULL;
        }
        return DB::table('invoice')->where('invoice_id', $id)->first();
    }

    public function getInvoiceByProjectId($id) {
        $invoices = Invoice::where('project_id', $id)
                ->get();
        $paid = 0;
        $paymentMod = new Payment();
        foreach ($invoices as $inv):
            $paid = $paid + $paymentMod->getPaymentByInvoiceId($inv['invoice_id']);
        endforeach;

        $discount = Invoice::where('project_id', $id)->sum('discount');
        return $paid + $discount;
    }
    
    public function getTotalInvoiceProject($id){
        $paid = Invoice::where('project_id', $id)->sum('invoice_total');
        $discount = Invoice::where('project_id', $id)->sum('discount');
        return $paid + $discount;
    }

}
