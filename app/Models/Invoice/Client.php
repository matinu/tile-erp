<?php namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class Client extends Model{
    protected $table = 'clients';
    protected $primaryKey = 'client_id';

    public function clientContact(){
        return $this->hasMany('App\Models\ClientContact','client');
    }
}