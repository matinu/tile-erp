<?php

/**
 * Created by PhpStorm.
 * User: absiddique
 * Date: 3/5/2015
 * Time: 4:37 PM
 */

namespace App\Models\Invoice;

use Illuminate\Support\Facades\DB;

class Custom {

    public static function getClientInfo($byId = null, $workStatus = null) {
        if ($byId != null) {
            $data = DB::table('company')
                    ->join('company_contact_person', 'company.company_id', '=', 'company_contact_person.company_id')
                    ->where('company.company_id', '=', $byId)
                    ->get();

            if ($data != null) {
                return $data[0];
            }
        }
        return $data;
    }

    public static function getInvoiceInfo($invoice_number = null) {
        if ($invoice_number != null) {
            
        }
        return null;
    }

    public static function getPaymentStatus($invoiceID = null) {
        if ($invoiceID != null) {
            $data = DB::table('payments')
                    ->where('invoice_id', '=', $invoiceID)
                    ->sum('amount');

            if ($data != null) {
                return $data;
            }
        }
        return null;
    }

    public static function getInvoiceList($clientID = null, $id = null, $date = null) {
        $paymentList = Payment::select(DB::raw('sum(amount) AS amount, invoice_id, method, payment_date, payment_id'))->groupBy('invoice_id')->toSql();

        return $data = DB::table('invoice')
                ->join('company', 'company.company_id', '=', 'invoice.client')
                ->leftJoin(DB::Raw(("($paymentList) AS payments")), 'payments.invoice_id', '=', 'invoice.invoice_id')
                ->orderBy('invoice_number', 'desc')
                ->where(function($query) use ($id) {
                    if ($id != null) {
                        $query->where('invoice.invoice_number', '=', $id);
                    }
                })
                ->where(function($query) use ($clientID) {
                    if ($clientID != null) {
                        $query->where('invoice.client', '=', $clientID);
                    }
                })
                ->where(function($query) use ($date) {
                    if ($date != null) {
                        $query->where('payments.payment_date', '=', $date);
                    }
                })
                ->select('invoice.*', 'company.company_name', 'payments.*')
                ->get();
    }

    public static function getEmployeeDetails($id = null) {
        return $data = DB::table('employee_info')
                ->join('employee_personal_info')
                ->join('employee_emergency_contact')
                ->join('employee_bank_info')
                ->get();
    }

}
