<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Payment extends Model {

    protected $table = 'payments';
    protected $primaryKey = 'payment_id';

    public static function getTotalAmount($id = NULL) {
        if ($id == Null) {
            return NULL;
        }
        return DB::table('payments')
                        ->from('payments')
                        ->where('payments.invoice_id', $id)
                        ->join('invoice', 'payments.invoice_id', '=', 'invoice.invoice_id')
                        ->sum('amount');
    }

    public static function insertPayment() {
        $id = DB::table('users')->insertGetId(
                ['email' => 'john@example.com', 'votes' => 0]
        );
    }

    public static function getPaymentbyid($id) {
        return DB::table('payments')
                        ->join('invoice', 'payments.invoice_id', '=', 'invoice.invoice_id')
                        ->select('payments.*', 'invoice.invoice_number', 'invoice.invoice_total')
                        ->where('payments.payment_id', $id)
                        ->get();
    }

    public static function getPayment($input = null) {
        if ($input == null || ($input['organizationName'] == null && $input['date'] == null && $input['invoiceId'] == null)) {
            return DB::table('payments')
                            ->join('invoice', 'payments.invoice_id', '=', 'invoice.invoice_id')
                            ->select('payments.*', 'invoice.invoice_number', 'invoice.invoice_id', 'invoice.invoice_total', 'invoice.discount')
                            ->get();
        } else {
            return DB::table('payments')
                            ->join('invoice', 'payments.invoice_id', '=', 'invoice.invoice_id')
                            ->where(function ($query) use ($input) {
                                if ($input['organizationName']) {
                                    $query->where('invoice.client', '=', $input['organizationName']);
                                }

                                if ($input['date']) {
                                    $query->where('payments.payment_date', '=', $input['date']);
                                }
                                if ($input['invoiceId']) {
                                    $query->where('invoice.invoice_number', '=', $input['invoiceId']);
                                }
                            })
                            ->select('payments.*', 'invoice.invoice_number', 'invoice.invoice_total', 'invoice.discount')
                            ->get();
        }
    }

    public static function getUnpaidInvoice() {
        return DB::select("SELECT * from invoice i where i.invoice_id not in (SELECT i.invoice_id from payments p, invoice i where p.invoice_id=i.invoice_id group by p.invoice_id,i.invoice_id having sum(p.amount) >= i.invoice_total)");
    }

    public static function getUnpaidInvoicebycompany($id = 0) {
        if ($id == 0)
            return NULL;
        return DB::select("SELECT invoice_id,invoice_number from invoice i where i.invoice_id not in (SELECT i.invoice_id from payments p, invoice i where p.invoice_id=i.invoice_id group by p.invoice_id,i.invoice_id having sum(p.amount) >= i.invoice_total) and client='" . $id . "' ORDER BY invoice_number DESC") ;
    }

    public function getPaymentByInvoiceId($id) {
        return Payment::where('invoice_id', $id)
                ->sum('amount');
    }

}
