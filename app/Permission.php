<?php

namespace App;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission {

    public function getallpermissions($Nextpage = 0) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $permissiondata = Role::from('permissions')
                ->limit(20)
                ->offset($Nextpage)
                ->get();
        return $permissiondata;
    }

    public function allpermissioncount() {
        $permissioncount = Role::from('permissions')
                ->count();
        return $permissioncount;
    }

}
