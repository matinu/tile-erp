<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

    public function getallroles($Nextpage = 0) {
        if ($Nextpage != 0) {
            $Nextpage = ($Nextpage - 1) * 20;
        }
        $roledata = Role::from('roles')
                ->limit(20)
                ->offset($Nextpage)
                ->get();
        return $roledata;
    }
    
    public function allrolecount() {
        $rolecount = Role::from('roles')
                ->count();
        return $rolecount;
    }

}
