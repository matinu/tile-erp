<?php

namespace App;

use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends \Illuminate\Database\Eloquent\Model {

    use EntrustUserTrait; // add this trait to your user model
    
}
