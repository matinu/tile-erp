<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', 'WelcomeController@index');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
//Login and user
Route::any('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::get('profile', 'WelcomeController@profile');
Route::get('dashboard', 'WelcomeController@dashboard');
Route::any('create-user', 'Admin\UserController@create_user');
Route::get('edit-user/{userid}', 'Admin\UserController@edit_user');
Route::post('edit-user', 'Admin\UserController@edit_user');
Route::any('list-user/{page}', 'Admin\UserController@manage_user');
Route::get('delete-user/{userid}', 'Admin\UserController@delete_user');
Route::get('ban-user/{userdetailsid}', 'Admin\UserController@ban_user');
Route::get('unban-user/{userdetailsid}', 'Admin\UserController@unban_user');
Route::get('validateemail/{userdetailsid}', 'Admin\UserController@validateemail');



//Role
Route::any('create-role', 'Admin\RoleController@Create_Role');
Route::any('list-role/{page}', 'Admin\RoleController@index');
Route::any('edit-role/{id}', 'Admin\RoleController@edit_Role');
Route::any('delete-role/{id}', 'Admin\RoleController@delete_Role');
Route::any('set-role', 'Admin\RoleController@set_Role');
Route::any('view-user-role', 'Admin\RoleController@view_UserRole');
Route::any('edit-user-role/{id}', 'Admin\RoleController@edit_UserRole');
Route::any('delete-user-role/{id}', 'Admin\RoleController@delete_UserRole');

Route::any('set-permission', 'Admin\PermissionController@set_RolePermission');
Route::any('view-role-permission', 'Admin\PermissionController@view_RolePermission');
Route::any('edit-role-permission/{id}', 'Admin\PermissionController@edit_RolePermission');
Route::any('delete-role-permission/{id}', 'Admin\PermissionController@delete_RolePermission');
Route::any('list-permission/{page}', 'Admin\PermissionController@index');
Route::any('edit-permission/{id}', 'Admin\PermissionController@edit_Permission');
Route::any('delete-permission/{id}', 'Admin\PermissionController@delete_Permission');

///invoice and payment

Route::get('invoice-add', 'Invoice\InvoiceController@add');
Route::get('invoice-list/{page}', 'Invoice\InvoiceController@invoiceList');
Route::get('invoice-list/{page}/{id}', 'Invoice\InvoiceController@invoiceList');
Route::post('create-invoice', 'Invoice\InvoiceController@invoiceInsert');
Route::get('invoice-edit/{id}', 'Invoice\InvoiceController@invoiceEdit');
Route::post('invoice-item-delete', 'Invoice\InvoiceController@deleteInvoiceItemById');
Route::post('invoice-update', 'Invoice\InvoiceController@invoiceUpdate');
Route::get('invoice-delete/{id}', 'Invoice\InvoiceController@invoiceDelete');
Route::get('printInvoice/{id}.pdf', 'Invoice\InvoiceController@invoiceFile');

//Route::get('payment', 'Invoice\PaymentController@getListToAddPayment');
Route::get('payment/{id}', 'Invoice\PaymentController@getPaymentDetails');
Route::post('payment-add', 'Invoice\PaymentController@add');
Route::get('payment-add', 'Invoice\PaymentController@paymentList'); ///same
Route::get('payment-add/{id}', 'Invoice\PaymentController@add');
Route::get('payment-list', 'Invoice\PaymentController@paymentList'); ///same
Route::post('payment-list', 'Invoice\PaymentController@searchPayment');
Route::get('payment-edit/{id}/', 'Invoice\PaymentController@paymentEdit');
Route::get('payment-delete/{id}/', 'Invoice\PaymentController@paymentDelete');
Route::post('payment-entry/{id}/', 'Invoice\PaymentController@paymentEntry');
Route::get('payment-entry/{id}/', 'Invoice\PaymentController@paymentList');
Route::any('payment-edit', 'Invoice\PaymentController@updatePayment');
Route::get('invoiceList/{id}', 'Invoice\PaymentController@invoiceList');

//company
Route::get('create-company', 'Admin\CompanyController@create');
Route::Post('save-company', 'Admin\CompanyController@save');
Route::get('list-company/{page}', 'Admin\CompanyController@listCompany');
Route::get('details-company/{id}', 'Admin\CompanyController@details');
Route::Post('add-company-contact-person', 'Admin\CompanyController@add_contact_person');
Route::Post('add-document/{id}', 'Admin\CompanyController@add_document');
Route::Post('add-correspondence/{id}', 'Admin\CompanyController@add_correspondence');
Route::get('delete-company/{id}', 'Admin\CompanyController@delete');
Route::get('delete-correspondence/{id}', 'Admin\CompanyController@deleteCorrespondence');
Route::get('delete-person/{id}', 'Admin\CompanyController@deletePerson');
Route::get('delete-document/{id}', 'Admin\CompanyController@deleteDocument');
Route::get('delete-credit-invoice/{id}', 'Admin\CompanyController@deleteInvoice');
Route::get('edit-company/{id}', 'Admin\CompanyController@edit');
Route::Post('update-company/{id}', 'Admin\CompanyController@update');
Route::Post('add-company-credit-invoice', 'Admin\CompanyController@add_credit_invoice');
Route::Post('billing-info-details', 'Admin\CompanyController@billing_info_details');
Route::Post('billing-pdf', 'Admin\CompanyController@billing_pdf');
Route::get('edit-credit-invoice/{id}', 'Admin\CompanyController@editInvoice');
Route::Post('update-credit-invoice/{id}', 'Admin\CompanyController@updateInvoice');
Route::Post('add-tamplate/{id}', 'Admin\CompanyController@addTemplate');
Route::get('delete-template/{id}', 'Admin\CompanyController@deleteTemplate');
Route::Post('add-statement/{id}', 'Admin\CompanyController@addStatement');
Route::get('delete-statement/{id}', 'Admin\CompanyController@deleteStatement');
Route::Post('add-incorporation/{id}', 'Admin\CompanyController@addIncorporation');
Route::get('delete-incorporation/{id}', 'Admin\CompanyController@deleteIncorporation');
Route::Post('add-compliance/{id}', 'Admin\CompanyController@addCompliance');
Route::get('delete-compliance/{id}', 'Admin\CompanyController@deleteCompliance');
Route::Post('add-tax-return/{id}', 'Admin\CompanyController@addTaxReturn');
Route::get('delete-tax-return/{id}', 'Admin\CompanyController@deleteTaxreturn');
Route::Post('add-financial-statements/{id}', 'Admin\CompanyController@addFinancialStatements');
Route::get('delete-financial-statements/{id}', 'Admin\CompanyController@deleteFinancialStatements');

// billing
Route::get('create-billing', 'Admin\CompanyBillingController@create');
Route::Post('save-billing', 'Admin\CompanyBillingController@save');
Route::get('list-billing/{page}', 'Admin\CompanyBillingController@listBilling');
Route::get('delete-billing/{id}', 'Admin\CompanyBillingController@delete');
Route::get('edit-billing/{id}', 'Admin\CompanyBillingController@edit');
Route::Post('update-billing/{id}', 'Admin\CompanyBillingController@update');


//company views
Route::any('correspondance-view/{id}/{page}', 'Admin\CompanyDetailsController@correspondance_view');
Route::any('document-view/{id}/{page}', 'Admin\CompanyDetailsController@document_view');
Route::any('contact-person-view/{id}/{page}', 'Admin\CompanyDetailsController@contact_person_view');
Route::any('invoices-view/{id}/{page}', 'Admin\CompanyDetailsController@invoices_view');
Route::any('templates-view/{id}/{page}', 'Admin\CompanyDetailsController@templates_view');
Route::any('statements-view/{id}/{page}', 'Admin\CompanyDetailsController@statements_view');
Route::any('incorporation-view/{id}/{page}', 'Admin\CompanyDetailsController@incorporation_view');
Route::any('compliance-view/{id}/{page}', 'Admin\CompanyDetailsController@compliance_view');
Route::any('tax-return-view/{id}/{page}', 'Admin\CompanyDetailsController@tax_return_view');
Route::any('financial-statements-view/{id}/{page}', 'Admin\CompanyDetailsController@financial_statements_view');

//bank Account Open

Route::any('bank-account-open', 'Admin\BankAccountController@create');
Route::any('list-bank-account/{id}', 'Admin\BankAccountController@index');
Route::any('delete-bank-account/{id}', 'Admin\BankAccountController@delete');

//banl account
Route::any('create-kyc', 'Admin\KYCController@create');
Route::any('list-kyc/{id}', 'Admin\KYCController@index');
Route::any('delete-kyc/{id}', 'Admin\KYCController@delete');