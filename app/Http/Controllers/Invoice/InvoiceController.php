<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use App\Models\Invoice\Invoice_item;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use App\Models\CompanyModel;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use \App\Models\Invoice\Custom;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use \Illuminate\Support\Facades\Request;

class InvoiceController extends Controller {

    function __construct() {
//        LoginController::checkLogin();
    }

    /**
     * @ Function Name          : add
     * @ Route Name             : invoice-add
     * @ Function Purpose  :  display Create invoice form
     * @ Function Returns : 
     */
    public function add() {
        $data['clients'] = CompanyModel::all();
//        $data['project'] = Client::all();
        $data['pageTitle'] = "Invoice Create";
        $data['breadcamps'] = array('Invoice' => 'invoice-list/1', 'Create Invoice' => 'invoice-add');
        return view('invoice/add', $data);
    }

    /**
     * @ Function Name          : invoiceList
     * @ Route Name             : invoice-list
     * @ Function Purpose  :  display invoice list
     * @ Function Returns :
     */
    public function invoiceList($page = 0, $id = null) {
        $Nextpage = 0;
        if ($page != 0) {
            $Nextpage = ($page - 1) * 20;
        }
        $data['invoices'] = DB::table('invoice')
                ->join('company', 'invoice.client', '=', 'company.company_id')
                // ->join('company_document', 'company.company_id', '=', 'company_document.company_id')
                ->select('invoice.*', 'company.company_name')
                ->limit(20)
                ->offset($Nextpage)
                ->get();
        $data['count'] = DB::table('invoice')
                ->join('company', 'invoice.client', '=', 'company.company_id')
                // ->join('company_document', 'company.company_id', '=', 'company_document.company_id')
                ->select('invoice.*', 'company.company_name')
                ->count();
//        echo '<pre>';
//        print_r($data['invoices']);
//        die('hie'.$data['count']);
        $data['page'] = $page;
        $data['pageTitle'] = "Invoice List";
        $data['breadcamps'] = array('Invoice' => '#', 'Invoice List' => 'invoice-list/1');
        $data['status'] = $id;
        return view('invoice/list', $data);
    }

    /**
     * @ Function Name          : invoiceEdit
     * @ Route Name             : invoice-edit/{id}
     * @ Function Purpose  :  display edit form with information to update
     * @ Function Returns : 
     */
    public function invoiceEdit($id) {
        $data['allClients'] = CompanyModel::all();
        $data['invoice'] = Invoice::where('invoice.invoice_id', '=', $id)->first();
        $data['items'] = Invoice_item::where('invoice', '=', $id)->get();

        $data['pageTitle'] = "Invoice Edit";
        $data['breadcamps'] = array('Invoice' => 'invoice-list/1', 'Invoice Edit' => 'invoice-edit/' . $id);

        /* echo "<pre/>";
          var_dump($data);
          die; */


        return view('invoice/edit', $data);
    }

    /**
     * @ Function Name          : invoiceDelete
     * @ Route Name             : invoice-delete/{id}
     * @ Function Purpose  :  delete invoice by id
     * @ Function Returns : 
     */
    public function invoiceDelete($id) {

        ///work to do
        DB::table('invoice_items')->where('invoice', '=', $id)->delete();
        DB::table('invoice')->where('invoice_id', '=', $id)->delete();
        return redirect('invoice-list');

        // redirect to list
    }

    /**
     * @ Function Name          : invoiceInsert
     * @ Route Name             : create-invoice
     * @ Function Purpose  :  insert new invoice after submit
     * @ Function Returns : 
     */
    public function invoiceInsert() {

        /* var_dump(Input::all());

          die; */

        $validationRules = array(
            'client' => 'required',
            'doi' => 'required',
            'poNumber' => 'required'
        );

        $validationMessages = array(
            'required' => 'The :attribute required a value'
        );

        $validator = Validator::make(Input::all(), $validationRules, $validationMessages);

        if (!$validator->fails()) {
            $data = Input::get();
            $invoice = new Invoice();

            $invoice_number = sprintf("%03d", Input::get('client')) . "-" . date('dmY') . "-" . sprintf("%03d", ($invoice->max('invoice_id') + 1));

            if ($data['client'] != '') {
                $i = 0;
                $validation = FALSE;
                if (count($data['item'])) {
                    foreach ($data['item'] as $item) {
                        if ($data['item'][$i] != '' && $data['unit'][$i] != '' && $data['qty'][$i] != '') {
                            $validation = TRUE;
                            break;
                        }
                        $i++;
                    }

                    if ($validation) {
                        if (trim($data['doi']) != '' && trim($data['poNumber']) != '' && trim($data['grandTotal']) != '')
                            $invoice->client = $data['client'];
                        $invoice->invoice_number = $invoice_number;
//                        $invoice->project_id = $data['project_id'];
                        $invoice->issue_date = date('Y-m-d', strtotime(str_replace('-', '/', $data['doi'])));
                        $invoice->po_number = $data['poNumber'];
                        $invoice->invoice_total = $data['grandTotal'];
                        $invoice->discount = $data['discount-amount'];
                        $invoice->terms = $data['terms'];
                        $invoice->note = $data['notes'];
                        $invoice->save();

                        $inv_id = $invoice->invoice_id;

                        $i = 0;
                        foreach ($data['item'] as $item) {
                            if ($data['item'][$i] != '' && $data['unit'][$i] != '' && $data['qty'][$i] != '') {
                                $inputItem = new Invoice_item();
                                $inputItem->invoice = $inv_id;
                                $inputItem->item = $data['item'][$i];
                                $inputItem->description = $data['desc'][$i];
                                $inputItem->unit_cost = $data['unit'][$i];
                                $inputItem->quantity = $data['qty'][$i];
                                $inputItem->total = $data['unit'][$i] * $data['qty'][$i];
                                $inputItem->save();
                            }
                            $i++;
                        }

                        Session::flash('msg', 'Invoice Has been Added.');

                        Session::flash('alert-class', 'alert-success');
                        //return Redirect::to('invoice-list/'.$inputItem->invoice_details_id);
                    } else
                        return redirect(URL::to('/invoice-add'));
                    //echo "validation false. should redirect";
                }
            } else {
                return redirect(URL::to('/invoice-add'));
                //echo "must give client name. should redirect";
            }
            return redirect(URL::to('/invoice-list/1'));
        } else {
            return redirect('invoice-add')->withErrors($validator);
        }
    }

    /**
     * @ Function Name          : invoiceUpdate
     * @ Route Name             : invoice-update
     * @ Function Purpose  :  update invoice after submit
     * @ Function Returns : 
     */
    public function invoiceUpdate() {
        $data = Input::get();
        $invoice = Invoice::find($data['invoice_id']);

        if ($data['grandTotal'] != '') {
            $i = 0;
            $validation = TRUE;
            if ($validation) {
                //$invoice->client = $data['client'];
                //$invoice->project_id = $data['project_id'];
                $invoice->issue_date = date('Y-m-d', strtotime(str_replace('-', '/', $data['doi'])));
                $invoice->po_number = $data['poNumber'];
                $invoice->invoice_total = $data['grandTotal'];
                $invoice->discount = $data['discount'];
                $invoice->terms = $data['terms'];
                $invoice->note = $data['notes'];
                $invoice->save();

                $inv_id = $invoice->invoice_id;


                $i = 0;
                foreach ($data['old_item'] as $item) {
                    if ($data['old_item'][$i] != '' && $data['old_unit'][$i] != '' && $data['old_qty'][$i] != '') {
                        $inputItem = Invoice_item::find($data['old_id'][$i]);
                        $inputItem->item = $data['old_item'][$i];
                        $inputItem->description = $data['old_desc'][$i];
                        $inputItem->unit_cost = $data['old_unit'][$i];
                        $inputItem->quantity = $data['old_qty'][$i];
                        $inputItem->total = $data['old_unit'][$i] * $data['old_qty'][$i];
                        $inputItem->save();
                    }
                    $i++;
                }


                $i = 0;
                foreach ($data['item'] as $item) {
                    if ($data['item'][$i] != '' && $data['unit'][$i] != '' && $data['qty'][$i] != '') {
                        $inputItem = new Invoice_item();
                        $inputItem->invoice = $inv_id;
                        $inputItem->item = $data['item'][$i];
                        $inputItem->description = $data['desc'][$i];
                        $inputItem->unit_cost = $data['unit'][$i];
                        $inputItem->quantity = $data['qty'][$i];
                        $inputItem->total = $data['unit'][$i] * $data['qty'][$i];
                        $inputItem->save();
                    }
                    $i++;
                }
            } else {
                return Redirect::to('invoice-edit/' . $data['invoice_id']);
            }
        } else {
            return Redirect::to('invoice-edit/' . $data['invoice_id']);
        }
        Session::flash('msg', 'Invoice Has been Updated.');

        Session::flash('alert-class', 'alert-success');
        return Redirect::to('invoice-edit/' . $data['invoice_id']);
    }

    /**
     * @ Function Name          : deleteInvoiceItemById
     * @ Route Name             : invoice-item-delete
     * @ Function Purpose  :  delete invoice item by id
     * @ Function Returns : 
     */
    public function deleteInvoiceItemById() {
        $id = Input::get('id');
        $invoiceItem = Invoice_item::find($id);
        if ($invoiceItem->delete())
            echo "success";
    }

    /**
     * @ Function Name          : invoiceFile
     * @ Route Name             : printInvoice/{id}.pdf
     * @ Function Purpose  :  display pdf file for a invoice
     * @ Function Returns : 
     */
    public function invoiceFile($invoice_number = null) {
        $invoice_number = str_replace('.pdf', '', $invoice_number);
        $invoiceInfo = null;
        $invoiceItems = null;
        $items = '';
        $clientInfo = '';

        if ($invoice_number != null) {
            $invoiceInfo = Invoice::where('invoice_number', '=', $invoice_number)->get();

            if (count($invoiceInfo) == 1) {
                $invoiceInfo = $invoiceInfo[0]['original'];
                $clientInfo = Custom::getClientInfo($invoiceInfo['client']);
                $invoiceItems = Invoice_item::where('invoice', '=', $invoiceInfo['invoice_id'])->get();
                $total_invoice = $invoiceInfo['invoice_total'] + $invoiceInfo['discount'];
                $total_payment = Custom::getPaymentStatus($invoiceInfo['invoice_id']);

                if ($total_payment == NULL) {
                    $total_payment = 0.00;
                }
                if ($invoiceInfo['invoice_total'] - $total_payment == 0) {
                    $due_amount = '0.00';
                } else {

                    $due_amount = $invoiceInfo['invoice_total'] - $total_payment;
                }

                foreach ($invoiceItems as $item) {
                    $items .= '<tr>
                        <td style="text-align:left;">' . $item['original']['item'] . '</td>
                        <td style="text-align:left;">' . $item['original']['description'] . '</td>
                        <td>' . $item['original']['unit_cost'] . '</td>
                        <td>' . $item['original']['quantity'] . '</td>
                        <td>' . $item['original']['total'] . '</td>
                    </tr>';
                }
            } else {
                return redirect(URL::to('/invoice-list/1'));
            }
        } else {
            return redirect(URL::to('/invoice-list/1'));
        }

        $dt = array(
            'clientInfo' => $clientInfo,
            'invoiceInfo' => $invoiceInfo,
            'due_amount' => $due_amount,
            'total_invoice' => $total_invoice,
            'total_payment' => $total_payment,
            'items' => $items,
        );
//        echo "<pre>";
//        print_r($dt);die;
        //return view('pdf/from',$dt);
//        $pdf = App::make('dompdf');
//      
//        $pdf->loadView('pdf/from', $dt);
//        return $pdf->stream("olu.pdf");
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf/from', $dt);
        return $pdf->stream();
    }

}
