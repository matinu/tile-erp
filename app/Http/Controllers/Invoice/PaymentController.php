<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Models\CompanyModel;
use App\Models\Invoice\Custom;
use App\Models\Invoice\Invoice;
use App\Models\Invoice\Payment;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use \Illuminate\Support\Facades\Request;

class PaymentController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
//        LoginController::checkLogin();
    }

    /**
     * @ Function Name          : add
     * @ Route Name             : payment-add
     * @ Function Purpose  :  display new payment entry
     * @ Function Returns :
     */
    public function add($id = null) {
        $invoice_id = $id;
        if ($id == null) {
            $id = Input::get('invoice');
        } else {
            $id = Invoice::where('invoice_number', '=', $id)->first(array('invoice_id'))['original']['invoice_id'];
        }

        if ($id == null) {
            return redirect(url('payment'));
        }

        $data['amount'] = Payment::getTotalAmount($id);
        $data['invoice'] = Invoice::getInvoiceDetails($id);
        $data['pageTitle'] = "Invoice Payment";
        $data['breadcamps'] = array('Invoice' => 'invoice-list', 'Payment List' => 'payment-list', 'Payment Details' => 'payment/' . $invoice_id, 'Add New Payment' => 'payment-add/' . $invoice_id);

        return view('payment/add', $data);
    }

    /**
     * @ Function Name          : paymentEntry
     * @ Route Name             : payment-entry/{id}/
     * @ Function Purpose  :  display new payment entry after submit form
     * @ Function Returns :
     */
    public function paymentEntry($id = 0) {
        if (Input::get('amount') == NULL || Input::get('type') == NULL || Input::get('date') == NULL) {
            return redirect('payment-list');
        }
        $payment = new Payment();
        $payment->invoice_id = $id;
        $payment->amount = Input::get('amount');
        $payment->method = Input::get('type');
        $payment->payment_date = date('Y-m-d', strtotime(str_replace('-', '/', Input::get('date'))));
        $payment->note = Input::get('notes');
        $sql1Status = $payment->save();

        Session::flash('msg', 'Payment added successfully.');
        Session::flash('alert-class', 'alert-success');
        return redirect('payment-list');
    }

    /**
     * @ Function Name          : paymentList
     * @ Route Name             : payment-list
     * @ Function Purpose  :  view all payment
     * @ Function Returns :
     */
    public function paymentList() {
//        die;
        //$data['payment'] = Payment::getUnpaidInvoice();
        $data['action'] = 'all';
        $data['organizations'] = CompanyModel::all(array('company_id', 'company_name'));
        $data['invoices'] = Custom::getInvoiceList();
        $data['pageTitle'] = "Payment List";
        $data['breadcamps'] = array('Invoice' => 'invoice-list/1', 'Payment List' => 'payment-list');
        return view('payment/list', $data);
    }

    /*
     * @ Function Name      :   getListToAddPayment
     * @ Router Name        :   payment
     * @ Function Purpose   :   show all invoice list with left join with payments table
     * @ Function Returns   :   to router with data
     */

    public function getPaymentDetails($id = null) {
        if ($id != null) {
            if ($id == 'paid') {
                $invoices = Custom::getInvoiceList();
                $action = 'paid';
            } else {
                $invoices = Payment::getPayment(array(
                            'invoiceId' => $id,
                            'organizationName' => null,
                            'date' => null,
                ));
                $action = 'single';
            }
        } else {
            $invoices = Custom::getInvoiceList();
            $action = 'all';
        }
        //$data['payment']        = Payment::getUnpaidInvoice();
        $data['organizations'] = CompanyModel::all(array('company_id', 'company_name'));
        $data['invoices'] = $invoices;
        $data['pageTitle'] = "Payment Details";
        $data['action'] = $action;
        $data['breadcamps'] = array('Invoice' => 'invoice-list/1','Payment List' => 'payment-list', 'Payment Details' => 'payment/' . $id);

        //var_dump($data);die;

        return view('payment/list', $data);
    }

    /**
     * @ Function Name          : invoiceList
     * @ Route Name             : invoice-list/{id}
     * @ Function Purpose  :
     * @ Function Returns : get invoice by company
     */
    public function invoiceList($id = 0) {
        $var = "<option value=''>Select Invoice</option>";
        if ($id == 0)
            echo $var;
        else {
            $invoice = Payment::getUnpaidInvoicebycompany($id);

            if (!empty($invoice)) {
                foreach ($invoice as $inv):
                    $var .= "<option value='" . $inv->invoice_number . "'>" . $inv->invoice_number . "</option>";
                endforeach;
            }
            echo $var;
        }
    }

    /**
     * @ Function Name          : searchPayment
     * @ Route Name             : payment-list
     * @ Function Purpose  :  search payment information
     * @ Function Returns :
     */
    public function searchPayment() {
        $inputData = @Input::get();
        $organizationName = $inputData['organizationName'];
        $invoiceId = $inputData['invoiceId'];
        $date = $inputData['date'];
        if ($invoiceId != NULL) {
            $data['invoice_number'] = $invoiceId;
        }


        $data['organizations'] = CompanyModel::all(array('company_id', 'company_name'));
        $data['invoices'] = Payment::getPayment($inputData);
        $data['invoices'] = Custom::getInvoiceList($organizationName, $invoiceId, $date);
        $data['pageTitle'] = "Payment Search";
        $data['action'] = 'all';
        $data['breadcamps'] = array('Payment List' => 'Payment-list');

        //echo "<pre/>";
        //var_dump($data);die;

        return view('payment/list', $data);
    }

    /**
     * @ Function Name          : paymentEdit
     * @ Route Name             : payment-edit/{id}/
     * @ Function Purpose  :  show payment edit form by id
     * @ Function Returns :
     */
    public function paymentEdit($id = 0) {
        $data['invoice'] = Payment::getPaymentbyid($id);
        $data['pageTitle'] = "Payment Edit";
        $data['breadcamps'] = array('Payment List' => 'payment-list', 'Payment Details' => 'payment/' . $data['invoice'][0]->invoice_number, 'Payment Edit' => 'payment-edit/' . $id);
        return view('payment/edit', $data);
    }

    /**
     * @ Function Name          : updatePayment
     * @ Route Name             : payment-edit/
     * @ Function Purpose  :  update payment information
     * @ Function Returns :
     */
    public function updatePayment() {
        if (isset($_POST['payment_id'])) {
            /* echo "<pre/>";
              print_r($_POST);die; */
            $paymentId = Input::get('payment_id');
            $paymentDue = Input::get('payment_due');
            $amount = Input::get('amount');
            $paymentType = Input::get('payment-type');
            $issueDate = date('Y-m-d', strtotime(str_replace('-', '/', Input::get('issue-date'))));
            $notes = Input::get('notes');

            $previouslyPay = Payment::find($paymentId, array('amount'));
            $previouslyPay = $previouslyPay['original']['amount'];

            $newDueAmount = $paymentDue + $previouslyPay;


            if ($newDueAmount >= $amount) {
                $payment = Payment::find($paymentId);
                $payment->amount = $amount;
                $payment->method = $paymentType;
                $payment->payment_date = $issueDate;
                $payment->note = $notes;

                if ($payment->save()) {
                    Session::flash('msg', 'Payment Updated Successfully.');
                    Session::flash('alert-class', 'alert-success');
                    return redirect(URL::to('/payment/' . Input::get('invoice_number')));
                } else {
                    Session::flash('msg', 'Payment Updated Failed.');
                    Session::flash('alert-class', 'alert-danger');
                }
            } else {
                Session::flash('msg', 'Payment Updated Failed. You may pay max ' . $newDueAmount . '!');

                Session::flash('alert-class', 'alert-danger');
            }

            return redirect(URL::to('/payment-edit/' . $paymentId));
        }

        return redirect(URL::to('/payment-list'));
    }

    /**
     * @ Function Name          : paymentDelete
     * @ Route Name             : payment-delete/{id}/
     * @ Function Purpose  :  delete payment by id
     * @ Function Returns :
     */
    public function paymentDelete($id = 0) {

        ///work to do
        $user = DB::table('payments')
                        ->join('invoice', 'invoice.invoice_id', '=', 'payments.invoice_id')
                        ->where('payment_id', '=', $id)->get();
        DB::table('payments')->where('payment_id', '=', $id)->delete();
        Session::flash('msg', 'Payment entry deleted successfully.');
        Session::flash('alert-class', 'alert-success');
        return redirect('payment/' . $user[0]->invoice_number);
        // redirect to list
    }

}
