<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Permission;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Session;

abstract class Controller extends BaseController {

    use DispatchesCommands,
        ValidatesRequests;

    public function __construct() {
        if (Session::get('user_id')==NULL)
            return redirect(url('login'))->send();
        $requesturl = $_SERVER['QUERY_STRING'];
        $check = strpos($requesturl, "/");
        if ($check != '') { //if slash found in the URL
            $string = $_SERVER['QUERY_STRING'];
            $string = explode('/', $string);
            $lastpart = array_pop($string);
            $string = implode('/', $string);
            if (is_numeric($lastpart) || strlen($lastpart) > 25) {
                $name = $string;
            } else {
                $name = $_SERVER['QUERY_STRING'];
            }
        } else {
            $name = $_SERVER['QUERY_STRING'];
        }

        $permissiondetails = Permission::where('name', '=', $name)->first();
        if (count($permissiondetails) == 0) {   //if no data found in database
//            $name = $_SERVER['QUERY_STRING'];
            $dispname = '';
            $check = strpos($name, "/");
            if ($check != '') { //if slash found in the URL
                $tempname = explode("/", $name);
                end($tempname);
                $last_id = key($tempname);
                foreach ($tempname as $key => $tmp):
                    if ($key != $last_id) {
                        $dispname = $dispname . ucfirst($tmp) . ':';
                    } else {
                        $lastname = $tmp;
                    }
                endforeach;
            } else {
                $lastname = $name;
            }

            $check = strpos($lastname, "-");
            if ($check != '') {
                $templastname = explode("-", $lastname);
                end($templastname);
                $last_id = key($templastname);
                foreach ($templastname as $key => $tmp):
                    if ($key != $last_id) {
                        $dispname = $dispname . ucfirst($tmp) . ' ';
                    } else {
                        $dispname = $dispname . ucfirst($tmp);
                    }
                endforeach;
            } else {
                $dispname = $dispname . ucfirst($lastname);
            }

            //name build done
            $add = new Permission;
            $add->name = $name;
            $add->display_name = $dispname;
            $add->description = '';
            $add->created_at = Carbon::now();
            $add->updated_at = Carbon::now();
            $add->save();
        }
//        if ($name != "dashboard"):
//            $user = User::where('id', '=', Session::get('user_id'))->first();
//            if ($user->can($name) != TRUE):
//                return redirect(url('dashboard'))->send();
//            endif;
//        endif;
    }

}
