<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Models\CompanyModel;
use App\Models\CompanyBillingConfiguration;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Session;
use \Illuminate\Support\Facades\DB;

class WelcomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index() {

        if (Session::get('user_email'))
            return redirect(url('list-user/1'));
        else
            return redirect(url('login'));
    }

    public function profile() {
        $data = array(
            'pageTitle' => 'My Account',
            'breadcamps' => array('Profile' => 'profile'),
        );
        $user = new UserModel();
        $data['user'] = $user->getuserbyid(Session::get('user_id'));
        $user = User::where('id', Session::get('user_id'))->first();
        $data['roles'] = $user->with('roles')->get();
        return view('admin/profile', $data);
    }

    public function dashboard() {
        $data = array(
            'pageTitle' => 'Dashboard',
            'breadcamps' => array(),
        );
        $data['companies'] = CompanyModel::paginate(5);
        $data['invoices'] = DB::table('invoice')
                ->join('company', 'invoice.client', '=', 'company.company_id')
                // ->join('company_document', 'company.company_id', '=', 'company_document.company_id')
                ->select('invoice.*', 'company.company_name')
                ->limit(5)
                ->get();
        $data['billings'] = CompanyBillingConfiguration::limit(5)
                ->get();
//         print_r($data['billings']);die;
        return view('admin/dashboard', $data);
    }

    public function postUpload() {
        Input::file('uploadfile')->move(storage_path('temp/'), 'import.xls');

        try {

            Artisan::call('importexcelfile', [
                'file' => 'app/storage/temp/import.xlsm',
            ]);

            return Response::json([
                        'result' => 1
            ]);
        } catch (Exception $e) {

            return Response::json([
                        'result' => -1,
                        'message' => $e->getMessage(),
            ]);
        }
    }

}
