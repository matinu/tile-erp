<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use App\Role;
use App\User;
use App\Permission;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PermissionController
 *
 * @author shahadat
 */
class PermissionController extends Controller {

    public function index($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $permission = new Permission();
        $data = array(
            'pageTitle' => 'Permission List',
            'breadcamps' => array('Permission' => '#', 'Permission List' => 'list-permission/1'),
        );
        $data['list'] = $permission->getallpermissions($page);
        $data['count'] = $permission->allpermissioncount();
        $data['page'] = $page;
//        $data['list'] = Permission::paginate(1000);
        return view('Permission/list', $data);
    }

    public function edit_Permission($id = 0, Request $request) {
        if ($id == 0):
            return redirect(url('list-permission/1'));
        endif;
        if (!$request->isMethod('post')) {
            $data = array(
                'pageTitle' => 'Edit Permission',
                'breadcamps' => array('Permission' => 'list-permission/1', 'Edit Permission' => 'edit-permission/' . $id),
            );
            $data['permission'] = Permission::find($id);
            return view('Permission/edit', $data);
        } else {
            $inputs = array(
                'display_name' => Input::get('display_name'),
                'description' => Input::get('description'),
                'name' => Input::get('name'),
            );
            $rules = array(
                'display_name' => "required",
                'description' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {

                $role = Permission::find($id);
                $role->name = Input::get('name');
                $role->display_name = Input::get('display_name');
                $role->description = Input::get('description');
                $role->save();
                Session::flash('msg', 'Permission has been Updated.');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('list-permission/1'));
            }
        }
    }

    public function delete_Permission($id) {
        Permission::destroy($id);
        Session::flash('msg', 'Permission has been Deleted.');

        Session::flash('alert-class', 'alert-success');
        return redirect(url('list-permission/1'));
    }

    public function set_RolePermission(Request $request) {
        if (!$request->isMethod('post')) {
            $data = array(
                'pageTitle' => 'Role Permission set',
                'breadcamps' => array('Permission' => 'list-permission/1', 'Role Permission List' => 'view-role-permission', 'Set Role Permission' => 'set-permission'),
            );
            $data['permissions'] = Permission::all();
            $role_permission = DB::table('permission_role')->select('role_id')->distinct()->get();
            $result = array();
            foreach ($role_permission as $key => $value) {
                $result[] = $value->role_id;
            }
            $data['roles'] = DB::table('roles')->whereNotIn('id', $result)->get();
            return view('Permission/setpermission', $data);
        } else {
            $inputs = array(
                'role' => Input::get('role'),
                'permission' => Input::get('permission'),
            );
            $rules = array(
                'role' => "required",
                'permission' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                $role = Role::where('id', '=', Input::get('role'))->first();
                $role->perms()->sync(Input::get('permission'));
                Session::flash('msg', 'Permission has been set for Role');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('view-role-permission'));
            }
        }
    }

    public function view_RolePermission() {
        $data = array(
            'pageTitle' => 'View Role Permission',
            'breadcamps' => array('Permission' => 'list-permission/1', 'Role Permission List' => 'view-role-permission'),
        );
        $data['list'] = Role::with('perms')->get();

        return view('Permission/listrolepermission', $data);
    }

    public function edit_RolePermission($id = 0, Request $request) {
        if ($id == 0):
            return redirect(url('view-role-permission'));
        endif;
        if (!$request->isMethod('post')) {
            $data = array(
                'pageTitle' => 'Role Permission set',
                'breadcamps' => array('Permission' => 'list-permission/1', 'Role Permission List' => 'view-role-permission', 'Edit Role Permission' => 'edit-role-permission/' . $id),
            );
            $data['permissions'] = Permission::all();
            $role_permission = DB::table('permission_role')->Where('role_id', $id)->get();
            $data['role_permission'] = array();
            $data['roles_id'] = $id;
            foreach ($role_permission as $key => $value) {
                $data['role_permission'][] = $value->permission_id;
            }

            $data['roles'] = Role::all();
            return view('Permission/editrolepermission', $data);
        } else {
            $inputs = array(
                'role' => Input::get('role'),
                'permission' => Input::get('permission'),
            );
            $rules = array(
                'role' => "required",
                'permission' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                DB::table('permission_role')->Where('role_id', $id)->delete();
                $role = Role::where('id', '=', Input::get('role'))->first();
                $role->perms()->sync(Input::get('permission'));
                Session::flash('msg', 'Permission has been Update for Role');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('view-role-permission'));
            }
        }
    }

    public function delete_RolePermission($id) {
        DB::table('permission_role')->Where('role_id', $id)->delete();
        Session::flash('msg', 'Permission has been Deleted for Role.');

        Session::flash('alert-class', 'alert-success');
        return redirect(url('view-role-permission'));
    }

}
