<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use Illuminate\Pagination\Paginator;
use App\Models;
use App\Models\CompanyBillingConfiguration;
use App\Models\CompanyModel;

class CompanyBillingController extends Controller {

    public function create() {
        $companies = CompanyModel::all();
        $data = array(
            'pageTitle' => 'Create Company Billing',
            'breadcamps' => array('Billing Configuration' => 'list-billing/1', 'Create Billing' => 'create-billing'),
            'companies' => $companies
        );
        return view('billing.create', $data);
    }

    public function save() {
        $company = Input::get('company_id');
        $billing_period = Input::get('billing_period');
        $cycle_start_date = Input::get('cycle_start_date');
        $hourly_rate = Input::get('hourly_rate');

        $inputs = array(
            'company' => $company,
            'billing_period' => $billing_period,
            'cycle_start_date' => date('Y-m-d', strtotime($cycle_start_date)),
            'hourly_rate' => $hourly_rate,
        );
        $rules = array(
            'company' => "required",
            'billing_period' => "required",
            'cycle_start_date' => "required|date",
            'hourly_rate' => "required|integer",
        );
        $validate = Validator::make($inputs, $rules);

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        } else {
            $companyConfiguration = new CompanyBillingConfiguration();

            $companyConfiguration->company_id = $company;
            $companyConfiguration->billing_period = $billing_period;
            $companyConfiguration->cycle_start_date = date('Y-m-d', strtotime($cycle_start_date));

            $companyConfiguration->hourly_rate = $hourly_rate;
            if ($companyConfiguration->save()) {

                $msg = "Company Billing successfully added !";
                Session::flash('success_add_msg', $msg);
                Session::flash('alert-class', 'alert-success');
                return redirect('list-billing/1');
            }
        }
    }

    public function edit($id) {
        $companies = CompanyModel::all();
        $billing = CompanyBillingConfiguration::find($id);
        $data = array(
            'companies' => $companies,
            'pageTitle' => 'Edit Billing',
            'breadcamps' => array('Billing Configuration' => 'list-billing/1', 'Edit billing' => 'edit-billing/' . $id),
        );
        $data['billing'] = $billing;
        return view('billing.edit', $data);
    }

    public function update($id) {
        $company = Input::get('company_id');
        $billing_period = Input::get('billing_period');
        $cycle_start_date = Input::get('cycle_start_date');
        $hourly_rate = Input::get('hourly_rate');

        $inputs = array(
            'company' => $company,
            'billing_period' => $billing_period,
            'cycle_start_date' => $cycle_start_date,
            'hourly_rate' => $hourly_rate,
        );
        $rules = array(
            'company' => "required",
            'billing_period' => "required",
            'cycle_start_date' => "required|date",
            'hourly_rate' => "required|integer",
        );
        $validate = Validator::make($inputs, $rules);

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        } else {
            $companyConfiguration = CompanyBillingConfiguration::find($id);

            $companyConfiguration->company_id = $company;
            $companyConfiguration->billing_period = $billing_period;
            $companyConfiguration->cycle_start_date = date('Y-m-d', strtotime($cycle_start_date));

            $companyConfiguration->hourly_rate = $hourly_rate;
            if ($companyConfiguration->save()) {

                $msg = "Company Billing successfully Updated !";
                Session::flash('success_update_msg', $msg);
                Session::flash('alert-class', 'alert-success');
                return redirect('list-billing/1');
            }
        }
    }

    public function listBilling($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $billingmodel = new CompanyBillingConfiguration();
        $data = array(
            'pageTitle' => 'List Billing',
            'breadcamps' => array('Billing Configuration' => '#', 'List Billing' => 'list-billing/1'),
        );
        $billings = $billingmodel->getallbilling($page);
        $data['billings'] = $billings;
        $data['count'] = $billingmodel->allbillingcount();
        $data['page'] = $page;
        return view('billing.list', $data);
    }

    public function delete($id) {
        $company = CompanyBillingConfiguration::find($id);
        if ($company->delete()) {
            $msg = "Company Billing info Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect('list-billing/1');
        }
    }

}
