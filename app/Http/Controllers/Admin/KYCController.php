<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use Illuminate\Pagination\Paginator;
use App\Models;
use App\Models\CompanyBillingConfiguration;
use App\Models\CompanyModel;
use Models\BankAccountOpening;
use \Illuminate\Support\Facades\File;

class KYCController extends Controller {

    public function index($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $list = new Models\KYC();
        $data = array(
            'pageTitle' => 'KYC',
            'breadcamps' => array('KYC' => 'list-kyc/1'),
        );
        $data['list'] = $list->pagesearch($page);
        $data['count'] = Models\KYC::all()->count();
        $data['page'] = $page;
//        $data['list'] = Role::all();
        return view('KYC/list', $data);
    }

    public function create() {
        if ($_POST) {
            $documentFile = Input::file('attached');
            $document = new Models\KYC();
            $document->name = Input::get('name');
            $document->country = Input::get('country');
            $document->description = Input::get('description');
            $documentFile = Input::file('kyc_file');

            if ($documentFile != '') {

                $destinationPath = 'public/images/kyc';
                $filename = rand() . $documentFile->getClientOriginalName();
                $upload_success = $documentFile->move($destinationPath, $filename);
                if ($upload_success) {
                    $document->filename = $filename;
                }
            }
            if ($document->save()) {
                $msg = "KYC successfully added !";
                Session::flash('success_add_msg', $msg);
                Session::flash('alert-class', 'alert-success');
                return redirect('list-kyc/1');
                //  echo json_encode(array('status' => true, 'data' => $document));
            }
        } else {
            $data = array(
                'pageTitle' => 'Create KYC',
                'breadcamps' => array('KYC' => 'list-kyc/1', 'Create KYC' => 'create-kyc'),
                'countries' => Models\Country::all()
            );
            return view('KYC/create', $data);
        }
    }

    public function delete($id) {
        $company = Models\KYC::find($id);
        $filepath = 'public/images/kyc/';

        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "KYC Successfully Deleted !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

}
