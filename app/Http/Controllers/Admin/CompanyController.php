<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;
use \App\Models\Invoice\Custom;
use illuminate\html;
use Illuminate\Pagination\Paginator;
use App\Models;
use App\Models\LoginModel;
use App\Models\CompanyModel;
use App\Models\CompanyTemplate;
use App\Models\CompanyCreditInvoice;
use App\Models\CompanyBillingConfiguration;
use App\Models\CompanyFinancialStatements;
use \Illuminate\Support\Facades\File;

class CompanyController extends Controller {

    public function create() {
        $country = Models\Country::all();
        $data = array(
            'pageTitle' => 'Create Company',
            'breadcamps' => array('Company' => 'list-company/1', 'Create Company' => 'create-company'),
            'countries' => $country
        );
        return view('company.create', $data);
    }

    public function save() {
        $company_name = Input::get('company_name');
        $company_type = Input::get('company_type');
        $date_of_incorporation = Input::get('date_of_incorporation');
        $company_status = Input::get('company_status');
        $company_country = Input::get('company_country');
        $company_new_address = Input::get('company_new_address');
        $company_old_address = Input::get('company_old_address');
        $company_logo = Input::file('company_logo');

        $inputs = array(
            'company_name' => $company_name,
            'company_type' => $company_type,
            'date_of_incorporation' => $date_of_incorporation,
            'company_status' => $company_status,
            'company_country' => $company_country,
            'company_new_address' => $company_new_address,
            'company_old_address' => $company_old_address,
            'company_logo' => $company_logo,
        );
        $rules = array(
            'company_name' => "required",
            'company_type' => "required",
            'date_of_incorporation' => "required",
            'company_status' => "required",
            'company_country' => "required",
            'company_new_address' => "required",
            'company_old_address' => "required",
            'company_logo' => "required",
        );
        $validate = Validator::make($inputs, $rules);

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        } else {
            $company = new CompanyModel();
            $company->company_name = $company_name;
            $company->company_type = $company_type;
            $company->date_of_incorporation = date('Y-m-d', strtotime($date_of_incorporation));
            $company->company_status = $company_status;
            $company->company_country = $company_country;
            $company->company_new_address = $company_new_address;
            $company->company_old_address = $company_old_address;
            $company->created_at = date('Y-m-d H:i:s');

            $company->created_by = Session::get('user_id');

            if ($company_logo != '') {

                $destinationPath = 'public/images/company_logo';
                $filename = rand() . $company_logo->getClientOriginalName();
                $upload_success = $company_logo->move($destinationPath, $filename);
                if ($upload_success) {
                    $company->company_logo = $filename;
                }
            }
            $company_id = 0;
            if ($company->save()) {
                $company_id = $company->company_id;
            }

            $person_name = Input::get('person_name');
            $person_contact = Input::get('person_contact_number');
            $person_designation = Input::get('person_designation');
            $person_email = Input::get('person_email');
            $person_type = Input::get('person_type');
            $doappointment = Input::get('doappointment');
            $doresignation = Input::get('doresignation');
            $director = Input::get('director');
            $shareholder = Input::get('shareholder');
            $secretary = Input::get('secretary');

            if (count($person_name) != 0) {
                for ($i = 0; $i < count($person_name); $i++) {
                    $company_contact_person = new Models\CompanyContactPersonModel();
                    $company_contact_person->company_id = $company_id;
                    $company_contact_person->person_name = $person_name[$i];
                    $company_contact_person->contact_person_type = $person_type[$i];
                    $company_contact_person->person_designation = $person_designation[$i];
                    $company_contact_person->person_contact_number = $person_contact[$i];
                    $company_contact_person->person_email = $person_email[$i];
                    $company_contact_person->doappointment = date('Y-m-d', strtotime($doappointment[$i]));
                    $company_contact_person->doresignation = date('Y-m-d', strtotime($doresignation[$i]));
                    $company_contact_person->director = $director[$i];
                    $company_contact_person->shareholder = $shareholder[$i];
                    $company_contact_person->secretary = $secretary[$i];
                    $company_contact_person->save();
                }
            }

            $document_title = Input::get('document_title');
            $document_description = Input::get('document_description');
            $document_attached = Input::file('company_document');
            if ($document_attached[0] != '') {
                foreach ($document_attached as $key => $file) {

                    $destinationPath = 'public/images/company_document';
                    $filename = rand() . $file->getClientOriginalName();
                    $upload_success = $file->move($destinationPath, $filename);
                    if ($upload_success) {
                        $company_document = new Models\CompanyDocumentModel();
                        $company_document->company_id = $company_id;
                        $company_document->document_title = $document_title[$key];
                        $company_document->document_description = $document_description[$key];
                        $company_document->document_attached = $filename;
                        $company_document->save();
                    }
                }
            }
            $msg = "Company successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect('list-company/1');
        }
    }

    public function edit($id) {
        $country = Models\Country::all();
        $company = CompanyModel::find($id);
        $data = array(
            'countries' => $country,
            'pageTitle' => 'Edit Company',
            'breadcamps' => array('Company' => 'list-company/1', $company->company_name => 'edit-company/' . $company->company_id),
        );
        $data['company'] = $company;
        return view('company.edit', $data);
    }

    public function update($id) {
        $company_name = Input::get('company_name');
        $previous_name = Input::get('previous_name');
        $company_type = Input::get('company_type');
        $date_of_incorporation = Input::get('date_of_incorporation');
        $company_status = Input::get('company_status');
        $company_country = Input::get('company_country');
        $company_new_address = Input::get('company_new_address');
        $company_old_address = Input::get('company_old_address');
        $company_logo = Input::file('company_logo');

        $inputs = array(
            'company_name' => $company_name,
            'company_type' => $company_type,
            'date_of_incorporation' => $date_of_incorporation,
            'company_status' => $company_status,
            'company_country' => $company_country,
            'company_new_address' => $company_new_address,
            'company_old_address' => $company_old_address,
            'company_logo' => $company_logo,
        );
        $rules = array(
            'company_name' => "required",
            'company_type' => "required",
            'date_of_incorporation' => "required",
            'company_status' => "required",
            'company_country' => "required",
            'company_new_address' => "required",
            'company_old_address' => "required",
        );
        $validate = Validator::make($inputs, $rules);

        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        } else {
            $company = CompanyModel::find($id);
            $company->company_name = $company_name;
            $company->previous_name = $previous_name;
            $company->company_type = $company_type;
            $company->date_of_incorporation = date('Y-m-d', strtotime($date_of_incorporation));
            $company->company_status = $company_status;
            $company->company_country = $company_country;
            $company->company_new_address = $company_new_address;
            $company->company_old_address = $company_old_address;
            $company->created_at = date('Y-m-d H:i:s');
            $company->created_by = Session::get('user_id');

            if ($company_logo != '') {

                $destinationPath = 'public/images/company_logo';
                $filename = rand() . $company_logo->getClientOriginalName();
                $upload_success = $company_logo->move($destinationPath, $filename);
                if ($upload_success) {
                    $company->company_logo = $filename;
                }
            }
            if ($company->save()) {
                $msg = "Company Info Successfully Updated !";
                Session::flash('success_update_msg', $msg);
                Session::flash('alert-class', 'alert-success');
                return redirect('details-company/' . $id);
            }
        }
    }

    public function listCompany($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $companymodel = new CompanyModel();
        $data = array(
            'pageTitle' => 'List Company',
            'breadcamps' => array('Company' => '#', 'List Company' => 'list-company/1'),
        );
        $companies = $companymodel->getallcompany($page);
        $data['companies'] = $companies;
        $data['count'] = $companymodel->allcompanycount();
//        die('count'.$data['count']);
        $data['page'] = $page;
        return view('company.list', $data);
    }

    public function details($id) {
        $country = Models\Country::all();
        $company = CompanyModel::find($id);
//        echo '<pre>';
//        print_r($company);
//        die;
        $data = array(
            'countries' => $country,
            'pageTitle' => 'Details Company',
            'breadcamps' => array('Company' => 'list-company/1', $company->company_name => 'details-company/' . $company->company_id),
        );
        $data['company'] = $company;
        return view('company.details', $data);
    }

    public function add_contact_person() {
        $company_id = Input::get('company_id');
        $name = Input::get('person_name');
        $email = Input::get('person_email');
        $contact = Input::get('person_contact_number');
        $designation = Input::get('person_designation');
        $type = Input::get('person_type');
        $doappointment = Input::get('doappointment');
        $doresignation = Input::get('doresignation');
        $director = Input::get('director');
        $shareholder = Input::get('shareholder');
        $secretary = Input::get('secretary');

        $company_contact = new Models\CompanyContactPersonModel();
        $company_contact->company_id = $company_id;
        $company_contact->person_name = $name;
        $company_contact->person_email = $email;
        $company_contact->person_contact_number = $contact;
        $company_contact->person_designation = $designation;
        $company_contact->contact_person_type = $type;
        $company_contact->doappointment = date('Y-m-d', strtotime($doappointment));
        $company_contact->doresignation = date('Y-m-d', strtotime($doresignation));
        $company_contact->director = $director;
        $company_contact->shareholder = $shareholder;
        $company_contact->secretary = $secretary;
        if ($company_contact->save()) {
            $msg = "Company Contact Person successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            echo json_encode(array('status' => true, 'data' => $company_contact));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function add_credit_invoice() {
        $company_id = Input::get('company_id');
        $title = Input::get('title');
        $description = Input::get('description');
        $date = Input::get('date');
        $hours = Input::get('hours');
        $status = Input::get('status');
        $company_credit = new Models\CompanyCreditInvoice();
        $company_credit->company_id = $company_id;
        $company_credit->invoice_title = $title;
        $company_credit->invoice_description = $description;
        $company_credit->invoice_hours = $hours;
        $company_credit->invoice_date = date('Y-m-d', strtotime($date));
        $company_credit->invoice_status = $status;
        if ($company_credit->save()) {
            $msg = "Company Credit Info successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            echo json_encode(array('status' => true, 'data' => $company_credit));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function billing_info_details() {
        $company_id = Input::get('company_id');
        $companycreditinvoicemodel = new CompanyCreditInvoice();
        $invoice_data = $companycreditinvoicemodel->getinvoicedetails($company_id);
        $companybillingconfigurationmodel = new CompanyBillingConfiguration();
        foreach ($invoice_data as $key => $inv):
            $invoice_data[$key]['rate'] = $companybillingconfigurationmodel->hourlyrate($company_id, $inv['invoice_date']);
        endforeach;
        $data['invoice_data'] = $invoice_data;
        $data['company_id'] = $company_id;
        return view('invoice.renderinvoice', $data);
    }

    public function billing_pdf() {
        $items = '';
        $company_id = $_POST['company_id'];
        $clientInfo = Custom::getClientInfo($company_id);
        $emailTo = $clientInfo->person_email;
        $companycreditinvoicemodel = new CompanyCreditInvoice();
        $selectedData = $_POST;
        $invoice_data = array();
        unset($selectedData['company_id']);
        unset($selectedData['_token']);

        foreach ($selectedData as $sd):
            $invoice_data[$sd] = $companycreditinvoicemodel->getinvoicedetailsById($sd);
//        $companycreditinvoicemodel->changeinvoicestatus($sd);
        endforeach;
        $companybillingconfigurationmodel = new CompanyBillingConfiguration();
        foreach ($invoice_data as $key => $inv):
            $invoice_data[$key]['rate'] = $companybillingconfigurationmodel->hourlyrate($company_id, $inv['invoice_date']);
            $invoice_data[$key]['invoice_date'] = date("d-m-Y", strtotime($invoice_data[$key]['invoice_date']));
        endforeach;

        $grandTotal = 0;
        $totalHours = 0;

        foreach ($invoice_data as $item) {
            $items .= '<tr>
                        <td style="text-align:left;">' . $item['id'] . '</td>
                        <td style="text-align:left;">' . $item['invoice_title'] . '</td>
                        <td>' . $item['invoice_hours'] . '</td>
                        <td>' . $item['rate'] . '</td>
                        <td>' . $item['invoice_hours'] * $item['rate'] . '</td>
                        <td>' . $item['invoice_date'] . '</td>
                    </tr>';
            $grandTotal = $grandTotal + $item['invoice_hours'] * $item['rate'];
            $totalHours = $totalHours + $item['invoice_hours'];
        }

        $averageRate = $grandTotal / $totalHours;

        $dt = array(
            'clientInfo' => $clientInfo,
            'grandTotal' => $grandTotal,
            'totalHours' => $totalHours,
            'averageRate' => $averageRate,
            'items' => $items,
        );
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf/billing', $dt);
//        return $pdf->stream();
        $filepath = url() . '/resources/assets/mails/';
        $filename = 'tempmail' . time() . '.pdf';
        file_put_contents('resources/assets/mails/' . $filename, $pdf->stream());
        ini_set('allow_url_fopen', '1');
        $data = array();
        $pathToFile = $filepath . $filename;
        Mail::send('admin.mailtemplate', $data, function($message) use($emailTo, $pathToFile) {
            $message->from('info@workspaceit.com', 'Laravel');
            $message->to($emailTo);
            $message->attach($pathToFile);
        });
        $msg = "A mail has been sent to " . $emailTo . " with attachment of billing invoice !";
        Session::flash('success_add_msg', $msg);
        Session::flash('alert-class', 'alert-success');
        return redirect('details-company/' . $company_id);
    }

    public function add_document($company_id) {

        $documentFile = Input::file('file');
        $document = new Models\CompanyDocumentModel();
        $document->company_id = $company_id;
        $document->document_title = Input::get('title');
        $document->document_description = Input::get('description');
        $document->document_type = Input::get('type');
        if ($documentFile != '') {

            $destinationPath = 'public/images/company_document';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->document_attached = $filename;
            }
        }
        if ($document->save()) {
            $msg = "Company Document successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function add_correspondence($company_id) {

        $documentFile = Input::file('file');
        $document = new Models\CompanyCorrespondenceModel();
        $document->company_id = $company_id;
        $document->correspondence_title = Input::get('title');
        $document->date_of_correspondence = date('Y-m-d', strtotime(Input::get('date')));
        $document->correspondence_description = Input::get('description');
        $document->correspondence_type = Input::get('type');
        $document->received_by = Input::get('received_by');
        $document->correspondence_content = Input::get('content');

        if ($documentFile != '') {

            $destinationPath = 'public/images/company_correspondence';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->correspondence_document = $filename;
            }
        }
        if ($document->save()) {
            $msg = "Company Correspondence successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function addCompliance($company_id) {
        $documentFile = Input::file('file');
        $document = new Models\CompanyComplianceReportModel();
        $document->company_id = $company_id;
        $document->title = Input::get('title');
        $document->description = Input::get('description');
        $document->uploaded_by = Session::get('user_id');

        if ($documentFile != '') {

            $destinationPath = 'public/images/company_compliance';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->document_attachment = $filename;
            }
        }
        if ($document->save()) {
            $msg = "Company Compliance Report successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function addIncorporation($company_id) {
        $documentFile = Input::file('file');
        $document = new Models\CompanyIncorporationModel();
        $document->company_id = $company_id;
        $document->title = Input::get('title');
        $document->description = Input::get('description');
        $document->uploaded_by = Session::get('user_id');

        if ($documentFile != '') {

            $destinationPath = 'public/images/company_incorporation';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->filename = $filename;
            }
        }
        if ($document->save()) {
            $msg = "Company Incorporation successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function editInvoice($id) {
        $companies = CompanyModel::all();
        $credit_invoice = Models\CompanyCreditInvoice::find($id);
        $company = CompanyModel::find($credit_invoice->company_id);
        $data = array(
            'credit_invoice' => $credit_invoice,
            'pageTitle' => 'Edit Invoice',
            'breadcamps' => array('Company' => '#', $company->company_name => 'details-company/' . $company->company_id, 'Edit Invoice' => 'edit-credit-invoice' . "/" . $id),
        );
        $data['companies'] = $companies;
        return view('company.edit_invoice', $data);
    }

    public function updateInvoice($id) {
        $title = Input::get('invoice_title');
        $description = Input::get('invoice_description');
        $date = Input::get('invoice_date');
        $hours = Input::get('invoice_hours');
        $status = Input::get('invoice_status');
        $company_credit = Models\CompanyCreditInvoice::find($id);
        $company_credit->invoice_title = $title;
        $company_credit->invoice_description = $description;
        $company_credit->invoice_hours = $hours;
        $company_credit->invoice_date = date('Y-m-d', strtotime($date));
        $company_credit->invoice_status = $status;
        if ($company_credit->save()) {
            $msg = "Company Credit Info successfully update !";
            Session::flash('success_update_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect('details-company/' . $company_credit->company_id);
        }
    }

    public function delete($id) {
        $company = CompanyModel::find($id);
        if ($company->delete()) {
            $msg = "Company Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect('list-company/1');
        }
    }

    public function addTemplate($id) {

        $document = new Models\CompanyTemplate();
        $document->company_id = $id;
        $document->type = Input::get('template_type');
        $document->uploaded_by = Session::get('user_id');
        $documentFile = Input::file('file');

        if ($documentFile != '') {

            $destinationPath = 'public/images/template';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->filename = $filename;
            }
        }

        if ($document->save()) {
            $msg = "Company template successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            //echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function addStatement($id) {

        $document = new Models\CompanyBankStatements();
        $document->company_id = $id;
        $document->title = Input::get('statement_title');
        $document->date_of_issue = date("Y-m-d", strtotime(Input::get('statement_doi')));
        $document->uploaded_by = Session::get('user_id');
        $documentFile = Input::file('file');
        $document->status = 1;

        if ($documentFile != '') {

            $destinationPath = 'public/images/statement';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->statement = $filename;
            }
        }

        if ($document->save()) {
            $msg = "Company bank statement successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            //echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function addTaxReturn($id) {
        //echo "die";die("smile");
        $document = new Models\CompanyTaxReturn();
        $document->company_id = $id;
        $document->client_name = Input::get('client_name');
        $document->a_c_name = Input::get('a_c_name');
        $document->taxation_date = date("Y-m-d", strtotime(Input::get('taxation_date')));
        $document->uploaded_by = Session::get('user_id');
        $documentFile = Input::file('file');

        if ($documentFile != '') {

            $destinationPath = 'public/images/taxreturn';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->filename = $filename;
            }
        }

        if ($document->save()) {
            $msg = "Company Tax Return successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            //echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function addFinancialStatements($id) {
        //echo "die";die("smile");
        $document = new Models\CompanyFinancialStatements();
        $document->company_id = $id;
        $document->client_name = Input::get('client_name');
        $document->bank_name = Input::get('bank_name');
        $document->issue_date = date("Y-m-d", strtotime(Input::get('issue_date')));
        $document->uploaded_by = Session::get('user_id');
        $documentFile = Input::file('file');

        if ($documentFile != '') {

            $destinationPath = 'public/images/financial_statements';
            $filename = rand() . $documentFile->getClientOriginalName();
            $upload_success = $documentFile->move($destinationPath, $filename);
            if ($upload_success) {
                $document->filename = $filename;
            }
        }

        if ($document->save()) {
            $msg = "Company Tax Return successfully added !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            //echo json_encode(array('status' => true, 'data' => $document));
        }
    }

    public function deleteTaxreturn($id) {
        $company = Models\CompanyTaxReturn::find($id);
        $filepath = 'public/images/taxreturn/';

        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Company Tax Return Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function FinancialStatements($id) {
        $company = Models\CompanyFinancialStatements::find($id);
        $filepath = 'public/images/financial_statements/';

        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Company Financial Statements Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deletePerson($id) {
        $company = Models\CompanyContactPersonModel::find($id);
        if ($company->delete()) {
            $msg = "Company Person Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteTemplate($id) {
        $company = Models\CompanyTemplate::find($id);
        $filepath = 'public/images/template/';

        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Company tamplate successfully deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteStatement($id) {
        $company = Models\CompanyBankStatements::find($id);
        $filepath = 'public/images/statement/';

        if (File::exists($filepath . $company->statement)) {
            File::delete($filepath . $company->statement);
        }
        if ($company->delete()) {
            $msg = "Company bank statement successfully deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteCompliance($id) {
        $company = Models\CompanyComplianceReportModel::find($id);
        $filepath = 'public/images/company_compliance/';

        if (File::exists($filepath . $company->document_attachment)) {
            File::delete($filepath . $company->document_attachment);
        }
        if ($company->delete()) {
            $msg = "Company compliance reports successfully deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteIncorporation($id) {
        $company = Models\CompanyIncorporationModel::find($id);
        $filepath = 'public/images/company_incorporation/';

        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Company Incorporation successfully deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteCorrespondence($id) {
        $company = Models\CompanyCorrespondenceModel::find($id);

        $filepath = 'public/images/company_correspondence';
        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Company Correspondence Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteDocument($id) {
        $company = Models\CompanyDocumentModel::find($id);

        $filepath = 'public/images/company_document';
        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Company Document Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

    public function deleteInvoice($id) {
        $company = Models\CompanyCreditInvoice::find($id);
        if ($company->delete()) {
            $msg = "Company Credit Invoice Successfully Deleted !";
            Session::flash('success_delete_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

}
