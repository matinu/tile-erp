<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use Illuminate\Pagination\Paginator;
use App\Models;
use App\Models\CompanyBillingConfiguration;
use App\Models\CompanyModel;
use Models\BankAccountOpening;
use \Illuminate\Support\Facades\File;

class BankAccountController extends Controller {

    public function index($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $list = new Models\BankAccountOpening();
        $data = array(
            'pageTitle' => 'Open Bank Account',
            'breadcamps' => array('Bank Account' => 'list-bank-account/1'),
        );
        $data['list'] = $list->pagesearch($page);
        $data['count'] = Models\BankAccountOpening::all()->count();
        $data['page'] = $page;
//        $data['list'] = Role::all();
        return view('bankAccount/list', $data);
    }

    public function create() {
        if ($_POST) {
            $documentFile = Input::file('attached');
            $document = new Models\BankAccountOpening();
            $document->user_id = Session::get('user_id');
            $document->bank_name = Input::get('bank_name');

            if ($documentFile != '') {

                $destinationPath = 'public/images/bank_opening';
                $filename = rand() . $documentFile->getClientOriginalName();
                $upload_success = $documentFile->move($destinationPath, $filename);
                if ($upload_success) {
                    $document->filename = $filename;
                }
            }
            if ($document->save()) {
                $msg = "Bank Account successfully added !";
                Session::flash('success_add_msg', $msg);
                Session::flash('alert-class', 'alert-success');
                return redirect('list-bank-account/1');
                //  echo json_encode(array('status' => true, 'data' => $document));
            }
        } else {
            $data = array(
                'pageTitle' => 'Open Bank Account',
                'breadcamps' => array('Bank Account' => 'list-bank-account/1', 'Create Bank Account' => 'bank-account-open'),
            );
            return view('bankAccount/create', $data);
        }
    }
    public function delete($id) {
        $company = Models\BankAccountOpening::find($id);
        $filepath = 'public/images/bank_opening/';
        if (File::exists($filepath . $company->filename)) {
            File::delete($filepath . $company->filename);
        }
        if ($company->delete()) {
            $msg = "Bank Account Details Successfully Deleted !";
            Session::flash('success_add_msg', $msg);
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
        }
    }

}
