<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use App\Models;
use App\Models\LoginModel;
use App\Models\UserModel;
use App\Models\UserDetailsModel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller {

    public function index() {
        
    }

    public function create_user() {
        if (!isset($_POST['name'])) {
            $data = array(
                'pageTitle' => 'Add User',
                'breadcamps' => array('User Management' => 'list-user/1', 'Add User' => 'create-user'),
            );
            return view('admin/user', $data);
        } else {
            $inputs = array(
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'confirmation_password' => Input::get('confirmation_password'),
                'contact' => Input::get('cnumber'),
                'dob' => Input::get('dob')
            );
            $rules = array(
                'name' => "required",
                'email' => "required|email|unique:users",
                'password' => "required|min:6",
                'confirmation_password' => "required|same:password",
                'contact' => "required|numeric",
                'dob' => "required"
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
//                echo 'hello';
                unset($inputs['confirmation_password']);
                $inputs['dob'] = date('Y-m-d', strtotime($inputs['dob']));
                $loginModel = new LoginModel();
                $userdetailsid = $loginModel->adduser($inputs);
                $this->sendemail($userdetailsid, $inputs['email']);
//                $data = array(
//                    'pageTitle' => 'Add User',
//                    'breadcamps' => array('User List' => 'list-user', 'Create List' => 'create-user'),
//                );
                Session::flash('success_message', 'New user has been created');
                Session::flash('alert-class', 'alert-success');
                return redirect('/list-user/1');
//                return view('admin/user', $data);
            }
        }
    }

    public function edit_user($userid = 0) {
        if (!isset($_POST['name'])) {
            $user = new UserModel();
            $userdata = $user->getuserbyid($userid);
            $data = array(
                'pageTitle' => 'Edit User',
                'breadcamps' => array('User Management' => 'list-user/1', 'Edit User' => 'edit-user/' . $userid),
                'userdata' => $userdata,
            );
            return view('admin/edituser', $data);
        } else {
            $inputs = array(
                'id' => Input::get('id'),
                'name' => Input::get('name'),
                'contact' => Input::get('cnumber'),
                'dob' => Input::get('dob')
            );

//            echo 'user id = '.$inputs['id'];
            $rules = array(
                'name' => "required",
                'contact' => "required",
                'dob' => "required"
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                unset($inputs['confirmation_password']);
                $inputs['dob'] = date('Y-m-d', strtotime($inputs['dob']));
                $user = new UserModel();
                $user->edituser($inputs);

                Session::flash('success_message', 'User information has been updated');
                Session::flash('alert-class', 'alert-success');
                return redirect('/list-user/1');
            }
        }
    }

    public function manage_user($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $user = new UserModel();
        $userdata = $user->getalluserdata($page);
        $data = array(
            'pageTitle' => 'List User',
            'breadcamps' => array('User Management' => '#', 'List Users' => 'list-user/1'),
            'userdata' => $userdata,
        );
        $data['count'] = $user->allusercount();
        $data['page'] = $page;
//        echo $data['count'];
//        die($data['page']);
        return view('admin/manageuser', $data);
    }

    public function delete_user($userid) {
        $user = new UserModel();
        $userdata = $user->deleteuser($userid);
        $user = new UserModel();
        $userdata = $user->getalluserdata();


        Session::flash('success_message', 'User deleted');
        Session::flash('alert-class', 'alert-success');
        return redirect('/list-user/1');
    }

    public function ban_user($userdetailsid) {
        $userdetails = UserDetailsModel::find($userdetailsid);
        $userdetails->status = 2;
        $userdetails->save();

        Session::flash('success_message', 'User has been banned');
        Session::flash('alert-class', 'alert-success');
        return redirect('/list-user/1');
    }

    public function unban_user($userdetailsid) {
        $userdetails = UserDetailsModel::find($userdetailsid);
        $userdetails->status = 1;
        $userdetails->save();

        Session::flash('success_message', 'User has been unbanned');
        Session::flash('alert-class', 'alert-success');
        return redirect('/list-user/1');
    }

    public function sendemail($userdetailsid = 0, $useremail) {
        /* $encrypted = Crypt::encrypt($userdetailsid);
          $link = url() . "/validateemail/" . $encrypted;
          $to = $useremail;
          $subject = "Account Activation";
          //echo $link;
          $body = "
          <html>
          <head>
          <title>Validate Email</title>
          </head>
          <body>
          <p>Please follow the next link to activate your account:</p>
          <p><a href='" . $link . "' target='_blank'>Click Here</a></p>
          <p>If you haven't registered here please ignore this email.</p>
          </body>
          </html>
          ";

          // Always set content-type when sending HTML email
          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

          // More headers
          $headers .= 'From: <info@workspaceit.com>' . "\r\n";

          mail($to, $subject, $message, $headers);
          /* Mail::send('emails.welcome', ['key' => 'value'], function($mail) {
          $mail->to(Input::get('email'), $body)->subject($headers);
          });
         *//*
          //        Mail::raw($body, function($message) {
          //            $message->from('info@workspaceit.com', 'Til-erp');
          //
          //            $message->to(Input::get('email'));
          //        });


          $data = array(
          'link' => $link,
          );
          Mail::send('admin.addusermail', $data, function($message) use($to) {
          $message->from('info@workspaceit.com', 'Laravel');
          $message->to($to);
          }); */

        Mail::raw("TEST", function($message) {
            $message->from('info@workspaceit.com', 'Til-erp');

            $message->to("shurid84@gmail.com");
        });
    }

    public function validateemail($userdetailsid) {
        $decrypted = Crypt::decrypt($userdetailsid);
        $loginModel = new LoginModel();
        $userid = $loginModel->validateemail($decrypted);
        return redirect('login');
    }

}
