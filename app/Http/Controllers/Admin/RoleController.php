<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use App\Role;
use App\User;
use App\Permission;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;

class RoleController extends Controller {

    public function index($page = 0) {
        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $role = new Role();
        $data = array(
            'pageTitle' => 'Role List',
            'breadcamps' => array('Role' => '#', 'Role List' => 'list-role/1'),
        );
        $data['list'] = $role->getallroles($page);
        $data['count'] = $role->allrolecount();
        $data['page'] = $page;
//        $data['list'] = Role::all();
        return view('Role/list', $data);
    }

    public function Create_Role() {
        if (!isset($_POST['name'])) {
            $data = array(
                'pageTitle' => 'Add New Role',
                'breadcamps' => array('Role' => 'list-role/1', 'Add Role' => 'create-Role'),
            );
            return view('Role/create', $data);
        } else {
            $inputs = array(
                'name' => Input::get('name'),
                'display_name' => Input::get('display_name'),
                'description' => Input::get('description'),
            );
            $rules = array(
                'name' => "required|unique:roles",
                'display_name' => "required",
                'description' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                $role = new Role();
                $role->name = Input::get('name');
                $role->display_name = Input::get('display_name');
                $role->description = Input::get('description');
                $role->save();

                Session::flash('msg', 'Role has been Created.');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('list-role/1'));
            }
        }
    }

    public function edit_Role($id = 0, Request $request) {
        if ($id == 0):
            return redirect(url('list-role/1'));
        endif;
        if (!$request->isMethod('post')) {
            $data = array(
                'pageTitle' => 'Edit Role',
                'breadcamps' => array('Role' => 'list-role/1', 'Edit Role' => 'edit-role/' . $id),
            );
            $data['role'] = Role::find($id);
            return view('Role/edit', $data);
        } else {
            $inputs = array(
                'display_name' => Input::get('display_name'),
                'description' => Input::get('description'),
            );
            $rules = array(
                'display_name' => "required",
                'description' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {

                $role = Role::find($id);
                $role->display_name = Input::get('display_name');
                $role->description = Input::get('description');
                $role->save();
                Session::flash('msg', 'Role has been Updated.');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('list-role/1'));
            }
        }
    }

    public function delete_Role($id) {
        Role::destroy($id);
        Session::flash('msg', 'Role has been Deleted.');

        Session::flash('alert-class', 'alert-success');
        return redirect(url('list-role/1'));
    }

    public function set_Role(Request $request) {
        if (!$request->isMethod('post')) {
            $data = array(
                'pageTitle' => 'User role set',
                'breadcamps' => array('Role' => 'list-role/1', 'View User Role' => 'view-user-role', 'Set Role' => 'set-role'),
            );
            $data['roles'] = Role::all();
            $user_role = DB::table('role_user')->select('user_id')->get();
            $result = array();
            foreach ($user_role as $key => $value) {
                $result[] = $value->user_id;
            }
            $data['users'] = DB::table('users')->whereNotIn('id', $result)->get();
            return view('Role/set', $data);
        } else {
            $inputs = array(
                'role' => Input::get('role'),
                'user' => Input::get('user'),
            );
            $rules = array(
                'role' => "required",
                'user' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                $user = User::where('id', '=', Input::get('user'))->first();
                $user->roles()->attach(Input::get('role'));
                Session::flash('msg', 'Role has been set to user');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('view-user-role'));
            }
        }
    }

    public function view_UserRole() {
        $data = array(
            'pageTitle' => 'Role List',
            'breadcamps' => array('Role' => 'list-role/1', 'View User Role' => 'view-user-role'),
        );
        $check = DB::table('role_user')->get();
        if (sizeof($check) == 0) {
            Session::flash('msg', 'No role hasn\'t been set for any user. Please set once');

            Session::flash('alert-class', 'alert-danger');
            return redirect(url('set-role'));
        }
        $data['list'] = User::with('roles')->get();
        return view('Role/list_userrole', $data);
    }

    public function edit_UserRole($id, Request $request) {
        if ($id == 0):
            return redirect(url('view-user-role'));
        endif;

        if (!$request->isMethod('post')) {
            $data = array(
                'pageTitle' => 'User role set',
                'breadcamps' => array('Role' => 'list-role/1', 'View User Role' => 'view-user-role', 'Edit User Role' => 'edit-user-role/' . $id),
            );
            $data['roles'] = Role::all();
            $data['userrole'] = DB::table('role_user')->Where('user_id', $id)->get();
            $data['users'] = User::all();
            return view('Role/edit_userrole', $data);
        } else {
            $inputs = array(
                'role' => Input::get('role'),
                'user' => Input::get('user'),
            );
            $rules = array(
                'role' => "required",
                'user' => "required",
            );
            $validate = Validator::make($inputs, $rules);

            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                $user = User::where('id', '=', Input::get('user'))->first();
                $user->roles()->sync(array(Input::get('role')));
                Session::flash('msg', 'Role has been Update for user');

                Session::flash('alert-class', 'alert-success');
                return redirect(url('view-user-role'));
            }
        }
    }

    public function delete_UserRole($id) {
        $userrole = DB::table('role_user')->Where('user_id', $id)->delete();
        Session::flash('msg', 'Role has been Deleted for User.');

        Session::flash('alert-class', 'alert-success');
        return redirect(url('view-user-role'));
    }

}
