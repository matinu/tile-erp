<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use illuminate\html;
use App\Models;
use App\Models\CompanyComplianceReportModel;
use App\Models\CompanyIncorporationModel;
use App\Models\CompanyTemplate;
use App\Models\CompanyCreditInvoice;
use App\Models\CompanyContactPersonModel;
use App\Models\CompanyCorrespondenceModel;
use App\Models\CompanyDocumentModel;
use App\Models\CompanyModel;
use App\Models\CompanyFinancialStatements;
use Illuminate\Support\Facades\Crypt;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
//use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
Use App\Models\CompanyBankStatements;
use \Illuminate\Support\Facades\Request;

class CompanyDetailsController extends Controller {

    public function index() {
        
    }

    public function correspondance_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['title'] = Input::get('titlefind');
            $cond['type'] = Input::get('correspondence_type');
            $cond['todate'] = Input::get('todate');
            $cond['fromdate'] = Input::get('fromdate');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'date_of_correspondence between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'date_of_correspondence = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'date_of_correspondence = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'correspondence_title like "%' . $cond['title'] . '%"';
            }
            if ($cond['type'] != '') {
                $search['type'] = 'correspondence_type = "' . $cond['type'] . '"';
            }
        }
        $data = array(
            'pageTitle' => 'Company Correspondance',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Correspondence' => 'correspondance-view/' . $id . '/1'),
        );
        $company_name = new CompanyCorrespondenceModel();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyCorrespondenceModel::where('company_id', $id)->count();
        return view('CompanyDetails/correspondance', $data);
    }

    public function document_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['title'] = Input::get('titlefind');
            $cond['type'] = Input::get('document_type');
            $cond['todate'] = Input::get('todate');
            $cond['fromdate'] = Input::get('fromdate');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'document_title like "%' . $cond['title'] . '%"';
            }
            if ($cond['type'] != '') {
                $search['type'] = 'document_type = "' . $cond['type'] . '"';
            }
        }
        $data = array(
            'pageTitle' => 'Company Documents',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Document' => 'document-view/' . $id . '/1'),
        );
        $company_name = new CompanyDocumentModel();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyDocumentModel::where('company_id', $id)->count();
        return view('CompanyDetails/document', $data);
    }

    public function contact_person_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['name'] = Input::get('name');
            $cond['email'] = Input::get('email');
            $cond['type'] = Input::get('person_type');
            if ($cond['name'] != '') {
                $search['name'] = 'person_name like "%' . $cond['name'] . '%"';
            }
            if ($cond['email'] != '') {
                $search['email'] = 'person_email like "%' . $cond['email'] . '%"';
            }
            if ($cond['type'] != '') {
                $search['type'] = 'contact_person_type = ' . $cond['type'];
            }
        }
        $data = array(
            'pageTitle' => 'Company Contact Person',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Contact Person' => 'contact-person-view/' . $id . '/1'),
        );
        $company_name = new CompanyContactPersonModel();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyContactPersonModel::where('company_id', $id)->count();
        return view('CompanyDetails/contactperson', $data);
    }

    public function invoices_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['fromdate'] = Input::get('fromdate');
            $cond['todate'] = Input::get('todate');
            $cond['title'] = Input::get('titlefind');
            $cond['invoice_status'] = Input::get('invoice_status');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'invoice_date between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'invoice_date = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'invoice_date = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'invoice_title like "%' . $cond['title'] . '%"';
            }
            if ($cond['invoice_status'] != '') {
                $search['invoice_status'] = 'invoice_status = "' . $cond['invoice_status'] . '"';
            }
        }
        $data = array(
            'pageTitle' => 'Company Credit Invoices',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Credit Invoice' => 'invoices-view/' . $id . '/1'),
        );
        $company_name = new CompanyCreditInvoice();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyCreditInvoice::where('company_id', $id)->count();
        return view('CompanyDetails/creditinvoice', $data);
    }

    public function templates_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['template_type'] = Input::get('template_type');
            $cond['todate'] = Input::get('todate');
            $cond['fromdate'] = Input::get('fromdate');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['template_type'] != '') {
                $search['template_type'] = 'type = ' . $cond['template_type'];
            }
        }
        $data = array(
            'pageTitle' => 'Template Document Details',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Template Document' => 'templates-view/' . $id . '/1'),
        );
        $company_name = new CompanyTemplate();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
//          echo "<pre>";
//         print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyTemplate::where('company_id', $id)->count();
        return view('CompanyDetails/templates', $data);
    }

    public function statements_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['fromdate'] = Input::get('fromdate');
            $cond['todate'] = Input::get('todate');
            $cond['title'] = Input::get('titlefind');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'date_of_issue between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'date_of_issue = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'date_of_issue = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'title like "%' . $cond['title'] . '%"';
            }
        }
        $data = array(
            'pageTitle' => 'Bank Statement Details',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Bank Statement' => 'statements-view/' . $id . '/1'),
        );
        $company_name = new CompanyBankStatements();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyBankStatements::where('company_id', $id)->count();
        return view('CompanyDetails/statements', $data);
    }

    public function tax_return_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['fromdate'] = Input::get('fromdate');
            $cond['todate'] = Input::get('todate');
            $cond['title'] = Input::get('titlefind');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'taxation_date between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'taxation_date = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'taxation_date = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'client_name like "%' . $cond['title'] . '%"';
            }
        }
        $data = array(
            'pageTitle' => 'Annual Tax Return Details',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Annual Tax Return' => 'tax-return-view/' . $id . '/1'),
        );
        $company_name = new Models\CompanyTaxReturn();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyBankStatements::where('company_id', $id)->count();
        return view('CompanyDetails/taxreturn', $data);
    }

    public function financial_statements_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['fromdate'] = Input::get('fromdate');
            $cond['todate'] = Input::get('todate');
            $cond['title'] = Input::get('titlefind');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'issue_date between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'issue_date = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'issue_date = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'client_name like "%' . $cond['title'] . '%"';
            }
        }
        $data = array(
            'pageTitle' => 'Financial Statements Details',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Financial Statements' => 'tax-return-view/' . $id . '/1'),
        );
        $company_name = new CompanyFinancialStatements();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
        //  echo "<pre>";
        // print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyBankStatements::where('company_id', $id)->count();
        return view('CompanyDetails/financialStatements', $data);
    }

    public function incorporation_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['title'] = Input::get('titlefind');
            $cond['todate'] = Input::get('todate');
            $cond['fromdate'] = Input::get('fromdate');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'title like "%' . $cond['title'] . '%"';
            }
        }
        $data = array(
            'pageTitle' => 'Incorporation Details',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Incorporation' => 'incorporation-view/' . $id . '/1'),
        );
        $company_name = new CompanyIncorporationModel();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);
//          echo "<pre>";
//         print_r($company);die;
        $data['company'] = $company;
        $data['count'] = CompanyIncorporationModel::where('company_id', $id)->count();
        return view('CompanyDetails/incorporation', $data);
    }

    public function compliance_view($id = 0, $page = 0) {
        $search = array();
        if ($_POST) {
            $cond['title'] = Input::get('titlefind');
            $cond['todate'] = Input::get('todate');
            $cond['fromdate'] = Input::get('fromdate');
            if ($cond['fromdate'] != '') {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at between "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '" and "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                } else {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['fromdate'])) . '"';
                }
            } else {
                if ($cond['todate'] != '') {
                    $search['date'] = 'created_at = "' . date("Y-m-d H:i:s", strtotime($cond['todate'])) . '"';
                }
            }
            if ($cond['title'] != '') {
                $search['title'] = 'title like "%' . $cond['title'] . '%"';
            }
        }
        $data = array(
            'pageTitle' => 'Compliance Reports Details',
            'breadcamps' => array('Company' => 'list-company/1', 'Company Details' => 'details-company/' . $id, 'Company Compliance Report' => 'compliance-view/' . $id . '/1'),
        );
        $company_name = new CompanyComplianceReportModel();

        if ($page != 0) {
            $page = $page;
        } else {
            $page = 1;
        }
        $data['page'] = $page;
        $company = $company_name->pagesearch($id, $page, $search);

        $data['company'] = $company;
        $data['count'] = CompanyComplianceReportModel::where('company_id', $id)->count();
        return view('CompanyDetails/compliance', $data);
    }

}
