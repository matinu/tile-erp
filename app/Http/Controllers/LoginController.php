<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\html;
use App\Models;
use App\Models\LoginModel;
use Illuminate\Routing\Controller as BaseController;
use \Illuminate\Http\Request;

class LoginController extends BaseController {

    public function __construct() {

        if (Session::get('user_email'))
            return redirect(url('list-user'));
    }

    public function index(Request $request) {
        if ($request->isMethod('post')) {
            $inputs['email'] = Input::get('email');
            $inputs['password'] = Input::get('password');
            $rules = array(
                'email' => "required",
                'password' => "required",
            );
            $validate = Validator::make($inputs, $rules);
            if ($validate->fails()) {
                return redirect()->back()->withInput()->withErrors($validate);
            } else {
                $loginModel = new LoginModel();
                $userData = $loginModel->checkuservalidation($inputs);
                if (count($userData) == 0 || $userData[0]['status'] != 1) {    //if no data found in users table or users_details table
                    if (count($userData) == 0) {
                        $data['invalid'] = -1;
                    } else
                        $data['invalid'] = $userData[0]['status'];
                    $data['email'] = Input::get('email');
                    $data['password'] = Input::get('password');
                    return view('login/login', $data);
                } else {    //if it is a valid user
                    $sessionData['user_id'] = $userData[0]['id'];
                    $sessionData['user_email'] = $userData[0]['email'];
                    $sessionData['user_name'] = $userData[0]['name'];
                    $sessionData['created_at'] = $userData[0]['created_at'];
                    Session::put($sessionData);
                    return redirect(url('dashboard'));
                }
            }
        } else {
            return view('login/login');
        }
    }

    public function logout() {
        Session::flush();
        return redirect('login')->send();
    }

}
