@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Company Correspondence</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="">
                    <div class="col-sm-2">
                        <input type="text" id="fromdate" name="fromdate" value="{{@$_POST['fromdate']}}" class="form-control" placeholder="Date from">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="todate" name="todate" value="{{@$_POST['todate']}}" class="form-control" placeholder="Date to">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="titlefind" name="titlefind" value="{{@$_POST['titlefind']}}" class="form-control" placeholder="Title">
                    </div>
                    <div class="col-sm-3">
<!--                        <input type="text" id="typefind" class="form-control" placeholder="Type">-->
                        <select title="Select correspondence type" class="form-control" id="correspondence_type" name="correspondence_type">
                            <option value="">Select type</option>
                            <option value="Letters of inquiry" @if(@$_POST['correspondence_type']=="Letters of inquiry"){{"selected='selected'"}}@endif>Letters of inquiry</option>
                            <option value="Letters of claim/complaints" @if(@$_POST['correspondence_type']=="Letters of claim/complaints"){{"selected='selected'"}}@endif>Letters of claim/complaints</option>
                            <option value="Letters of Application" @if(@$_POST['correspondence_type']=="Letters of Application"){{"selected='selected'"}}@endif>Letters of Application</option>
                            <option value="Letters of approval/dismissal" @if(@$_POST['correspondence_type']=="Letters of approval/dismissal"){{"selected='selected'"}}@endif>Letters of approval/dismissal</option>
                            <option value="Letters of recommendations" @if(@$_POST['correspondence_type']=="Letters of recommendations"){{"selected='selected'"}}@endif>Letters of recommendations</option>
                            <option value="Letters of Sales" @if(@$_POST['correspondence_type']=="Letters of Sales"){{"selected='selected'"}}@endif>Letters of Sales</option>
                        </select>
                    </div>
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="float: right;" class="btn btn-success" value="Search">
                </form>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th>Content</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($company as $key=>$correspondence)
                        <tr>
                            <td> {{date("jS F, Y", strtotime($correspondence->date_of_correspondence))}}</td>
                            <td> {{$correspondence->correspondence_title}}</td>
                            <td> {{$correspondence->correspondence_description}}</td>
                            <td> {{$correspondence->correspondence_type}}</td>
                            <td>
                                @if($correspondence->received_by=='document')
                                <a href="{{URL::to('public/images/company_correspondence')."/".$correspondence->correspondence_document}}"
                                   download="{{$correspondence->correspondence_document}}" class="btn btn-primary">Download</a>&nbsp

                                @endif

                                @if($correspondence->received_by=='email')
                                <button type="button" class="btn btn-sm btn-default" data-placement="top" data-toggle="popover"  data-content="{{ strip_tags($correspondence->correspondence_content)}}">Show</button>
                                @endif
                            </td>
                            <td>
                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-correspondence')."/".$correspondence->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('CompanyDetails.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "info": false,
            "searching": false,
        });


        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });

    $(document).ready(function () {
        $("#fromdate").datepicker({endDate: new Date()});
        $("#todate").datepicker({endDate: new Date()});
    });
</script>
@stop