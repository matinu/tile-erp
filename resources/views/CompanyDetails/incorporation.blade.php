@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Incorporation Details</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="">
                    <div class="col-sm-2">
                        <input type="text" id="fromdate" name="fromdate" value="{{@$_POST['fromdate']}}" class="form-control datepicker" placeholder="Date from">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="todate" name="todate" value="{{@$_POST['todate']}}"  class="form-control datepicker" placeholder="Date to">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="titlefind" name="titlefind" value="{{@$_POST['titlefind']}}" class="form-control" placeholder="Title">
                    </div>
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="float: right;" class="btn btn-success" value="Search">
                </form>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Uploaded By</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($company as $key=>$doc)
                        <tr>
                            <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                            <td> {{$doc->email}}</td>
                            <td> {{$doc->title}}</td>
                            <td> {{$doc->description}}</td>
                            <td>
                                <a href="{{URL::to('public/images/company_incorporation')."/".$doc->filename}}"
                                   download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp<a
                                   data-toggle="lightbox"
                                   href="{{URL::to('public/images/company_incorporation')."/".$doc->filename}}"
                                   class="btn btn-primary">View</a>

                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-incorporation')."/".$doc->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('CompanyDetails.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "info": false,
            "searching": false,
        });


        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop