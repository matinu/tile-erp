@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Company Document</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="">
                    <div class="col-sm-2">
                        <input type="text" id="fromdate" name="fromdate" value="{{@$_POST['fromdate']}}" class="form-control datepicker" placeholder="Date from">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="todate" name="todate" value="{{@$_POST['todate']}}"  class="form-control datepicker" placeholder="Date to">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="titlefind" name="titlefind" value="{{@$_POST['titlefind']}}" class="form-control" placeholder="Title">
                    </div>
                    <div class="col-sm-3">
                        <select title="Select Document type" class="form-control" id="document_type" name="document_type">
                            <option value="">Select type</option>
                            <option value="KYC" @if(@$_POST['document_type']=="KYC"){{"selected='selected'"}}@endif>KYC</option>
                            <option value="Statutory" @if(@$_POST['document_type']=="Statutory"){{"selected='selected'"}}@endif>Statutory</option>
                            <option value="Minutes Book" @if(@$_POST['document_type']=="Minutes Book"){{"selected='selected'"}}@endif>Minutes Book</option>
                            <option value="Register" @if(@$_POST['document_type']=="Register"){{"selected='selected'"}}@endif>Register</option>
                            <option value="Bank Statementss" @if(@$_POST['document_type']=="Bank Statementss"){{"selected='selected'"}}@endif>Bank Statements</option>
                            <option value="Correspondence" @if(@$_POST['document_type']=="Correspondence"){{"selected='selected'"}}@endif>Correspondence</option>
                            <option value="Incorporation Documents" @if(@$_POST['document_type']=="Incorporation Documents"){{"selected='selected'"}}@endif>Incorporation Documents</option>
                            <option value="Bank Account Opening Documents" @if(@$_POST['document_type']=="Bank Account Opening Documents"){{"selected='selected'"}}@endif>Bank Account Opening Documents</option>
                            <option value="Contracts &amp; Receipts of Payment" @if(@$_POST['document_type']=="Contracts & Receipts of Payment"){{"selected='selected'"}}@endif>Contracts &amp; Receipts of Payment</option>
                            <option value="Invoices" @if(@$_POST['document_type']=="Invoices"){{"selected='selected'"}}@endif>Invoices</option>
                            <option value="Accounting" @if(@$_POST['document_type']=="Accounting"){{"selected='selected'"}}@endif>Accounting</option>
                            <option value="Statutory Filing" @if(@$_POST['document_type']=="Statutory Filing"){{"selected='selected'"}}@endif>Statutory Filing</option>
                            <option value="Others" @if(@$_POST['document_type']=="Others"){{"selected='selected'"}}@endif>Others</option>
                        </select>
                    </div>
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="float: right;" class="btn btn-success" value="Search">
                </form>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($company as $key=>$doc)
                        <tr>
                             <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                            <td> {{$doc->document_title}}</td>
                            <td> {{$doc->document_description}}</td>
                            <td> {{$doc->document_type}}</td>
                            <td>
                                <a href="{{URL::to('public/images/company_document')."/".$doc->document_attached}}"
                                   download="{{$doc->document_attached}}" class="btn btn-primary">Download</a>&nbsp<a
                                   data-toggle="lightbox"
                                   href="{{URL::to('public/images/company_document')."/".$doc->document_attached}}"
                                   class="btn btn-primary">View</a>

                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-document')."/".$doc->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('CompanyDetails.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "info": false,
            "searching": false,
        });


        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop