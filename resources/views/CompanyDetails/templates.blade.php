@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Template Document Details</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="">
                    <div class="col-sm-2">
                        <input type="text" id="fromdate" name="fromdate" value="{{@$_POST['fromdate']}}" class="form-control datepicker" placeholder="Date from">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="todate" name="todate" value="{{@$_POST['todate']}}"  class="form-control datepicker" placeholder="Date to">
                    </div>
                    <div class="col-sm-3">
                        <select id="template_type" name="template_type" class="form-control">
                            <option value="">---Select Type---</option>
                            <option value="1" @if(@$_POST['template_type']==1){{"selected='selected'"}}@endif>FSC</option>
                            <option value="2" @if(@$_POST['template_type']==2){{"selected='selected'"}}@endif>ROC</option>
                            <option value="3" @if(@$_POST['template_type']==3){{"selected='selected'"}}@endif>Minutes Of Meetings</option>
                        </select>
                    </div>
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="float: right;" class="btn btn-success" value="Search">
                </form>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Uploaded By</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($company as $key=>$doc)
                        <tr>
                            <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                            <td> {{$doc->email}}</td>
                            <td> @if($doc->type==1) {{"FSC"}}@elseif($doc->type==2) {{"ROC"}}@elseif($doc->type==3) {{"Minutes Of Meetings"}}@endif</td>
                            <td>
                                <a href="{{URL::to('public/images/template')."/".$doc->filename}}"
                                   download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp<a
                                   data-toggle="lightbox"
                                   href="{{URL::to('public/images/template')."/".$doc->filename}}"
                                   class="btn btn-primary">View</a>

                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-template')."/".$doc->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('CompanyDetails.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "info": false,
            "searching": false,
        });


        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop