@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Company Contact Person</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="">

                    <div class="col-sm-3">
                        <input type="text" id="name" value="{{@$_POST['name']}}" name="name" class="form-control" placeholder="Name">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="email" name="email" value="{{@$_POST['email']}}"class="form-control" placeholder="Email">
                    </div>
                    <div class="col-sm-3">
                        <select title="" data-toggle="tooltip" name="person_type" id="person_type" class="form-control" data-original-title="Select Person Type">
                            <option value="">---Select Type---</option>
                            <option value="1" @if(@$_POST['person_type']==1){{"selected='selected'"}}@endif>Ultimate Beneficial Owner</option>
                            <option value="2" @if(@$_POST['person_type']==2){{"selected='selected'"}}@endif>Shareholder</option>
                            <option value="3" @if(@$_POST['person_type']==3){{"selected='selected'"}}@endif>Director</option>
                            <option value="4" @if(@$_POST['person_type']==4){{"selected='selected'"}}@endif>Trustees</option>
                            <option value="5" @if(@$_POST['person_type']==5){{"selected='selected'"}}@endif>Others</option>
                        </select>
                    </div>
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="float: right;"class="btn btn-success" value="Search">
                </form>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Designation</th>
                            <th>Date of Appointment</th>
                            <th>Contact</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($company as $key=>$person)
                        <tr>
                            <td> {{$person->person_name}}</td>
                            <td> {{$person->person_email}}</td>
                            <td>
                                <?php
                                if ($person->contact_person_type == 1) {
                                    echo "Ultimate Beneficial Owner";
                                } elseif ($person->contact_person_type == 2) {
                                    echo "Shareholder";
                                } elseif ($person->contact_person_type == 3) {
                                    echo "Director";
                                } elseif ($person->contact_person_type == 4) {
                                    echo "Trustees";
                                } elseif ($person->contact_person_type == 5) {
                                    echo "Others";
                                }
                                ?></td>
                            <td> {{$person->person_designation}}</td>
                            <td> {{$person->doappointment}}</td>
                            <td> {{$person->person_contact_number}}</td>
                            <td>
                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-person')."/".$person->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('CompanyDetails.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "info": false,
            "searching": false,
        });


        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop