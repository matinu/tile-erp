@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Company Credit Invoices</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="">
                    <div class="col-sm-2">
                        <input type="text" id="fromdate" name="fromdate" value="{{@$_POST['fromdate']}}" class="form-control" placeholder="Date from">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="todate" name="todate" value="{{@$_POST['todate']}}" class="form-control" placeholder="Date to">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="titlefind" name="titlefind" value="{{@$_POST['titlefind']}}" class="form-control" placeholder="Title">
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" id="invoice_status" name="invoice_status">
                            <option value="">Select type</option>
                            <option value="pending" @if(@$_POST['invoice_status']=="pending"){{"selected='selected'"}}@endif>Pending</option>
                            <option value="paid" @if(@$_POST['invoice_status']=="paid"){{"selected='selected'"}}@endif>Paid</option>
                        </select>
                    </div>
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" style="float: right;" class="btn btn-success" value="Search">
                </form>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Hours</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($company as $key=>$invoice)
                        <tr>
                            <td> {{date("jS F, Y", strtotime($invoice->invoice_date))}}</td>
                            <td> {{$invoice->invoice_title}}</td>
                            <td> {{$invoice->invoice_description}}</td>
                            <td> {{$invoice->invoice_hours}}</td>
                            <td> {{$invoice->invoice_status}}</td>
                            <td>
                                <a href="{{URL::to('edit-credit-invoice')."/".$invoice->id}}"
                                   class="btn btn-primary">Edit</a>&nbsp

                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-credit-invoice')."/".$invoice->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('CompanyDetails.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "info": false,
            "searching": false,
        });


        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
    $(document).ready(function () {
        $("#fromdate").datepicker({endDate: new Date()});
        $("#todate").datepicker({endDate: new Date()});
    });
</script>
@stop