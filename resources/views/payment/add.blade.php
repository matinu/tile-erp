@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <div class="box-title">
                <h3>
                   {{$pageTitle}}
                </h3>
            </div>
            <div class="box-content">

                <form action="{{url('payment-entry/'.$invoice->invoice_id)}}" method="POST" class='form-horizontal' id="create-invoice" >
                    <div class="step" id="firstStep">
                        <div class="step-forms">

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Invoice Number</label>
                                <label class="control-label col-sm-4">{{$invoice->invoice_number}}</label>
                            </div>

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Total Amount : </label>
                                <label class="control-label col-sm-4">{{$invoice->invoice_total}}</label>

                                <label for="doi" class="control-label col-sm-2">Due Amount : </label>
                                <label class="control-label col-sm-4">{{$invoice->invoice_total - $amount}}</label>
                            </div>

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Payment Amount</label>

                                <div class="col-sm-10">
                                    <input type="text" name="amount" id="amount" class="form-control">
                                    <span id="amounterror" style="color: red;" class="col-sm-10"></span>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="client" class="control-label col-sm-2">Payment Type</label>
                                <div class="col-sm-10">
                                    <select name="type" id="type" class='form-control'>
                                        <option value="">Select Option</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Check">Check</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Date of Payment</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="date" id="date" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notes" class="control-label col-sm-2">Notes</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <textarea name="notes" id="notes" rows="5" cols="80" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="{{url('payment/'.$invoice->invoice_number)}}" class="btn"><i class="fa glyphicon-step_backward"></i>Go Back</a>
                        <input type="submit" id="submit" class="btn btn-primary" value="Submit" >
                    </div>
                    <input type="hidden" name="amountmax" id="amountmax" value="{{$invoice->invoice_total-$amount}}">
                </form>
                
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#amount').change(function () {
            if ($.isNumeric($('#amount').val())) {
                if (parseFloat($('#amount').val()) > parseFloat($('#amountmax').val())) {
                    $('#amounterror').html("Amount "+$('#amount').val()+' is larger than Due Amount');
                    $('#amount').val($('#amountmax').val());
                    return false;
                } else {
                    $('#amounterror').html('');
                    $('#amount').css("border-color", "");
                    return true;
                }
            }
            else{
                $('#amount').val('');
                $('#amounterror').html('Please Enter a Numeric Value');
            }

        });
        $('#submit').click(function () {
            if (!$('#amount').val()) {
                $('#amount').css("border-color", "red");
                return false;
            }
            if (!$('#type').val()) {
                $('#type').css("border-color", "red");
                return false;
            }
            if (!$('#date').val()) {
                $('#date').css("border-color", "red");
                return false;
            }
            if ($('#amount').val() == $('#amountmax').val()) {
                $('#amount').css("color", "red");
                $('#amount').css("border-color", "red");
                return confirm("Are you sure you want to full paid the bill???");
            }
        });
    });
</script>

@stop