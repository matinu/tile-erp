@extends('layout.admintamplate')

@section('content')


<div class="row">
    <div class="col-sm-12">
        @if(@$action != 'single')
        <div class="well">
            <div class="box-content">

                <h4>Search Payment</h4>

                @if(isset($invoice_number))
                <input type="hidden" id="invoice_number" value="{{$invoice_number}}">
                @else
                <input type="hidden" id="invoice_number" value="">
                @endif

                <form action="{{url('payment-list')}}" method="post" class='form-horizontal'>
                    <div class="form-group">
                        <label for="organizationName" class="control-label col-sm-2">Organization Name</label>

                        <div class="col-sm-4">
                            <select name="organizationName" id="organizationName" class="form-control">
                                <option value="">Select Company</option>
                                @foreach($organizations as $organization)
                                <option value="{{$organization->company_id}}" @if(Input::get('organizationName') == $organization->company_id){{ @selected }}@endif >{{$organization->company_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has())
                            <div class="alert-danger">
                                {{$errors->first('organizationName')}}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Invoice Number</label>

                        <div class="col-sm-4">
                            <select name="invoiceId" id="invoiceId" class='form-control'>
                                <option value="">Select Invoice</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="date" class="control-label col-sm-2">Date</label>

                        <div class="col-sm-4">
                            <input type="text" name="date" id="datepicker"
                                   class="form-control datepicker"
                                   data-rule-required="true" value="{{@Input::get('date')}}"/>
                            @if ($errors->has())
                            <div class="alert-danger">
                                {{$errors->first('date')}}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-4">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-primary" value="Search">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif

        <div class="well">
            <div class="box-title">
                <h3>
                    {{$pageTitle}}
                </h3>
                <?php if (Session::get('msg') != "") { ?>
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ Session::get('msg') }}
                    </div>
                <?php } ?>
            </div>
            <div class="box-content nopadding">
                <style>
                    th {
                        text-align: center !important;
                    }
                </style>
                @if($action == 'all' || $action == 'paid')
                <table class="table table-hover table-nomargin table-bordered usertable dataTable text-center">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>Organization Name</th>
                            <th>Invoice Number</th>
                            <th>Total Amount</th>
                            <th>Discount (%)</th>
                            <th>Received Amount</th>
                            <th>Due Amount</th>
                            <th class='hidden-350'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- */ $sl = 1; /* --}}
                        @foreach($invoices as $invoice)
                        @if($action == 'paid')
                        <?php $condition = $invoice->invoice_total == $invoice->amount; ?>
                        @else
                        <?php $condition = $invoice->invoice_total != $invoice->amount; ?>
                        @endif
                        @if($condition)
                        <tr>
                            <td class="with-checkbox">{{$sl}}</td>
                            <td>{{$invoice->company_name}}</td>
                            <td>{{$invoice->invoice_number}}</td>
                            <td>{{round($invoice->invoice_total + $invoice->discount,4)}}</td>
                            <td>{{round($invoice->discount,4)}}</td>
                            <td class='hidden-350'>
                                @if($invoice->amount < 0 || $invoice->amount == null)
                                {{@0}}
                                @else
                                {{round($invoice->amount,4)}}
                                @endif
                            </td>
                            <td class='hidden-1024'>{{round($invoice->invoice_total - $invoice->amount,4)}}</td>
                            <td class='hidden-480'>
                                @if($invoice->amount < $invoice->invoice_total)
                                <a href="<?php echo url() ?>/payment-add/{{$invoice->invoice_number}}"
                                   class="btn" rel="tooltip" title="Add Payment">
                                    <i class="fa glyphicon glyphicon-plus"></i>
                                </a>
                                @endif
                                @if($invoice->amount != null)
                                <a href="<?php echo url() ?>/payment/{{$invoice->invoice_number}}"
                                   class="btn" rel="tooltip" title="Details">
                                    <i class="fa fa-list"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        {{-- */ $sl++; /* --}}
                        @endif
                        @endforeach
                    </tbody>
                </table>
                @elseif($action == 'single')
                <table class="table table-hover table-nomargin table-bordered dataTable usertable text-center">
                    <thead>
                        <tr>
                            <th class='with-checkbox'>SL</th>
                            <th>Invoice Number</th>
                            <th>payment Amount</th>
                            <th>Payment Type</th>
                            <th>Note</th>
                            <th class='hidden-350'>Payment Date</th>
                            <th class='hidden-350'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- */
                                $sl = 1;
                                 $invoice_number = '';
                                 $amount = '';
                                 $invoice_total = '';
                            /* --}}

                        @foreach($invoices as $invoice)
                        <tr>
                            <td class="with-checkbox">{{$sl}}</td>
                            <td>{{$invoice->invoice_number}}</td>
                            <td class='hidden-350'>
                                {{$invoice->amount}}
                            </td>
                            <td class='hidden-1024'>{{$invoice->method}}</td>
                            <td class='hidden-1024'>{{-- */ echo $invoice->note; /* --}}</td>
                            <td class='hidden-1024'>{{$invoice->payment_date}}</td>
                            <td class='hidden-480'>
                                <a href="<?php echo url() ?>/payment-edit/{{$invoice->payment_id}}" class="btn"
                                   rel="tooltip" title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="<?php echo url() ?>/payment-delete/{{$invoice->payment_id}}"
                                   class="btn" rel="tooltip" title="Delete" onclick="if (confirm('Do you want to delete this client ?')) {
                                                       return;
                                                   } else {
                                                       return false;
                                                   }
                                                   ;">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                        {{-- */ $sl++;
                                    $invoice_number = $invoice->invoice_number;
                                    $invoice_total = $invoice->invoice_total;
                                    $amount = $invoice->amount;
                                 /* --}}
                        @endforeach

                        <tr>
                            <td colspan="7" class="text-right">
                                <a href="{{url('payment-list')}}" class="btn"><i class="fa glyphicon-step_backward"></i>Go
                                    Back</a>
                                @if($invoice_total > $amount)
                                <a href="{{url('payment-add/'.$invoice_number)}}" class="btn"><i
                                        class="fa glyphicon-circle_plus"></i> Add Payment</a>
                                @endif
                            </td>
                        </tr>

                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
</div>


<input type="hidden" id="url" value="{{url('invoiceList')}}"/>
<script>
    $(document).ready(function () {
        ;
        $('#organizationName').change(function () {
            if ($('#organizationName').val() == "")
                var url = $('#url').val() + "/0";
            else
                var url = $('#url').val() + "/" + $('#organizationName').val();
            var token = $("input[name=_token]").val();
            var dataString = 'token=' + token;
            $.ajax({
                type: "GET",
                url: url,
                data: dataString,
                success: function (data) {
                    $('#invoiceId').html(data);
                }
            }, "json");
        });
        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].text == valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }
        if ($('#organizationName').val() != "") {
            var url = $('#url').val() + "/" + $('#organizationName').val();
            var token = $("input[name=_token]").val();
            var dataString = 'token=' + token;
            $.ajax({
                type: "GET",
                url: url,
                data: dataString,
                success: function (data) {
                    $('#invoiceId').html(data);
                    //Get select object
                    var objSelect = document.getElementById("invoiceId");
                    //Set selected
                    setSelectedValue(objSelect, $('#invoice_number').val());
                }
            }, "json");
        }

    });
</script>
<script>
    $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
</script>
@stop