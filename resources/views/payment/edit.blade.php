@extends('layout.admintamplate')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <div class="box-title">
                <h3>
                    {{$pageTitle}}
                </h3>
            </div>
            <div class="box-content">
                <?php //var_dump($invoice); die;?>
                <div class="col-lg-12">
                    @if(@Session::get('success-message'))
                        <div class="alert alert-success text-center">{{@Session::pull('success-message')}}</div>
                    @endif
                    @if(@Session::get('failed-message'))
                        <div class="alert alert-danger text-center">{{@Session::pull('failed-message')}}</div>
                    @endif
                </div>
                <form action="{{url('/payment-edit')}}" method="POST" class='form-horizontal' id="create-invoice" >
                    <div class="step" id="firstStep">
                        <input type="hidden" name="payment_id" value="{{$invoice[0]->payment_id}}">
                        <div class="step-forms">

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Invoice Number</label>
                                <div class="col-sm-10">
                                    {{$invoice[0]->invoice_number}}
                                    <input type="hidden" name="invoice_number" value="{{$invoice[0]->invoice_number}}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Total Amount : </label>
                                <label for="doi" class="control-label col-sm-4">{{$invoice[0]->invoice_total}}</label>
                                <label for="doi" class="control-label col-sm-2">Due Amount : </label>
                                {{-- */ $due = $invoice[0]->invoice_total - \App\Models\Invoice\Custom::getPaymentStatus($invoice[0]->invoice_id); /* --}}
                                <input type="hidden" name="payment_due" value="{{$due}}">
                                <label for="doi" class="control-label col-sm-4">{{$due}}</label>
                            </div>

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Pay Amount</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="amount" id="amount" class="form-control" value="{{$invoice[0]->amount}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="client" class="control-label col-sm-2">Payment Type</label>
                                <div class="col-sm-10">
                                    <select name="payment-type" id="payment-type" class='form-control'>

                                        @if("Cash"==$invoice[0]->method)
                                        <option value="{{$invoice[0]->method}}" selected="selected">Cash</option>
                                        @else
                                        <option value="{{$invoice[0]->method}}">Cash</option>
                                        @endif
                                        @if("Check"==$invoice[0]->method)
                                        <option value="{{$invoice[0]->method}}" selected="selected">Check</option>
                                        @else
                                        <option value="{{$invoice[0]->method}}">Check</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Date of Issue</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?php if($invoice[0]->payment_date != null && $invoice[0]->payment_date != ''){ echo $date = date('d/m/Y', strtotime($invoice[0]->payment_date)); } ?>"  name="issue-date" id="issue-date" class="form-control datepick" data-rule-required="true">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="notes" class="control-label col-sm-2">Notes</label>
                                <div class="col-sm-10">
                                    <textarea name="notes" id="notes" rows="5" cols="80" class="form-control">{{$invoice[0]->note}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!--<input type="reset" class="btn" value="Back" id="back">-->
                        <input type="submit" class="btn btn-primary" value="Update" >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop