@extends('layout.admintamplate')

@section('content')
<style>
    .fancybox-wrap{
        top:15px !important;

    }
</style>
<script src="{{URL::to('resources/assets/js/tinymce/tinymce.min.js')}}"></script>
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        {{--Add Invoice --}}
        <div style="width: 600px; display: none" id="invoice" >
            <div class="col-md-12" >
                <div class="col-md-12">
                    <div class="modal-header">

                        <h4 class="modal-title">Add Credit Invoice</h4>

                        <div id="spinner_invoice" style="display: none">
                            <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                        </div>
                    </div>
                    <div class="modal-body">

                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Invoice Date:</label>
                                <input type="text" class="form-control datepicker" data-toggle="tooltip"
                                       name="invoice_date" id="invoice_date"
                                       title="Enter Invoice Date"
                                       placeholder="Date of Invoice" autocomplete="off" data-mask="email"/>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Invoice Title:</label>
                                <input type="text" name="invoice_title" data-toggle="tooltip" class="form-control"
                                       id="invoice_title" placeholder="Invoice Title"  title="Enter Invoice Title">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Invoice Description:</label>
                                <textarea  class="form-control" data-toggle="tooltip" name="invoice_description"
                                           id="invoice_description" placeholder="Invoice Description" title="Enter Invoice Description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Hours:</label>
                                <input type="number" name ="invoice_hours" placeholder="Invoice Hours" class="form-control" id="invoice_hours">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Status:</label>
                                <select name ="invoice_status" id="invoice_status" class="form-control">
                                    <option value="pending">Pending</option>
                                    <option value="paid">Paid</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default custom-close"  id="close_invoice" onclick="$.fancybox.close();">Close</button>
                        <button type="button" class="btn btn-primary" id="add_invoice">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        {{--add statement--}}
        @include('company.company_bank_statement')

        {{--Incorporation Details --}}
        @include('company.incorporation')

        {{--Compliance Details --}}
        @include('company.compliance')

        {{--Tax Return Details --}}
        @include('company.taxreturn')

        {{--Financial Statements Details --}}
        @include('company.financial')


        {{--Template Details --}}
        <div style="width: 600px; display: none;" id="template">
            <div class="col-md-12" >
                <div class="col-md-12">
                    <div class="modal-header">

                        <h4 class="modal-title">Add Template</h4>

                        <div id="spinner_invoice" style="display: none">
                            <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                        </div>
                    </div>
                    <div class="modal-body" id="template_description">
                        <form id="templateform" method="post"
                              action="{{URL::to('add-tamplate')."/".$company->company_id}}" enctype="multipart/form-data">
                              <input type="hidden" value="{{csrf_token()}}" id="token" name="_token"/>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Template Type:</label>
                                <select class="form-control" id="template_type">
                                    <option value="">---Select Type---</option>
                                    <option value="1">FSC</option>
                                    <option value="2">ROC</option>
                                    <option value="3">Minutes Of Meetings</option>
                                </select>

                            </div>
                            <div class="form-group"  >
                                <label for="message-text" class="control-label">Files:</label>
                                <input type="file" class="form-control" name="template_attached" id="template_attached">
                            </div>

                            <div class="form-group" >


                            </div>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default custom-close"  id="close_template" onclick="$.fancybox.close();">Close</button>
                            <button type="button" class="btn btn-primary" id="add_Tamplate">Add Template</button>
                        </div>
                        <div id="progress">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--Bill Details --}}
        <div style="width: 600px; display: none; min-height: 500px;" id="bill">
            <div class="col-md-12" >
                <div class="col-md-12">
                    <div class="modal-header">

                        <h4 class="modal-title">Generate Billing Details</h4>

                        <div id="spinner_invoice" style="display: none">
                            <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                        </div>
                    </div>
                    <div class="modal-body" id="bill_description">

                    </div>

                </div>
            </div>
        </div>

        {{--Add contact--}}
        <div style="width: 600px; display: none" id="contact" >
            <div class="col-md-12" >
                <div class="col-md-12">
                    <div class="modal-header">

                        <h4 class="modal-title">Add Contact</h4>

                        <div id="spinner_contact" style="display: none">
                            <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                        </div>
                    </div>
                    <div class="modal-body">

                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Name:</label>
                                <input type="text" data-toggle="tooltip" class="form-control" id="person_name" title="Enter Person Name" placeholder="*Person Name" >
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Contact Person Type:</label>
                                <select class="form-control" id="person_type" name="person_type" data-toggle="tooltip" title="Select Person Type">
                                    <option value="">---Select Type---</option>
                                    <option value="1" @if(old('person_type[]')==1){{"selected='selected'"}}@endif>Ultimate Beneficial Owner</option>
                                    <option value="2" @if(old('person_type[]')==2){{"selected='selected'"}}@endif>Shareholder</option>
                                    <option value="3" @if(old('person_type[]')==3){{"selected='selected'"}}@endif>Director</option>
                                    <option value="4" @if(old('person_type[]')==4){{"selected='selected'"}}@endif>Trustees</option>
                                    <option value="5" @if(old('person_type[]')==5){{"selected='selected'"}}@endif>Others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Email:</label>
                                <input type="email" data-toggle="tooltip" class="form-control" id="person_email" title="Enter Person Email" placeholder="*Person email" >
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Designation:</label>
                                <input type="text" data-toggle="tooltip" class="form-control" id="person_designation" title="Enter Person Designation"  placeholder="*Person designation" >
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Contact Number:</label>
                                <input type="text" data-toggle="tooltip" class="form-control" id="person_contact_number" title="Enter Person Contact Number" placeholder="*Person Contact Number">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Date of Appointment:</label>
                                <input type="text" data-toggle="tooltip" class="form-control datepicker" id="doappointment" name="doappointment"
                                       placeholder="*Person date of Appointment" autocomplete="off" value="{{old('doappointment')}}" title="Enter Person date of Appointment"/>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Date of Resignation:</label>
                                <input type="text" data-toggle="tooltip" class="form-control datepicker" id="doresignation" name="doresignation"
                                       placeholder="Person date of resignation" autocomplete="off" value="{{old('doresignation')}}" title="Enter Person date of Resignation"/>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Director:</label>
                                <input type="text" data-toggle="tooltip" class="form-control" name="director" id="director"
                                       placeholder="*Director" autocomplete="off" value="{{old('director')}}" title="Enter Director"/>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Shareholder:</label>
                                <input type="text" data-toggle="tooltip" class="form-control" name="shareholder" id="shareholder"
                                       placeholder="Share Holder" autocomplete="off" value="{{old('shareholder')}}" title="Enter Shareholder"/>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Company Secretary:</label>
                                <input type="text" data-toggle="tooltip" class="form-control" name="secretary" id="secretary"
                                       placeholder="Company secretary" autocomplete="off" value="{{old('secretary')}}" title="Enter Company Secretary"/>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default custom-close"  id="close_contact" onclick="$.fancybox.close();">Close</button>
                        <button type="button" class="btn btn-primary" id="add_contact">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        {{--add document--}}
        <div style="width: 600px; display: none" id="document" >
            <div class="col-md-12" >
                <div class="col-md-12">
                    <div class="modal-header">

                        <h4 class="modal-title">Add Document</h4>

                        <div id="spinner_document" style="display: none">
                            <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                        </div>
                    </div>
                    <div class="modal-body">

                        <form id="DocumentForm" method="post" action="{{URL::to('add-document')."/".$company->company_id}}"
                              enctype="multipart/form-data">
                              <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Document Title:</label>
                                <input type="text" name="document_title" class="form-control" id="document_title">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Document Description:</label>
                                <input type="text" class="form-control" name="document_description" id="document_description">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Document Type:</label>
                                <select name="document_type" id="document_type" class="form-control"  title="Select Document type">
                                    <option value="KYC">KYC</option>
                                    <option value="Statutory">Statutory</option>
                                    <option value="Minutes Book">Minutes Book</option>
                                    <option value="Register">Register</option>
                                    <option value="Bank Statementss">Bank Statements</option>
                                    <option value="Correspondence">Correspondence</option>
                                    <option value="Incorporation Documents">Incorporation Documents</option>
                                    <option value="Bank Account Opening Documents">Bank Account Opening Documents</option>
                                    <option value="Contracts & Receipts of Payment">Contracts & Receipts of Payment</option>
                                    <option value="Invoices">Invoices</option>
                                    <option value="Accounting">Accounting</option>
                                    <option value="Statutory Filing">Statutory Filing</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Files:</label>
                                <input type="file" class="form-control" name="document_attached" id="document_attached">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default custom-close"  id="close_document" onclick="$.fancybox.close();" >Close</button>
                        <button type="button" class="btn btn-primary" id="add_document">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        {{--add Correspondence--}}
        <div style="width: 1000px; display: none" id="correspondence" >
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Correspondence</h4>

                        <div id="spinner_correspondence" style="display: none">
                            <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                        </div>
                    </div>
                    <div class="modal-body">

                        <form id="CorrespondenceForm" method="post"
                              action="{{URL::to('add-correspondence')."/".$company->company_id}}" enctype="multipart/form-data">
                              <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Correspondence Date:</label>
                                    <input type="text" class="form-control datepicker" data-toggle="tooltip"
                                           name="date_of_correspondence" id="date_of_correspondence"
                                           title="Enter Date of correspondence"
                                           placeholder="Date of correspondence" autocomplete="off" data-mask="email"/>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Correspondence type:</label>
                                    <select name="correspondence_type" id="correspondence_type" class="form-control"  title="Select correspondence type">
                                        <option value="Letters of inquiry">Letters of inquiry</option>
                                        <option value="Letters of claim/complaints">Letters of claim/complaints</option>
                                        <option value="Letters of Application">Letters of Application</option>
                                        <option value="Letters of approval/dismissal">Letters of approval/dismissal</option>
                                        <option value="Letters of recommendations">Letters of recommendations</option>
                                        <option value="Letters of Sales">Letters of Sales</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Correspondence Title:</label>
                                    <input type="text" name="document_title" data-toggle="tooltip" class="form-control"
                                           id="correspondence_title" title="Enter Correspondence Title">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Correspondence Description:</label>
                                    <textarea  class="form-control" data-toggle="tooltip" name="correspondence_description"
                                               id="correspondence_description" title="Enter Correspondence Description"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Received By:</label>
                                    <input type="radio" name="received_by" checked="checked" id="correspondence_type_email" selected value="email">Email
                                    <input type="radio" name="received_by" id="correspondence_type_document"
                                           value="document">Document

                                </div>
                                <div class="form-group" id="type_document" style="display: none;">
                                    <label for="message-text" class="control-label">Files:</label>
                                    <input type="file" class="form-control" name="document_attached" id="correspondence_attached">
                                </div>
                                <div class="form-group" id="type_email" >
                                    <label for="message-text" class="control-label">Content:</label>
                                    <textarea id="correspondence_content" name="correspondence_content"></textarea>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"  id="close_correspondence" onclick="$.fancybox.close();">Close
                        </button>
                        <button type="button" class="btn btn-primary" id="add_correspondence">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!--<link href="{{URL::to('resources/assets/js/lightbox-master/dist/ekko-lightbox.css')}}" rel="stylesheet">-->
        <!--<script src="{{URL::to('resources/assets/js/lightbox-master/dist/ekko-lightbox.min.js')}}"></script>-->
        @if (Session::has('success_add_msg'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::pull('success_add_msg') }}
        </div>
        @endif
        @if (Session::has('success_update_msg'))
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::pull('success_update_msg') }}
        </div>
        @endif
        @if (Session::has('success_delete_msg'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::pull('success_delete_msg') }}
        </div>
        @endif
        <div>
            <?php if ($errors->first() != "") { ?>
                <div class="input-group col-md-8">
                    <div class="alert alert-danger">

                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ $errors->first() }}

                    </div>
                </div>
            <?php } ?>

            <div class="row">
                <input type="hidden" value="{{$company->company_id}}" id="company_id"/>
                <input type="hidden" value="{{csrf_token()}}" id="token"/>


                <div class="col-md-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingzero">
                                <h3 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsezero" aria-expanded="true" aria-controls="collapsezero">Company Basic Information</a> <span class="pull-right"><a href="{{URL::to('edit-company')."/".$company->company_id}}" class="btn btn-success "><i class="fa fa-pencil"></i></a> </span></h3>
                            </div>
                            <div id="collapsezero" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingzero">
                                <div class="panel-body">
                                    <div class="col-md-7">
                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <p style="margin-left:14%;"><img src="{{URL::to('public/images/company_logo/')."/".$company->company_logo}}"
                                                                                 width="200px" height="100px"/></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Company Name</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{$company->company_name}}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Company Previous Name</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{$company->previous_name}}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Company Type</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{$company->company_type}}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Date of Incorporation</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{date("jS F, Y", strtotime($company->date_of_incorporation))}}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Company Status</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{$company->company_status }}</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Company New Address</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{$company->company_new_address.", ".$company->company_country }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Company Old Address</label>

                                            <div class="col-sm-7">
                                                <p class="form-control-static">{{$company->company_old_address.", ".$company->company_country }}</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-5">
                                        {{-- */$c=0;/* --}}
                                        @foreach($company->contactPersons as $key=>$person)
                                        @if($c++ == 2) {{-- */break;/* --}} @endif
                                        <div class="box-body box-profile">

                                            <h3 class="profile-username text-center">{{$person->person_name}}</h3>
                                            <p class="text-muted text-center">{{$person->person_designation}},  {{$person->person_email}},  {{$person->person_contact_number}}</p>

                                            <ul class="list-group list-group-unbordered">
                                                <li class="list-group-item">
                                                    <b>Type</b>
                                                    <a class="pull-right"> <?php
                                                        if ($person->contact_person_type == 1) {
                                                            echo "Ultimate Beneficial Owner";
                                                        } elseif ($person->contact_person_type == 2) {
                                                            echo "Shareholder";
                                                        } elseif ($person->contact_person_type == 3) {
                                                            echo "Director";
                                                        } elseif ($person->contact_person_type == 4) {
                                                            echo "Trustees";
                                                        } elseif ($person->contact_person_type == 5) {
                                                            echo "Others";
                                                        }
                                                        ?></a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Date of Appointment</b> <a class="pull-right">{{$person->doappointment}}</a>
                                                </li>
                                            </ul>
                                        </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a data-toggle="tab" href="#collapseone" aria-expanded="true">Company Correspondence Details</a></li>
                            <li class=""><a data-toggle="tab" href="#collapsetwo" aria-expanded="false">Company Document Details</a></li>
                            <li class=""><a data-toggle="tab" href="#collapsethree" aria-expanded="false">Company Contact Person</a></li>
                            <li class=""><a data-toggle="tab" href="#collapsefour" aria-expanded="false">Company Credit Invoices</a></li>
                            <li class=""><a data-toggle="tab" href="#collapsefive" aria-expanded="false">Template Document Details</a></li>
                            <li class=""><a data-toggle="tab" href="#collapsesix" aria-expanded="false">Bank Statement Details</a></li>
                            <li class=""><a data-toggle="tab" href="#collapseseven" aria-expanded="false">Company Incorporation Details</a></li>
                            <li class=""><a data-toggle="tab" href="#collapseeight" aria-expanded="false">Company Compliance Reports Details</a></li>
                            <li class=""><a data-toggle="tab" href="#collapsenine" aria-expanded="false">Company Annual Tax Return</a></li>
                            <li class=""><a data-toggle="tab" href="#collapseten" aria-expanded="false">Company Financial Statements</a></li>

                            <!--<li class="pull-right"><a class="text-muted" href="#"><i class="fa fa-gear"></i></a></li>-->
                        </ul>
                        <div class="tab-content">
                            <div id="collapseone" class="tab-pane active">
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered">
                                        <caption>Company Correspondences

                                            <a class="fancybox btn btn-success  pull-right"  style="margin-right: 10px;"
                                               href="#correspondence">Add Correspondence
                                            </a>
                                        </caption>
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Type</th>
                                                <th>Content</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- */$c=0;/* --}}
                                            @foreach($company->correspondences as $key=>$correspondence)
                                            @if($c++ == 5) {{-- */break;/* --}} @endif
                                            <tr>
                                                <td> {{date("jS F, Y", strtotime($correspondence->date_of_correspondence))}}</td>
                                                <td> {{$correspondence->correspondence_title}}</td>
                                                <td> {{$correspondence->correspondence_description}}</td>
                                                <td> {{$correspondence->correspondence_type}}</td>
                                                <td>
                                                    @if($correspondence->received_by=='document')
                                                      <?php if (file_exists('public/images/company_correspondence/'.$correspondence->correspondence_document)) { ?>
                                                    <a href="{{URL::to('public/images/company_correspondence')."/".$correspondence->correspondence_document}}"
                                                       download="{{$correspondence->correspondence_document}}" class="btn btn-primary">Download</a>&nbsp
                                                      <?php } ?> 
                                                    @endif

                                                    @if($correspondence->received_by=='email')
                                                    <button type="button" class="btn btn-sm btn-default" data-placement="top" data-toggle="popover"  data-content="{{ strip_tags($correspondence->correspondence_content)}}">Show</button>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-correspondence')."/".$correspondence->id}}"
                                                       class="btn btn-danger">Delete</a>&nbsp
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <footer>

                                        <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                                           href="{{URL::to('correspondance-view')."/".$company->company_id."/1"}}">View More
                                    </a>
                                </footer>
                            </div>
                        </div>
                        <div id="collapsetwo" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-striped table-bordered" id="table_document">
                                    <caption>Company Documents

                                        <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                                           href="#document">Add Document
                                        </a></caption>
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$c=0;/* --}}
                                        @foreach($company->documents as $key=>$doc)
                                        @if($c++ == 5) {{-- */break;/* --}} @endif
                                        <tr>
                                            <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                                            <td> {{$doc->document_title}}</td>
                                            <td> {{$doc->document_description}}</td>
                                            <td> {{$doc->document_type}}</td>
                                            <td>
                                                <?php if (file_exists('public/images/company_document/'.$doc->document_attached)) { ?>
                                                    <a href="{{URL::to('public/images/company_document')."/".$doc->document_attached}}"
                                                       download="{{$doc->document_attached}}" class="btn btn-primary">Download</a>&nbsp<a
                                                       data-toggle="lightbox"
                                                       href="{{URL::to('public/images/company_document')."/".$doc->document_attached}}"
                                                       class="btn btn-primary">View</a>
                                                   <?php } ?> 
                                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-document')."/".$doc->id}}"
                                                   class="btn btn-danger">Delete</a>&nbsp
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <footer>

                                    <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                                       href="{{URL::to('document-view')."/".$company->company_id."/1"}}">View More
                                </a>
                            </footer>
                        </div>
                    </div>
                    <div id="collapsethree" class="tab-pane">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered" id="contact_table">
                                <caption>Company Contact Person
                                    <a class=" fancybox btn btn-success  pull-right" href="#contact">Add Contact
                                    </a>
                                </caption>
                                <thead>
                                    <tr>

                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Designation</th>
                                        <th>Date of Appointment</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$c=0;/* --}}
                                    @foreach($company->contactPersons as $key=>$person)
                                    @if($c++ == 5) {{-- */break;/* --}} @endif
                                    <tr>
                                        <td> {{$person->person_name}}</td>
                                        <td> {{$person->person_email}}</td>
                                        <td>
                                            <?php
                                            if ($person->contact_person_type == 1) {
                                                echo "Ultimate Beneficial Owner";
                                            } elseif ($person->contact_person_type == 2) {
                                                echo "Shareholder";
                                            } elseif ($person->contact_person_type == 3) {
                                                echo "Director";
                                            } elseif ($person->contact_person_type == 4) {
                                                echo "Trustees";
                                            } elseif ($person->contact_person_type == 5) {
                                                echo "Others";
                                            }
                                            ?></td>
                                        <td> {{$person->person_designation}}</td>
                                        <td> {{$person->doappointment}}</td>
                                        <td> {{$person->person_contact_number}}</td>
                                        <td>
                                            <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-person')."/".$person->id}}"
                                               class="btn btn-danger">Delete</a>&nbsp
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <footer>

                                <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                                   href="{{URL::to('contact-person-view')."/".$company->company_id."/1"}}">View More
                            </a>
                        </footer>
                    </div>
                </div>

                <div id="collapsefour" class="tab-pane">
                    <div class="panel-body">
                        <table class="table table-striped table-bordered" id="invoice_table">
                            <caption>Company Credit Invoices Details

                                <a class="fancybox btn btn-success  pull-right"  style="margin-right: 10px;"
                                   href="#invoice">Add Credit Invoice
                                </a>
                                <a class="fancybox btn btn-success  pull-right" id="billing_details" style="margin-right: 10px;"
                                   href="#bill">Generate Bill Invoice
                                </a>
                            </caption>
                            <thead>
                                <tr>

                                    <th>Date</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Hours</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- */$c=0;/* --}}
                                @foreach($company->invoices as $key=>$invoice)
                                @if($c++ == 5) {{-- */break;/* --}} @endif
                                <tr>
                                    <td> {{date("jS F, Y", strtotime($invoice->invoice_date))}}</td>
                                    <td> {{$invoice->invoice_title}}</td>
                                    <td> {{$invoice->invoice_description}}</td>
                                    <td> {{$invoice->invoice_hours}}</td>
                                    <td> {{$invoice->invoice_status}}</td>
                                    <td>
                                        <a href="{{URL::to('edit-credit-invoice')."/".$invoice->id}}"
                                           class="btn btn-primary">Edit</a>&nbsp

                                        <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-credit-invoice')."/".$invoice->id}}"
                                           class="btn btn-danger">Delete</a>&nbsp
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <footer>

                            <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                               href="{{URL::to('invoices-view')."/".$company->company_id."/1"}}">View More
                        </a>
                    </footer>
                </div>
            </div>
            <div id="collapsefive" class="tab-pane" >
                <div class="panel-body">
                    <table class="table table-striped table-bordered" id="table_document">
                        <caption>Company Template

                            <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                               href="#template">Add Template
                            </a>
                        </caption>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Uploaded By</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- */$c=0;/* --}}
                            @foreach($company->templates as $key=>$doc)
                            @if($c++ == 5) {{-- */break;/* --}} @endif
                            <tr>
                                <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                                <td> {{$doc->user->email}}</td>
                                <td> @if($doc->type==1) {{"FSC"}}@elseif($doc->type==2) {{"ROC"}}@elseif($doc->type==3) {{"Minutes Of Meetings"}}@endif</td>
                                <td>
                                    <?php if (file_exists('public/images/template/'.$doc->filename)) { ?>
                                        <a href="{{URL::to('public/images/template')."/".$doc->filename}}"
                                           download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp<a
                                           data-toggle="lightbox"
                                           href="{{URL::to('public/images/template')."/".$doc->filename}}"
                                           class="btn btn-primary">View</a>
                                       <?php } ?> 
                                    <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-template')."/".$doc->id}}"
                                       class="btn btn-danger">Delete</a>&nbsp
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <footer>

                        <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                           href="{{URL::to('templates-view')."/".$company->company_id."/1"}}">View More
                    </a>
                </footer>
            </div>
        </div>
        <div id="collapsesix" class="tab-pane">
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="table_document">
                    <caption>Company Bank Statement Details

                        <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                           href="#statement">Add Bank Statement
                        </a></caption>
                    <thead>
                        <tr>
                            <th>Uploaded By</th>
                            <th>Title</th>
                            <th>Date of Issue</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- */$c=0;/* --}}
                        @foreach($company->statements as $key=>$doc)
                        @if($c++ == 5) {{-- */break;/* --}} @endif
                        <tr>
                            <td> {{$doc->user->email}}</td>
                            <td> {{$doc->title}}</td>
                            <td> {{$doc->date_of_issue}}</td>
                            <td>
                                <?php if (file_exists('public/images/statement/'.$doc->statement)) { ?>
                                    <a href="{{URL::to('public/images/statement')."/".$doc->statement}}"
                                       download="{{$doc->statement}}" class="btn btn-primary">Download</a>&nbsp<a
                                       data-toggle="lightbox"
                                       href="{{URL::to('public/images/statement')."/".$doc->statement}}"
                                       class="btn btn-primary">View</a>
                                   <?php } ?>
                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-statement')."/".$doc->id}}"
                                   class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <footer>

                    <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                       href="{{URL::to('statements-view')."/".$company->company_id."/1"}}">View More
                </a>
            </footer>
        </div>
    </div>
    <div id="collapseseven" class="tab-pane">
        <div class="panel-body">
            <table class="table table-striped table-bordered" id="table_document">
                <caption>Incorporation Details

                    <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                       href="#incorporation">Add Incorporation
                    </a>
                </caption>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Uploaded By</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- */$c=0;/* --}}
                    @foreach($company->incorporation as $key=>$doc)
                    @if($c++ == 5) {{-- */break;/* --}} @endif

                    <tr>
                        <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                        <td> {{$doc->user->email}}</td>
                        <td> {{$doc->title}}</td>
                        <td> {{$doc->description}}</td>
                        <td>
                            <?php if (file_exists('public/images/company_incorporation/'.$doc->filename)) { ?>
                                <a href="{{URL::to('public/images/company_incorporation')."/".$doc->filename}}"
                                   download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp<a
                                   data-toggle="lightbox"
                                   href="{{URL::to('public/images/company_incorporation')."/".$doc->filename}}"
                                   class="btn btn-primary">View</a>
                               <?php } ?>
                            <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-incorporation')."/".$doc->id}}"
                               class="btn btn-danger">Delete</a>&nbsp
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <footer>

                <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                   href="{{URL::to('incorporation-view')."/".$company->company_id."/1"}}">View More
            </a>
        </footer>
    </div>
</div>
<div id="collapseeight" class="tab-pane">
    <div class="panel-body">
        <table class="table table-striped table-bordered" id="table_document">
            <caption> Compliance Reports Details

                <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                   href="#compliance">Add Compliance Reports
                </a></caption>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Uploaded By</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {{-- */$c=0;/* --}}
                @foreach($company->compliance as $key=>$doc)
                @if($c++ == 5) {{-- */break;/* --}} @endif
                <tr>
                    <td> {{date("jS F, Y", strtotime($doc->created_at))}}</td>
                    <td> {{$doc->user->email}}</td>
                    <td> {{$doc->title}}</td>
                    <td> {{$doc->description}}</td>
                    <td>
                        <?php if (file_exists('public/images/company_compliance/'.$doc->document_attachment)) { ?>
                            <a href="{{URL::to('public/images/company_compliance')."/".$doc->document_attachment}}"
                               download="{{$doc->document_attachment}}" class="btn btn-primary">Download</a>&nbsp<a
                               data-toggle="lightbox"
                               href="{{URL::to('public/images/company_compliance')."/".$doc->document_attachment}}"
                               class="btn btn-primary">View</a>
                           <?php } ?>
                        <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-compliance')."/".$doc->id}}"
                           class="btn btn-danger">Delete</a>&nbsp
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <footer>

            <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
               href="{{URL::to('compliance-view')."/".$company->company_id."/1"}}">View More
        </a>
    </footer>
</div>
</div>
<!-- text return -->
<div id="collapsenine" class="tab-pane">
    <div class="panel-body">
        <table class="table table-striped table-bordered" id="table_document">
            <caption> Annual Tax Return

                <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                   href="#taxreturn">Add Annual Tax Return
                </a></caption>
            <thead>
                <tr>
                    <th>Taxation Date</th>
                    <th>Uploaded By</th>
                    <th>Client Name</th>
                    <th>Accountant/Company Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {{-- */$c=0;/* --}}
                @foreach($company->taxreturn as $key=>$doc)
                @if($c++ == 5) {{-- */break;/* --}} @endif
                <tr>
                    <td> {{date("jS F, Y", strtotime($doc->taxation_date))}}</td>
                    <td> {{$doc->user->email}}</td>
                    <td> {{$doc->client_name}}</td>
                    <td> {{$doc->a_c_name}}</td>
                    <td>
                        <?php if (file_exists('public/images/taxreturn/'.$doc->filename)) { ?>
                            <a href="{{URL::to('public/images/taxreturn')."/".$doc->filename}}"
                               download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp<a
                               data-toggle="lightbox"
                               href="{{URL::to('public/images/taxreturn')."/".$doc->filename}}"
                               class="btn btn-primary">View</a>
                           <?php } ?>
                        <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-tax-return')."/".$doc->id}}"
                           class="btn btn-danger">Delete</a>&nbsp
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <footer>

            <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
               href="{{URL::to('tax-return-view')."/".$company->company_id."/1"}}">View More
        </a>
    </footer>
</div>
</div>
<!-- Financial Statements -->
<div id="collapseten" class="tab-pane">

    <div class="panel-body">
        <table class="table table-striped table-bordered" id="table_document">
            <caption> Financial Statements

                <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
                   href="#financial">Add Financial Statements
                </a></caption>
            <thead>
                <tr>
                    <th>Issue Date</th>
                    <th>Uploaded By</th>
                    <th>Client Name</th>
                    <th>Bank Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {{-- */$c=0;/* --}}
                @foreach($company->financialstatements as $key=>$doc)
                @if($c++ == 5) {{-- */break;/* --}} @endif
                <tr>
                    <td> {{date("jS F, Y", strtotime($doc->issue_date))}}</td>
                    <td> {{$doc->user->email}}</td>
                    <td> {{$doc->client_name}}</td>
                    <td> {{$doc->bank_name}}</td>
                    <td>
                        <?php if (file_exists('public/images/financial_statements/'.$doc->filename)) { ?>
                            <a href="{{URL::to('public/images/financial_statements')."/".$doc->filename}}"
                               download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp<a
                               data-toggle="lightbox"
                               href="{{URL::to('public/images/financial_statements')."/".$doc->filename}}"
                               class="btn btn-primary">View</a>
                           <?php } ?>
                        <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-financial-statements')."/".$doc->id}}"
                           class="btn btn-danger">Delete</a>&nbsp
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <footer>

            <a class="fancybox btn btn-success  pull-right" style="margin-right: 10px;"
               href="{{URL::to('financial-statements-view')."/".$company->company_id."/1"}}">View More
        </a>
    </footer>
</div>
</div>
</div><!-- /.tab-content -->
</div><!-- nav-tabs-custom -->
</div>

</div>
</div>
<script>



                            function IsEmail(email) {
                                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                return regex.test(email);
                            }

                            var _validFileExtensions = [".doc", ".docx", ".pdf"];
                            function ValidateSingleInput(oInput) {
                                if (oInput.type == "file") {
                                    var sFileName = oInput.value;
                                    if (sFileName.length > 0) {
                                        var blnValid = false;
                                        for (var j = 0; j < _validFileExtensions.length; j++) {
                                            var sCurExtension = _validFileExtensions[j];
                                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                                blnValid = true;
                                                break;
                                            }
                                        }

                                        if (!blnValid) {
                                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                                            oInput.value = "";
                                            return false;
                                        }
                                    }
                                }
                                return true;
                            }

                            $(document).ready(
                                    function() {
                                        $('#correspondence_type_email').click(
                                                function() {
                                                    $('#type_document').hide();
                                                    $('#type_email').show();
                                                }
                                        );
                                        $('#correspondence_type_document').click(
                                                function() {
                                                    $('#type_document').show();
                                                    $('#type_email').hide();
                                                }
                                        );
                                        $('#add_correspondence').click(
                                                function() {
                                                    var date = $('#date_of_correspondence');
                                                    var title = $('#correspondence_title');
                                                    var description = $('#correspondence_description');
                                                    var received_by = $('input:radio[name=received_by]:checked').val();
                                                    var correspondence_type = $('#correspondence_type');
                                                    var doc = $("#correspondence_attached");
                                                    if (date.val() == "") {
                                                        date.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (title.val() == "") {
                                                        title.focus().mouseover();
                                                        return false;
                                                    } else if (description.val() == "") {
                                                        description.focus().mouseover();
                                                        return false;
                                                    } else if (correspondence_type.val() == "") {
                                                        correspondence_type.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (received_by == "") {
                                                        $('input:radio[name=received_by]').focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (doc.val() == "" || !ValidateSingleInput($('#correspondence_attached')[0])) {
                                                        doc.focus();
                                                        return false;
                                                    }
                                                    else {
                                                        var token = $('#token').val();
                                                        var filename = $("#correspondence_attached").val();
                                                        var formData = new FormData();
                                                        formData.append('file', $('#correspondence_attached')[0].files[0]);
                                                        formData.append('_token', token);
                                                        formData.append('title', title.val());
                                                        formData.append('description', description.val());
                                                        formData.append('type', correspondence_type.val());
                                                        formData.append('received_by', received_by);
                                                        formData.append('content', tinyMCE.get('correspondence_content').getContent());
                                                        formData.append('date', date.val());
                                                        jQuery.ajax({
                                                            type: "POST",
                                                            url: $('#CorrespondenceForm').attr("action"),
                                                            enctype: 'multipart/form-data',
                                                            processData: false,
                                                            contentType: false,
                                                            data: formData,
                                                            beforeSend: function() {
                                                                $('#spinner_correspondence').show();
                                                            },
                                                            success: function(result) {
                                                                //                                                var data = JSON.parse(result)
                                                                //                                                if (data.status) {
                                                                //                                                    var date = data.data.date_of_correspondence;
                                                                //                                                    var title = data.data.correspondence_title;
                                                                //                                                     var description = data.data.document_description;
                                                                //                                                    var type = data.data.correspondence_type;
                                                                //                                              if(type=='document'){
                                                                //                                                  var url = $("#base_url").val()+"/public/images/company_correspondence/"+data.data.document_attached;
                                                                //                                                  var action = "<a href='"+url+"' download='"+url+"' class='btn btn-primary'>Download</a>"
                                                                //                                              }else if(type=='email')
                                                                //                                              {
                                                                //
                                                                //                                                  var action = "<button type='button' class='btn btn-sm btn-default' data-placement='top' data-toggle='popover'  data-content='+data.data.correspondence_content+'>Show</button>"
                                                                //                                              }
                                                                //
                                                                //                                              var row = '<tr><td>'+date+'</td><td><tr><td>'+title+'</td><td>'+description+'</td><td>'+type+'</td><td>'+action+'</td></tr>';
                                                                //                                                    $('#table_document').append(row);
                                                                $('#close_correspondence').hide();
                                                                $('#close_document').trigger("click");
                                                                location.reload();
                                                                //                                                }
                                                            }
                                                        });
                                                    }


                                                }
                                        );
                                        $('#add_contact').click(
                                                function() {
                                                    var name = $('#person_name');
                                                    var designation = $('#person_designation');
                                                    var contact = $('#person_contact_number');
                                                    var email = $('#person_email');
                                                    var type = $('#person_type');
                                                    var doappointment = $('#doappointment');
                                                    var director = $('#director');
                                                    if (name.val() == "") {
                                                        name.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (type.val() == "") {
                                                        type.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (email.val() == "" || !validateEmail(email.val())) {
                                                        email.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (designation.val() == "") {
                                                        designation.focus().mouseover();
                                                        return false;
                                                    }

                                                    else if (contact.val() == "" || !validateContact(contact.val())) {
                                                        contact.focus().mouseover();
                                                        return false;
                                                    }

                                                    else if (email.val() == "") {
                                                        email.focus().mouseover();
                                                        return false;
                                                    } else if (!IsEmail(email.val())) {
                                                        email.focus().mouseover();
                                                        return false;
                                                    } else if (doappointment.val() == "") {
                                                        doappointment.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (director.val() == "") {
                                                        director.focus().mouseover();
                                                        return false;
                                                    }
                                                    else {
                                                        var token = $('#token').val();
                                                        var company = $('#company_id').val();
                                                        $.ajax({
                                                            url: $("#base_url").val() + "/add-company-contact-person",
                                                            type: 'POST',
                                                            data: {
                                                                company_id: company,
                                                                person_name: name.val(),
                                                                person_email: email.val(),
                                                                person_contact_number: contact.val(),
                                                                person_designation: designation.val(),
                                                                person_type: type.val(),
                                                                doappointment: doappointment.val(),
                                                                director: director.val(),
                                                                shareholder: $('#shareholder').val(),
                                                                secretary: $('#secretary').val(),
                                                                doresignation: $('#doresignation').val(),
                                                                _token: token
                                                            },
                                                            dataType: 'json',
                                                            beforeSend: function() {
                                                                $('#spinner_contact').show();
                                                            },
                                                            success: function(result) {
                                                                if (result) {
                                                                    //                                                    var name = result.data.person_name;
                                                                    //                                                    var email = result.data.person_email;
                                                                    //                                                    var contact = result.data.person_contact_number;
                                                                    //                                                    var designation = result.data.person_designation;
                                                                    //                                                    var row = '<tr><td>' + name + '</td><td>' + email + '</td><td>' + contact + '</td><td>' + designation + '</td></tr>';
                                                                    //                                                    $('#contact_table').append(row);
                                                                    $('#spinner_contact').hide();
                                                                    $('#close_contact').trigger("click");
                                                                    location.reload();
                                                                }


                                                            }

                                                        });
                                                    }


                                                }
                                        );
                                        $('#add_invoice').click(
                                                function() {
                                                    var title = $('#invoice_title');
                                                    var description = $('#invoice_description');
                                                    var date = $('#invoice_date');
                                                    var hours = $('#invoice_hours');
                                                    var status = $('#invoice_status')
                                                    if (date.val() == "") {
                                                        date.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (title.val() == "") {
                                                        title.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (description.val() == "") {
                                                        description.focus().mouseover();
                                                        return false;
                                                    }

                                                    else if (hours.val() == "" || $.isNumeric(hours.val()) == false) {
                                                        hours.focus().mouseover();
                                                        return false;
                                                    } else if (status.val() == "") {
                                                        status.focus().mouseover();
                                                        return false;
                                                    } else {
                                                        var token = $('#token').val();
                                                        var company = $('#company_id').val();
                                                        $.ajax({
                                                            url: $("#base_url").val() + "/add-company-credit-invoice",
                                                            type: 'POST',
                                                            data: {
                                                                company_id: company,
                                                                title: title.val(),
                                                                description: description.val(),
                                                                date: date.val(),
                                                                hours: hours.val(),
                                                                status: status.val(),
                                                                _token: token
                                                            },
                                                            dataType: 'json',
                                                            beforeSend: function() {
                                                                $('#spinner_invoice').show();
                                                            },
                                                            success: function(result) {
                                                                if (result) {
                                                                    //                                                    var name = result.data.person_name;
                                                                    //                                                    var email = result.data.person_email;
                                                                    //                                                    var contact = result.data.person_contact_number;
                                                                    //                                                    var designation = result.data.person_designation;
                                                                    //                                                    var row = '<tr><td>' + name + '</td><td>' + email + '</td><td>' + contact + '</td><td>' + designation + '</td></tr>';
                                                                    //                                                    $('#contact_table').append(row);
                                                                    $('#spinner_invoice').hide();
                                                                    $('#close_invoice').trigger("click");
                                                                    location.reload();
                                                                }


                                                            }

                                                        });
                                                    }


                                                }
                                        );
                                        $('#add_document').click(
                                                function() {
                                                    var doc = $('#document_attached');
                                                    var title = $('#document_title');
                                                    var description = $('#document_description');
                                                    var type = $('#document_type')
                                                    if (title.val() == "") {
                                                        title.focus();
                                                        return false;
                                                    } else if (description.val() == "") {
                                                        description.focus();
                                                        return false;
                                                    } else if (type.val() == "") {
                                                        type.focus().mouseover();
                                                        return false;
                                                    }
                                                    else if (doc.val() == "" || !ValidateSingleInput($('#document_attached')[0])) {
                                                        doc.focus();
                                                        return false;
                                                    }
                                                    else {
                                                        var token = $('#token').val();
                                                        var filename = $("#document_attached").val();
                                                        var formData = new FormData();
                                                        formData.append('file', $('#document_attached')[0].files[0]);
                                                        formData.append('_token', token);
                                                        formData.append('title', title.val());
                                                        formData.append('description', description.val());
                                                        formData.append('type', type.val());
                                                        jQuery.ajax({
                                                            type: "POST",
                                                            url: $('#DocumentForm').attr("action"),
                                                            enctype: 'multipart/form-data',
                                                            processData: false,
                                                            contentType: false,
                                                            data: formData,
                                                            beforeSend: function() {
                                                                $('#spinner_document').show();
                                                            },
                                                            success: function(result) {
                                                                var data = JSON.parse(result)
                                                                if (data.status) {
                                                                    //                                                    var title = data.data.document_title;
                                                                    //                                                    var description = data.data.document_description;
                                                                    //
                                                                    //                                                    var url = $("#base_url").val() + "/public/images/company_document/" + data.data.document_attached;
                                                                    //                                                    var action = "<a href='" + url + "' download='" + url + "' class='btn btn-primary'>Download</a>&nbsp<a href='" + url + "' download='" + url + "' class='btn btn-primary'>view</a>"
                                                                    //                                                    var row = '<tr><td>' + title + '</td><td>' + description + '</td><td>' + action + '</td></tr>';
                                                                    //                                                    $('#table_document').append(row);
                                                                    $('#spinner_document').hide();
                                                                    $('#close_document').trigger("click");
                                                                    location.reload();
                                                                }
                                                            }
                                                        });
                                                    }


                                                }
                                        );

                                        $('#billing_details').click(
                                                function() {
                                                    var token = $('#token').val();
                                                    var company = $('#company_id').val();
                                                    $.ajax({
                                                        url: $("#base_url").val() + "/billing-info-details",
                                                        type: 'POST',
                                                        data: {
                                                            company_id: company,
                                                            _token: token
                                                        },
                                                        //                                    beforeSend: function () {
                                                        //                                        $('#spinner_invoice').show();
                                                        //                                    },
                                                        success: function(result) {
                                                            //                                        alert('jamil mamoni');
                                                            if (result) {
                                                                //                                            alert('what the...');
                                                                //                                            $('#bill_description').attr(result);
                                                                $('#bill_description').html(result);
                                                            }
                                                        }

                                                    });
                                                }
                                        );

                                        $('#add_Tamplate').click(function(e) { // capture submit
                                            e.preventDefault();
                                            if ($('#template_type').val() == "") {
                                                $('#template_type').focus();
                                                return false;
                                            } else if ($('#template_attached').val() == "" || !ValidateSingleInput($('#template_attached')[0])) {
                                                $('#template_attached').focus();
                                                return false;
                                            }
                                            // var fd = new FormData(this); // XXX: Neex AJAX2
                                            // You could show a loading image for example...
                                            var formData = new FormData();
                                            formData.append('file', $('#template_attached')[0].files[0]);
                                            formData.append('template_type', $('#template_type').val());
                                            formData.append('_token', $('#token').val());
                                            $.ajax({
                                                url: $('#templateform').attr('action'),
                                                xhr: function() { // custom xhr (is the best)

                                                    var xhr = new XMLHttpRequest();
                                                    var total = 0;

                                                    // Get the total size of files
                                                    $.each(document.getElementById('template_attached').files, function(i, file) {
                                                        total += file.size;
                                                    });

                                                    // Called when upload progress changes. xhr2
                                                    xhr.upload.addEventListener("progress", function(evt) {
                                                        // show progress like example
                                                        var loaded = (evt.loaded / total).toFixed(2) * 100; // percent

                                                        //$('#progress').text('Uploading... ' + loaded + '%');
                                                    }, false);

                                                    return xhr;
                                                },
                                                type: 'post',
                                                processData: false,
                                                contentType: false,
                                                data: formData,
                                                success: function(data) {
                                                    // do something...
                                                    $('#template').hide();
                                                    $('#close_template').trigger("click");
                                                    location.reload();

                                                },
                                                error: function(data) {

                                                    alert("Please try again.Maybe permission Error");
                                                }
                                            });
                                        });

                                        function validateEmail(email) {
                                            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                                            return re.test(email);
                                        }
                                        function validateContact(contact) {
                                            var re = /^[+]?[0-9]{7,18}$/;
                                            return re.test(contact);
                                        }
                                    });
</script>

</div>
</div>
<style>
    .panel-body
    {
        min-height: 350px !important;
    }
    .nav-tabs-custom .nav-tabs li a {
        border-radius: 0;
        color: #444;
        padding: 7px 3px !important;
    }
    .nav-tabs-custom .nav-tabs li{
        border-top: 0px;
        border-right: 1px solid #e6e6e6;
    }
    .nav-tabs-custom .nav-tabs li:last-child{
        border-right: 0px;
    }
    .nav-tabs-custom .nav-tabs li.active {
        border-top: 0px;
        border-right: 1px solid #e6e6e6;;
        background: rgb(60, 141, 188) none repeat scroll 0% 0%;
    }
    .nav-tabs-custom .nav-tabs li.active a:hover{
        background: rgb(60, 141, 188) none repeat scroll 0% 0%;
        color: #fff;

    }
    .nav-tabs-custom .nav-tabs li.active a{
        background: transparent;
        color: #fff;
    }
    #collapsethree th, #contact_table td{
        padding: 3px !important;
    }
</style>
@stop
