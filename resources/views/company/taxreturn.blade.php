<div style=" display: none;width: 600px;" id="taxreturn" >
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="modal-header">
                <h4 class="modal-title">Add Annual Tax Return</h4>

                <div id="spinner_tax" style="display: none">
                    <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                </div>
            </div>
            <div class="modal-body">

                <form id="taxreturnForm" method="post"
                      action="{{URL::to('add-tax-return')."/".$company->company_id}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                    <div class="form-group">
                        <label for="client-name" class="control-label">Client Name:</label>
                        <input type="text" name="client_name" data-toggle="tooltip" class="form-control"
                               id="client_name" title="Enter Client Name"/>
                    </div>
                    <div class="form-group">
                        <label for="client-name" class="control-label">Accountant/Company Name:</label>
                        <input type="text" name="a_c_name" data-toggle="tooltip" class="form-control"
                               id="a_c_name" title="Enter accountant/company name"/>
                    </div>
                    <div class="form-group">
                        <label for="client-name" class="control-label">Taxation Date:</label>
                        <input type="text" name="taxation_date" data-toggle="tooltip" class="form-control datepicker"
                               id="taxation_date" title="Enter Taxation Date"/>
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="control-label">Documents:</label>
                        <input type="file" class="form-control" name="taxreturn_attached" id="taxreturn_attached">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"  id="close_taxreturn" onclick="$.fancybox.close();">Close
                </button>
                <button type="button" class="btn btn-primary" id="add_taxreturn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(
            function () {
                $('#add_taxreturn').click(
                        function () {
                            var client_name = $('#client_name');
                            var a_c_name = $('#a_c_name');
                            var taxation_date = $('#taxation_date');
                            var file = $('#taxreturn_attached');
                            if (client_name.val() == "") {
                                client_name.focus().mouseover();
                                return false;
                            } else if (a_c_name.val() == "") {
                                a_c_name.focus().mouseover();
                                return false;
                            } else if (taxation_date.val() == "") {
                                taxation_date.focus().mouseover();
                                return false;
                            } else if (file.val() == "" || !ValidateSingleInput($('#taxreturn_attached')[0])) {
                                file.focus().mouseover();
                                return false;
                            } else {
                                var token = $('#token').val();
                                var filename = $("#taxreturn_attached").val();
                                var formData = new FormData();
                                formData.append('file', $('#taxreturn_attached')[0].files[0]);
                                formData.append('_token', token);
                                formData.append('client_name', client_name.val());
                                formData.append('taxation_date', taxation_date.val());
                                formData.append('a_c_name', a_c_name.val());
                                jQuery.ajax({
                                    type: "POST",
                                    url: $('#taxreturnForm').attr("action"),
                                    enctype: 'multipart/form-data',
                                    processData: false,
                                    contentType: false,
                                    data: formData,
                                    beforeSend: function () {
                                        $('#spinner_tax').show();
                                    },
                                    success: function (result) {
                                        $('#close_taxreturn').hide();
                                        $('#close_taxreturn').trigger("click");
                                        location.reload();
                                    }
                                });
                            }


                        }
                );

                var _validFileExtensions = [".doc", ".docx", ".pdf"];
                function ValidateSingleInput(oInput) {
                    if (oInput.type == "file") {
                        var sFileName = oInput.value;
                        if (sFileName.length > 0) {
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensions.length; j++) {
                                var sCurExtension = _validFileExtensions[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }

                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                                oInput.value = "";
                                return false;
                            }
                        }
                    }
                    return true;
                }
            });
</script>

