<div style=" display: none" id="compliance" >
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="modal-header">
                <h4 class="modal-title">Add Compliance Report</h4>

                <div id="spinner_compliance" style="display: none">
                    <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                </div>
            </div>
            <div class="modal-body">

                <form id="ComplianceForm" method="post"
                      action="{{URL::to('add-compliance')."/".$company->company_id}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Compliance Title:</label>
                        <input type="text" name="compliance_title" data-toggle="tooltip" class="form-control"
                               id="compliance_title" title="Enter Compliance Title">
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="control-label">Description:</label>
                        <textarea id="compliance_content" name="compliance_content"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="control-label">Files:</label>
                        <input type="file" class="form-control" name="compliance_attached" id="compliance_attached">
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"  id="close_compliance" onclick="$.fancybox.close();">Close
                </button>
                <button type="button" class="btn btn-primary" id="add_compliance">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(
            function () {
                $('#add_compliance').click(
                        function () {
                            var title = $('#compliance_title');
                            var file = $('#compliance_attached');
                            if (title.val() == "") {
                                title.focus().mouseover();
                                return false;
                            } else if (tinyMCE.get('compliance_content').getContent() == "") {
                                tinyMCE.activeEditor.focus();
                                return false;
                            } else if (file.val() == "" || !ValidateSingleInput($('#compliance_attached')[0])) {
                                file.focus().mouseover();
                                return false;
                            } else {
                                var token = $('#token').val();
                                var filename = $("#compliance_attached").val();
                                var formData = new FormData();
                                formData.append('file', $('#compliance_attached')[0].files[0]);
                                formData.append('_token', token);
                                formData.append('title', title.val());
                                formData.append('description', tinyMCE.get('compliance_content').getContent());
                                jQuery.ajax({
                                    type: "POST",
                                    url: $('#ComplianceForm').attr("action"),
                                    enctype: 'multipart/form-data',
                                    processData: false,
                                    contentType: false,
                                    data: formData,
                                    beforeSend: function () {
                                        $('#spinner_compliance').show();
                                    },
                                    success: function (result) {
                                        $('#close_compliance').hide();
                                        $('#close_document').trigger("click");
                                        location.reload();
                                    }
                                });
                            }


                        }
                );

                var _validFileExtensions = [".doc", ".docx", ".pdf"];
                function ValidateSingleInput(oInput) {
                    if (oInput.type == "file") {
                        var sFileName = oInput.value;
                        if (sFileName.length > 0) {
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensions.length; j++) {
                                var sCurExtension = _validFileExtensions[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }

                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                                oInput.value = "";
                                return false;
                            }
                        }
                    }
                    return true;
                }
            });
</script>