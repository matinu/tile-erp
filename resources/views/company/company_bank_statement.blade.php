
<div style="width: 600px; display: none" id="statement" >
    <div class="col-md-12" >
        <div class="col-md-12">
            <div class="modal-header">

                <h4 class="modal-title">Add statement</h4>

                <div id="spinner_statement" style="display: none">
                    <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                </div>
            </div>
            <div class="modal-body">

                <form id="statementForm" method="post" action="{{URL::to('add-statement')."/".$company->company_id}}"
                      enctype="multipart/form-data">
                    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Statement Title:</label>
                        <input type="text" name="statement_title" class="form-control" id="statement_title">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Date of Issue:</label>
                        <input type="text" class="form-control datepicker" name="statement_doi" id="statement_doi">
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="control-label">Files:</label>
                        <input type="file" class="form-control" name="statement_attached" id="statement_attached">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default custom-close"  id="close_statement" onclick="$.fancybox.close();">Close</button>
                <button type="button" class="btn btn-primary" id="add_statement">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#add_statement').click(function (e) { // capture submit
        e.preventDefault();
        if ($('#statement_title').val() == "") {
            $('#statement_title').focus();
            return false;
        } else if ($('#statement_doi').val() == "") {
            $('#statement_doi').focus();
            return false;
        } else if ($('#statement_attached').val() == "" || !ValidateSingleInput($('#statement_attached')[0])) {
            $('#statement_attached').focus();
            return false;
        }
        // var fd = new FormData(this); // XXX: Neex AJAX2
        // You could show a loading image for example...
        var formData = new FormData();
        formData.append('file', $('#statement_attached')[0].files[0]);
        formData.append('statement_doi', $('#statement_doi').val());
        formData.append('statement_title', $('#statement_title').val());
        formData.append('_token', $('#token').val());
        $.ajax({
            url: $('#statementForm').attr('action'),
            xhr: function () { // custom xhr (is the best)

                var xhr = new XMLHttpRequest();
                var total = 0;

                // Get the total size of files
                $.each(document.getElementById('statement_attached').files, function (i, file) {
                    total += file.size;
                });

                // Called when upload progress changes. xhr2
                xhr.upload.addEventListener("progress", function (evt) {
                    // show progress like example
                    var loaded = (evt.loaded / total).toFixed(2) * 100; // percent

                    //$('#progress').text('Uploading... ' + loaded + '%');
                }, false);

                return xhr;
            },
            type: 'post',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                // do something...
                $('#statement').hide();
                $('#close_statement').trigger("click");
                location.reload();

            },
            error: function (data) {

                alert("Please try again.Maybe permission Error");
            }
        });
    });
    
    var _validFileExtensions = [".doc", ".docx", ".pdf"];
        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
</script>
