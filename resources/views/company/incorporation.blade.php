<div style="display: none" id="incorporation" >
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="modal-header">
                <h4 class="modal-title">Add Incorporation</h4>

                <div id="spinner_incorporation" style="display: none">
                    <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                </div>
            </div>
            <div class="modal-body">

                <form id="incorporationForm" method="post"
                      action="{{URL::to('add-incorporation')."/".$company->company_id}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Incorporation Title:</label>
                        <input type="text" name="document_title" data-toggle="tooltip" class="form-control"
                               id="incorporation_title" title="Enter incorporation Title">
                    </div>
                    <div class="form-group" >
                        <label for="message-text" class="control-label">Description:</label>
                        <textarea id="incorporation_description" name="incorporation_description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Files:</label>
                        <input type="file" class="form-control" name="incorporation_attached" id="incorporation_attached">
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"  id="close_incorporation" onclick="$.fancybox.close();">Close
                </button>
                <button type="button" class="btn btn-primary" id="add_incorporation">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#add_incorporation').click(function () {
            var title = $('#incorporation_title');
            var file = $('#incorporation_attached');
            if (title.val() == "") {
                title.focus().mouseover();
                return false;
            } else if (tinyMCE.get('incorporation_description').getContent() == "") {
                tinyMCE.activeEditor.focus();
                return false;
            }
            else if (file.val() == "" || !ValidateSingleInput($('#incorporation_attached')[0])) {
                file.focus().mouseover();
                return false;
            }
            else {
                var token = $('#token').val();
                var formData = new FormData();
                formData.append('file', $('#incorporation_attached')[0].files[0]);
                formData.append('_token', token);
                formData.append('title', title.val());
                formData.append('description', tinyMCE.get('incorporation_description').getContent());
                jQuery.ajax({
                    type: "POST",
                    url: $('#incorporationForm').attr("action"),
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                    beforeSend: function () {
                        $('#spinner_incorporation').show();
                    },
                    success: function (result) {
                        $('#close_correspondence').hide();
                        $('#close_document').trigger("click");
                        location.reload();
                    },
                    error: function (data) {
                        alert("Please try again.Maybe permission error");

                    }
                });
            }


        }
        );

        var _validFileExtensions = [".doc", ".docx", ".pdf"];
        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
    });
</script>