@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <div id="message">
            @if (Session::has('success_add_msg'))
            <div class="alert alert-info">{{ Session::pull('success_add_msg') }}</div>
            @endif
            @if (Session::has('success_update_msg'))
            <div class="alert alert-info">{{ Session::pull('success_update_msg') }}</div>
            @endif
            @if (Session::has('success_delete_msg'))
            <div class="alert alert-info">{{ Session::pull('success_delete_msg') }}</div>
            @endif
        </div>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Company Name</th>
                    <th>Company Type</th>
                    <th>Date of Incorporation</th>
                    <th>Status</th>
                    <th>Create Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($companies as $company)
                <tr>
                    <td>{{ $company->company_id }}</td>
                    <td>{{ $company->company_name }}</td>
                    <td>{{ $company->company_type }}</td>
                    <td>{{ date("jS F, Y", strtotime($company->date_of_incorporation)) }}</td>
                    <td>{{ $company->company_status }}</td>
                    <td>{{ date("jS F, Y", strtotime( $company->created_at)) }}</td>
                    <td><a href="{{URL::to('details-company')."/".$company->company_id}}" class="btn btn-primary">Details</a> <a href="{{URL::to('delete-company')."/".$company->company_id}}" class="btn btn-danger" onclick="if (confirm('Do you want to delete this client ?')) {
                                return;
                            } else {
                                return false;
                            }
                            ;">Delete</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.pagination')

    </div>

</div>
<script>

    $(document).ready(function () {
        $('#message').fadeOut(5000);
    });
</script>
@stop

