@extends('layout.admintamplate')

@section('content')
    <div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
        <div class="input-group col-md-8">
            <div class="alert alert-danger">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}

            </div>
        </div>
        <?php } ?>
        <form action="{{URL::to('/update-company')."/".$company->company_id}}" onsubmit="return checkValidation()" method="post" role="form" id="form_adduser"
              enctype="multipart/form-data">
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="previous_name" name="previous_name" value="{{$company->company_name}}">
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control" data-toggle="tooltip" name="company_name" id="company_name" placeholder="* Company Name" title="Enter Company name"
                           autocomplete="off" data-mask="email" value="{{$company->company_name}}"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <select name="company_type" class="form-control" data-toggle="tooltip" placeholder="* company type" id="company_type" title="Select Company Type">
                        <option value="">* Company Type</option>
                        <option value="Domestic" @if($company->company_type=="Domestic") selected @endif>Domestic</option>
                        <option value="IBC" @if($company->company_type=="IBC") selected @endif>IBC</option>
                        <option value="LLC"  @if($company->company_type=="LLC") selected @endif>LLC</option>
                        <option value="PVT"  @if($company->company_type=="PVT") selected @endif>PVT</option>
                        <option value="GBC1"  @if($company->company_type=="GBC1") selected @endif>GBC 1</option>
                        <option value="GBC2"  @if($company->company_type=="GBC2") selected @endif>GBC 2</option>
                        <option value="PLC"  @if($company->company_type=="PLC") selected @endif>PLC</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control datepicker" data-toggle="tooltip" name="date_of_incorporation" id="date_of_incorporation" title="Enter Date of incorporation"
                           placeholder="* Date of Incorporation" autocomplete="off" data-mask="email"
                           value="{{ date('m/d/Y',strtotime($company->date_of_incorporation))}}"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <select name="company_status" class="form-control" data-toggle="tooltip" placeholder="* company status" id="company_status" title="Select Company Status">
                        <option value="">* Company Status</option>
                        <option value="new" @if($company->company_status=="new") selected @endif>new</option>
                        <option value="refurbished" @if($company->company_status=="refurbished") selected @endif>refurbished</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">

                    <select name="company_country" class="form-control" data-toggle="tooltip" placeholder="* company Country" id="company_country" title="Select Company Country">
                        <option value="">* Company Country</option>
                        @foreach($countries as $country)
                            <option value="{{$country->name}}" @if($company->company_country==$country->name) selected @endif>{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control" data-toggle="tooltip" name="company_new_address" id="company_new_address" title="Enter New Address"
                           placeholder="* Company New Address" autocomplete="off" value="{{$company->company_new_address}}"/>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control" data-toggle="tooltip" name="company_old_address" id="company_old_address" title="Enter old Address"
                           placeholder="* Company Old Address" autocomplete="off" value="{{$company->company_old_address}}"/>
                </div>
            </div>
            <div class="form-group">
                <label>* Logo</label>

                <div class="input-group col-md-8">
                    <div class="col-md-8">
                        <p><img src="{{URL::to('public/images/company_logo/')."/".$company->company_logo}}"
                                width="200px" height="200px"/></p>
                    </div>
                    <input type="file" class="form-control" data-toggle="tooltip"  name="company_logo" id="company_logo" title="Enter Company Logo"
                           placeholder="Company logo"/>
                </div>
            </div>



            <div class="form-group">
                <div class="input-group col-md-8">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        function checkValidation()
        {
            var company_name = $('#company_name').val();

            if(company_name=="")
            {
                $('#company_name').focus().mouseover();
                return false;
            }

            var company_type = $('#company_type').val();

            if(company_type=="")
            {
                $('#company_type').focus().mouseover();
                return false;
            }
            var date_of_incorporation = $('#date_of_incorporation').val();

            if (date_of_incorporation == "") {
                $('#date_of_incorporation').focus().mouseover();
                return false;
            }

            var company_status = $('#company_status').val();

            if (company_status == "") {
                $('#company_status').focus().mouseover();
                return false;
            }

            var company_country = $('#company_country').val();

            if (company_country == "") {
                $('#company_country').focus().mouseover();
                return false;
            }

            var company_new_address = $('#company_new_address').val();

            if (company_new_address == "") {
                $('#company_new_address').focus().mouseover();
                return false;
            }

            var company_old_address = $('#company_old_address').val();

            if (company_old_address == "") {
                $('#company_old_address').focus().mouseover();
                return false;
            }





        }
        $(document).ready(
                function () {
                    $('[data-toggle="tooltip"]').tooltip();

                }
        );
    </script>
@stop