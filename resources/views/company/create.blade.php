@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
            <div class="input-group col-md-8">
                <div class="alert alert-danger">

                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}

                </div>
            </div>
        <?php } ?>
        <form action="{{URL::to('/save-company')}}" onsubmit="return checkValidation()" method="post" role="form" id="form_adduser"
              enctype="multipart/form-data">
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control" data-toggle="tooltip" name="company_name" id="company_name" placeholder="* Company Name" title="Enter Company name"
                           autocomplete="off" data-mask="email" value="{{old('company_name')}}"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <select name="company_type" class="form-control" data-toggle="tooltip" placeholder="* company type" id="company_type" title="Select Company Type">
                        <option value="">* Company Type</option>
                        <option value="Domestic">Domestic</option>
                        <option value="IBC">IBC</option>
                        <option value="LLC">LLC</option>
                        <option value="PVT">PVT</option>
                        <option value="GBC1">GBC 1</option>
                        <option value="GBC2">GBC 2</option>
                        <option value="PLC">PLC</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control datepicker" data-toggle="tooltip" name="date_of_incorporation" id="date_of_incorporation" title="Enter Date of incorporation"
                           placeholder="* Date of Incorporation" autocomplete="off" data-mask="email"
                           value="{{old('date_of_incorporation')}}"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <select name="company_status" class="form-control" data-toggle="tooltip" placeholder="* company status" id="company_status" title="Select Company Status">
                        <option value="">* Company Status</option>
                        <option value="new">new</option>
                        <option value="refurbished">refurbished</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">

                    <select name="company_country" class="form-control" data-toggle="tooltip" placeholder="* company Country" id="company_country" title="Select Company Country">
                        <option value="">* Company Country</option>
                        @foreach($countries as $country)
                        <option value="{{$country->name}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control" data-toggle="tooltip" name="company_new_address" id="company_new_address" title="Enter New Address"
                           placeholder="* Company New Address" autocomplete="off" value="{{old('company_new_address')}}"/>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control" data-toggle="tooltip" name="company_old_address" id="company_old_address" title="Enter old Address"
                           placeholder="* Company Old Address" autocomplete="off" value="{{old('company_old_address')}}"/>
                </div>
            </div>
            <div class="form-group">
                <label>* Logo</label>

                <div class="input-group col-md-8">

                    <input type="file" class="form-control" data-toggle="tooltip"  name="company_logo" id="company_logo" title="Enter Company Logo"
                           placeholder="Company logo"/>
                </div>
            </div>
            <div class="form-group">
                <label>Contact Person</label>

                <div id="contact_person">
                    <div id="contact">
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control person_name" name="person_name[]"
                                       placeholder="* person name" autocomplete="off" value="{{old('person_name[]')}}" title="Enter Person name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control person_designation" name="person_designation[]"
                                       placeholder="* person designation" autocomplete="off" title="Enter Person Designation"
                                       value="{{old('person_designation[]')}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <select class="form-control person_type" id="person_type" name="person_type[]" data-toggle="tooltip" title="Select Person Type">
                                    <option value="">---Select Type---</option>
                                    <option value="1" @if(old('person_type[]')==1){{"selected='selected'"}}@endif>Ultimate Beneficial Owner</option>
                                    <option value="2" @if(old('person_type[]')==2){{"selected='selected'"}}@endif>Shareholder</option>
                                    <option value="3" @if(old('person_type[]')==3){{"selected='selected'"}}@endif>Director</option>
                                    <option value="4" @if(old('person_type[]')==4){{"selected='selected'"}}@endif>Trustees</option>
                                    <option value="5" @if(old('person_type[]')==5){{"selected='selected'"}}@endif>Others</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control person_contact_number"
                                       name="person_contact_number[]" title="Enter Person Contact Number"
                                       placeholder="* person contact number" autocomplete="off"
                                       value="{{old('person_contact_number[]')}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control person_email" name="person_email[]" title="Enter Person Email"
                                       placeholder="* person email" autocomplete="off" value="{{old('person_email[]')}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control datepicker doappointment" name="doappointment[]"
                                       placeholder="* Person date of Appointment" autocomplete="off" value="{{old('doappointment[]')}}" title="Enter Person date of Appointment"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control datepicker doresignation" name="doresignation[]"
                                       placeholder="Person date of resignation" autocomplete="off" value="{{old('doresignation[]')}}" title="Enter Person date of Resignation"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control director" name="director[]"
                                       placeholder="Director" autocomplete="off" value="{{old('director[]')}}" title="Enter Director"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control shareholder" name="shareholder[]"
                                       placeholder="Share Holder" autocomplete="off" value="{{old('shareholder[]')}}" title="Enter Shareholder"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control secretary" name="secretary[]"
                                       placeholder="Company secretary" autocomplete="off" value="{{old('secretary[]')}}" title="Enter Company Secretary"/>
                            </div>
                        </div>

                    </div>

                </div>
                <button id="add_more_person" type="button" class="btn btn-primary">(+)Add More</button>
            </div>
            <div class="form-group">
                <label>Company Document</label>

                <div id="company_document">
                    <div id="document">
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control document_title" name="document_title[]" title="Enter Document title"
                                       placeholder="* document title" autocomplete="off"
                                       value="{{old('document_title[]')}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="text" data-toggle="tooltip" class="form-control document_description" title="Enter Document Description"
                                       name="document_description[]"
                                       placeholder="* document description" autocomplete="off"
                                       value="{{old('person_designation[]')}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-8">

                                <input type="file" data-toggle="tooltip" class="form-control company_document" name="company_document[]" title="Enter document"
                                       placeholder="* Company logo"/>
                            </div>
                        </div>

                    </div>

                </div>
                <button id="add_more_document" type="button" class="btn btn-primary">(+)Add More</button>
            </div>
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
                <div class="input-group col-md-8">
                    <button type="submit" class="btn btn-primary">
                        Add
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        function checkValidation()
        {
            var company_name = $('#company_name').val();

            if (company_name == "")
            {
                $('#company_name').focus().mouseover();
                return false;
            }

            var company_type = $('#company_type').val();

            if (company_type == "")
            {
                $('#company_type').focus().mouseover();
                return false;
            }
            var date_of_incorporation = $('#date_of_incorporation').val();

            if (date_of_incorporation == "") {
                $('#date_of_incorporation').focus().mouseover();
                return false;
            }

            var company_status = $('#company_status').val();

            if (company_status == "") {
                $('#company_status').focus().mouseover();
                return false;
            }

            var company_country = $('#company_country').val();

            if (company_country == "") {
                $('#company_country').focus().mouseover();
                return false;
            }

            var company_new_address = $('#company_new_address').val();

            if (company_new_address == "") {
                $('#company_new_address').focus().mouseover();
                return false;
            }

            var company_old_address = $('#company_old_address').val();

            if (company_old_address == "") {
                $('#company_old_address').focus().mouseover();
                return false;
            }

            var company_logo = $('#company_logo').val();

            if (company_logo == "") {
                $('#company_logo').focus().mouseover();
                return false;
            }

            var company_logo = $('#company_logo').val();

            if (company_logo == "") {
                $('#company_logo').focus().mouseover();
                return false;
            }


            var name = $('.person_name').last();
            var designation = $('.person_designation').last();
            var contact = $('.person_contact_number').last();
            var email = $('.person_email').last();
            if (name.val() == "") {
                name.focus().mouseover();
                return false;
            }
            else if (designation.val() == "") {
                designation.focus().mouseover();
                return false;
            }

            else if (contact.val() == "") {
                contact.focus().mouseover();
                return false;
            }

            else if (email.val() == "") {
                email.focus().mouseover();
                return false;
            } else if (!IsEmail(email.val())) {
                email.focus().mouseover();
                return false;
            }

            var doc = $('.company_document').last();
            var title = $('.document_title').last();
            var description = $('.document_description').last();
            if (title.val() == "") {
                title.focus().mouseover();
                return false;
            } else if (description.val() == "") {
                description.focus().mouseover();
                return false;
            }
            else if (doc.val() == "") {
                doc.focus().mouseover();
                return false;
            }
        }
        $(document).ready(
                function () {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('#add_more_person').click(
                            function () {
                                var name = $('.person_name').last();
                                var designation = $('.person_designation').last();
                                var type = $('.person_type').last();
                                var contact = $('.person_contact_number').last();
                                var email = $('.person_email').last();
                                var doappointment = $('.doappointment').last();
                                var director = $('.director').last();
                                if (name.val() == "") {
                                    name.focus().mouseover();
                                    return false;
                                }
                                else if (designation.val() == "") {
                                    designation.focus().mouseover();
                                    return false;
                                }

                                else if (type.val() == "") {
                                    type.focus().mouseover();
                                    return false;
                                }
                                else if (contact.val() == "") {
                                    contact.focus().mouseover();
                                    return false;
                                }

                                else if (email.val() == "") {
                                    email.focus().mouseover();
                                    return false;
                                } else if (!IsEmail(email.val())) {
                                    email.focus().mouseover();
                                    return false;
                                } else if (doappointment.val() == "") {
                                    doappointment.focus().mouseover();
                                    return false;
                                }
                                else if (director.val() == "") {
                                    director.focus().mouseover();
                                    return false;
                                }
                                else {
                                    var html = $('#contact').html();
                                    $('#contact_person').append(html);
                                }


                            }
                    );
                    $('#add_more_document').click(
                            function () {
                                var doc = $('.company_document').last();
                                var title = $('.document_title').last();
                                var description = $('.document_description').last();
                                if (title.val() == "") {
                                    title.focus().mouseover();
                                    return false;
                                } else if (description.val() == "") {
                                    description.focus().mouseover();
                                    return false;
                                }
                                else if (doc.val() == "") {
                                    doc.focus().mouseover();
                                    return false;
                                }
                                else {
                                    var html = $('#document').html();
                                    $('#company_document').append(html);
                                }


                            }
                    );
                }
        );
    </script>
    @stop