<div style=" display: none;width: 600px;" id="financial" >
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="modal-header">
                <h4 class="modal-title">Financial Statements</h4>

                <div id="spinner_financial" style="display: none">
                    <img src="{{URL::to('public/images/giphy.gif')}}" height="50px" width="50px"/>
                </div>
            </div>
            <div class="modal-body">

                <form id="financialForm" method="post"
                      action="{{URL::to('add-financial-statements')."/".$company->company_id}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                    <div class="form-group">
                        <label for="f_client-name" class="control-label">Client Name:</label>
                        <input type="text" name="client_name" data-toggle="tooltip" class="form-control"
                               id="f_client_name" title="Enter Client Name"/>
                    </div>
                    <div class="form-group">
                        <label for="f_bank_name" class="control-label">Bank Name:</label>
                        <input type="text" name="bank_name" data-toggle="tooltip" class="form-control"
                               id="f_bank_name" title="Enter Bank name"/>
                    </div>
                    <div class="form-group">
                        <label for="f_issue-date" class="control-label">Issue Date:</label>
                        <input type="text" name="issue_date" data-toggle="tooltip" class="form-control datepicker"
                               id="f_issue_date" title="Enter Issue Date"/>
                    </div>

                    <div class="form-group">
                        <label for="f_documents" class="control-label">Documents:</label>
                        <input type="file" class="form-control" name="financial_attached" id="f_financial_attached">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"  id="close_financial" onclick="$.fancybox.close();">Close
                </button>
                <button type="button" class="btn btn-primary" id="add_financial">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(
            function () {
                $('#add_financial').click(
                        function () {
                            var client_name = $('#f_client_name');
                            var a_c_name = $('#f_bank_name');
                            var taxation_date = $('#f_issue_date');
                            var file = $('#f_financial_attached');
                            if (client_name.val() == "") {
                                client_name.focus().mouseover();
                                return false;
                            } else if (a_c_name.val() == "") {
                                a_c_name.focus().mouseover();
                                return false;
                            } else if (taxation_date.val() == "") {
                                taxation_date.focus().mouseover();
                                return false;
                            } else if (file.val() == "" || !ValidateSingleInput($('#f_financial_attached')[0])) {
                                file.focus().mouseover();
                                return false;
                            } else {
                                var token = $('#token').val();
                                var filename = $("#f_financial_attached").val();
                                var formData = new FormData();
                                formData.append('file', $('#f_financial_attached')[0].files[0]);
                                formData.append('_token', token);
                                formData.append('client_name', client_name.val());
                                formData.append('issue_date', taxation_date.val());
                                formData.append('bank_name', a_c_name.val());
                                jQuery.ajax({
                                    type: "POST",
                                    url: $('#financialForm').attr("action"),
                                    enctype: 'multipart/form-data',
                                    processData: false,
                                    contentType: false,
                                    data: formData,
                                    beforeSend: function () {
                                        $('#spinner_financial').show();
                                    },
                                    success: function (result) {
                                        $('#close_financial').hide();
                                        $('#close_financial').trigger("click");
                                        location.reload();
                                    }
                                });
                            }


                        }
                );

                var _validFileExtensions = [".doc", ".docx", ".pdf"];
                function ValidateSingleInput(oInput) {
                    if (oInput.type == "file") {
                        var sFileName = oInput.value;
                        if (sFileName.length > 0) {
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensions.length; j++) {
                                var sCurExtension = _validFileExtensions[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }

                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                                oInput.value = "";
                                return false;
                            }
                        }
                    }
                    return true;
                }
            });
</script>

