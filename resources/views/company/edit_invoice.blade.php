@extends('layout.admintamplate')

@section('content')
    <div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
        <div class="input-group col-md-8">
            <div class="alert alert-danger">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}

            </div>
        </div>
        <?php } ?>
        <form action="{{URL::to('/update-credit-invoice')."/".$credit_invoice->id}}" onsubmit="return checkValidation()" method="post" role="form" id="form_adduser"
              enctype="multipart/form-data">
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <form>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Invoice Date:</label>
                    <input type="text" class="form-control datepicker" data-toggle="tooltip"
                           name="invoice_date" id="invoice_date"
                           title="Enter Invoice Date"
                           placeholder="Date of correspondence" autocomplete="off" data-mask="email" value="{{ date('m/d/Y',strtotime($credit_invoice->invoice_date))}}"/>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Invoice Title:</label>
                    <input type="text" name="invoice_title" data-toggle="tooltip" class="form-control"
                           id="invoice_title" title="Enter Invoice Title" value="{{$credit_invoice->invoice_title}}">
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Invoice Description:</label>
                            <textarea  class="form-control" data-toggle="tooltip" name="invoice_description"
                                       id="invoice_description" title="Enter Invoice Description" >{{$credit_invoice->invoice_description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Hours:</label>
                    <input type="number" name ="invoice_hours" class="form-control" id="invoice_hours" value="{{$credit_invoice->invoice_hours}}">
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Status:</label>
                    <select name ="invoice_status" id="invoice_status" class="form-control">
                        <option value="pending" @if($credit_invoice->invoice_status=="pending") selected @endif>Pending</option>
                        <option value="paid" @if($credit_invoice->invoice_status=="paid") selected @endif>Paid</option>
                    </select>
                </div>





            <div class="form-group">
                <div class="input-group col-md-8">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        function checkValidation()
        {
            var company_name = $('#company_name').val();

            if(company_name=="")
            {
                $('#company_name').focus().mouseover();
                return false;
            }

            var company_type = $('#company_type').val();

            if(company_type=="")
            {
                $('#company_type').focus().mouseover();
                return false;
            }
            var date_of_incorporation = $('#date_of_incorporation').val();

            if (date_of_incorporation == "") {
                $('#date_of_incorporation').focus().mouseover();
                return false;
            }

            var company_status = $('#company_status').val();

            if (company_status == "") {
                $('#company_status').focus().mouseover();
                return false;
            }

            var company_country = $('#company_country').val();

            if (company_country == "") {
                $('#company_country').focus().mouseover();
                return false;
            }

            var company_new_address = $('#company_new_address').val();

            if (company_new_address == "") {
                $('#company_new_address').focus().mouseover();
                return false;
            }

            var company_old_address = $('#company_old_address').val();

            if (company_old_address == "") {
                $('#company_old_address').focus().mouseover();
                return false;
            }





        }
        $(document).ready(
                function () {
                    $('[data-toggle="tooltip"]').tooltip();

                }
        );
    </script>
@stop