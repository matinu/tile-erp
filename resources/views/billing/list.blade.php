@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">

        @if (Session::has('success_add_msg'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::pull('success_add_msg') }}
        </div>
        @endif
        @if (Session::has('success_update_msg'))
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::pull('success_update_msg') }}
        </div>
        @endif
        @if (Session::has('success_delete_msg'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::pull('success_delete_msg') }}
        </div>
        @endif

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Billing Period</th>
                    <th>Hourly Rate</th>
                    <th>Cycle Start Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($billings as $bill)
                <tr>
                    <td>{{ $bill->company->company_name}}</td>
                    <td>{{ $bill->billing_period }}</td>
                    <td>{{ $bill->hourly_rate }}</td>
                    <td>{{ date("jS F, Y", strtotime($bill->cycle_start_date)) }}</td>
                    <td><a href="{{URL::to('edit-billing')."/".$bill->id}}" class="btn btn-primary">Edit</a> <a href="{{URL::to('delete-billing')."/".$bill->id}}" class="btn btn-danger" onclick="if (confirm('Do you want to delete this client ?')) {
                                return;
                            } else {
                                return false;
                            }
                            ;">Delete</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.pagination')

    </div>

</div>
<script>

    $(document).ready(function () {
        $('#message').fadeOut(5000);
    });
</script>
@stop

