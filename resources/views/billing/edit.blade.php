@extends('layout.admintamplate')

@section('content')
    <div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
        <div class="input-group col-md-8">
            <div class="alert alert-danger">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}

            </div>
        </div>
        <?php } ?>
        <form action="{{URL::to('/update-billing')."/".$billing->id}}" method="post" role="form" id="form_adduser"
              enctype="multipart/form-data">
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <div class="input-group col-md-8">

                    <select name="company_id" name="company_id" class="form-control" data-toggle="tooltip" placeholder="* company " title="Select Company">
                        <option value="">* Company</option>
                        @foreach($companies as $company)
                            <option value="{{$company->company_id}}" @if($company->company_id==$billing->company_id) selected @endif>{{$company->company_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <select name="billing_period" class="form-control" data-toggle="tooltip" placeholder="* billing period" id="billing_period" title="Select Billing Period">
                        <option value="">* Billing Period</option>
                        <option value="daily" @if($billing->billing_period=="daily") selected @endif>daily</option>
                        <option value="weekly"  @if($billing->billing_period=="weekly") selected @endif>weekly</option>
                        <option value="monthly" @if($billing->billing_period=="monthly") selected @endif>monthly</option>
                        <option value="quarterly" @if($billing->billing_period=="quarterly") selected @endif>quarterly</option>

                    </select>

                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-8">

                    <input type="text" class="form-control datepicker" data-toggle="tooltip" name="cycle_start_date" id="cycle_start_date" title="Enter cycle start date"
                           placeholder="* cycle start date" autocomplete="off" data-mask="email"
                           value="{{ date('m/d/Y',strtotime($billing->cycle_start_date))}}"/>
                </div>
            </div>
             <div class="form-group">
                <div class="input-group col-md-8">
                    <input type="number" class="form-control" data-toggle="tooltip" name="hourly_rate" id="hourly_rate" title="Enter Hourly Rate"
                           placeholder="* Hourly Rate" autocomplete="off" value="{{$billing->hourly_rate}}"/>
                </div>
            </div>




            <div class="form-group">
                <div class="input-group col-md-8">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        $(document).ready(
                function () {
                    $('[data-toggle="tooltip"]').tooltip();

                }
        );
    </script>
@stop