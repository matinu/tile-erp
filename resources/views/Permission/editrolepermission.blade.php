@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}
            </div>
        <?php } ?>
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>
        <form action="{{URL::to('edit-role-permission/'.$roles_id)}}" method="post" role="form" id="form_addrole">
            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <select name="role" id="role" class="form-control">
                        <option value="">---Select User---</option>
                        @foreach($roles as $role)
                        <option value="{{$role->id}}" @if(old('role')==$role->id){{"selected='selected'"}}@elseif ($roles_id==$role->id){{"selected='selected'"}}@endif>{{$role->display_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->
                    <select multiple name="permission[]" id="permission" class="form-control">
                        <option value="">---Select Permissions---</option>
                        @foreach($permissions as $permission)
                        <option value="{{$permission->id}}" @if(in_array($permission->id, $role_permission)) selected='selected'@endif>{{$permission->display_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <!--<i class="entypo-login"></i>-->
                    Add
                </button>
            </div>
        </form>
    </div>
</div>
@stop
