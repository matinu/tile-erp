@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Description</th>
                    <th>Created Date</th>
                    <th>Updated Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($list as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->display_name }}</td>
                    <td>{{ $role->description }}</td>
                    <td>{{ $role->created_at }}</td>
                    <td>{{ $role->updated_at }}</td>
                    <td><a href="{{ URL::to('edit-permission/'.$role->id) }}" class="btn btn-primary">Edit</a> <a href="{{ URL::to('delete-permission/'.$role->id) }}" class="btn btn-danger" onclick="if (confirm('Do you want to delete this client ?')) {
                                return;
                            } else {
                                return false;
                            }
                            ;">Delete</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.pagination')
    </div>

</div>
@stop

