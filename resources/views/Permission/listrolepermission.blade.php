@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>User Email</th>
                    <th>Role Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($list as $role)
                <tr>
                    <td>{{ $role->display_name }}</td>

                    <td>
                        <?php $last = ""; ?>
                        @foreach ($role['perms'] as $per)
                        <?php $last.=$per->display_name . ", " ?>
                        @endforeach
                        <?php $last = rtrim($last, ", ") ?>
                        {{ $last }}
                    </td>

                    <td><a href="{{ URL::to('edit-role-permission/'.$role->id) }}" class="btn btn-primary">Edit</a> <a href="{{ URL::to('delete-role-permission/'.$role->id) }}" class="btn btn-danger" onclick="if (confirm('Do you want to delete this client ?')) {
                            return;
                        } else {
                            return false;
                        }
                        ;">Delete</a></td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>

</div>
@stop

