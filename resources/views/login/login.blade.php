<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <meta name="description" content="login til erp admin panel" />
        <meta name="author" content="" />

        <title>ERP-Login</title>
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="{{URL::to('resources/assets/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{URL::to('resources/assets/css/AdminLTE.min.css')}}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{URL::to('resources/assets/js/iCheck/square/blue.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{URL::to('')}}"><img src="{{URL::to('resources/assets/img/logo/logo.gif')}}" height="60" alt="" /></a>
                <h2 style="color:#A6A6A6; font-weight:100;">
                    <small>Enterprise Resource Planning</small>  
                </h2>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <!-- progress bar indicator -->
                <?php if ($errors->first() != "") { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ $errors->first() }}
                    </div>
                <?php } ?>
                <?php if (isset($invalid)) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p style="text-align: center;">Invalid login</p>
                        <?php if($invalid==1){ ?>
                        <p>Please enter correct email and password!</p>
                        <?php }elseif ($invalid==0) {?>
                         <p>Please valid your Account or contact with administrator.</p>           
                        <?php }elseif ($invalid==2) { ?>
                          <p>Sorry! You account has been benned.</p> 
                        <?php }elseif ($invalid==-1){?>
                          <p>Sorry invalid email or password.</p>
                          <?php } ?>
                    </div>
                <?php } ?>
                <form action="" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" value="@if(isset($email)){{$email}}@else{{old('email')}}@endif" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" value="@if(isset($password)){{$password}}@else{{old('password')}}@endif" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <!--                        <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                        <label>
                                                            <input type="checkbox"> Remember Me
                                                        </label>
                                                    </div>
                                                </div> /.col -->
                        <div class="col-xs-4" style="float: right">
                            <button type="submit" name="submit" id="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div><!-- /.col -->
                    </div>
                </form>
                <!--                <div class="social-auth-links text-center">
                                    <p>- OR -</p>
                                    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
                                    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
                                </div>-->
                <!-- /.social-auth-links -->

                <!--<a href="#">I forgot my password</a><br>-->
                <!--<a href="register.html" class="text-center">Register a new membership</a>-->

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="{{URL::to('resources/assets/js/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="{{URL::to('resources/assets/js/bootstrap.min.js')}}"></script>
        <!-- iCheck -->
        <script src="{{URL::to('resources/assets/js/iCheck/icheck.min.js')}}"></script>
        <!--<script src="{{URL::to('resources/assets/js/neon-login.js')}}"></script>-->

    </body>
</html>