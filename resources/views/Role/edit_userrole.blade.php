@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}
            </div>
        <?php } ?>
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>
        <form action="{{URL::to('edit-user-role/'.$userrole[0]->user_id)}}" method="post" role="form" id="form_addrole">
            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <select name="user" id="user" class="form-control">
                        <option value="">---Select User---</option>
                        @foreach($users as $user)
                        <option value="{{$user->id}}" @if(old('user')==$user->id){{"selected='selected'"}}@elseif ($userrole[0]->user_id==$user->id){{"selected='selected'"}}@endif >{{$user->email}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->
                    <select name="role" id="role" class="form-control">
                        <option value="">---Select Role---</option>
                        @foreach($roles as $role)
                        <option value="{{$role->id}}" @if(old('role')==$role->id){{"selected='selected'"}}@elseif($role->id==$userrole[0]->role_id){{"selected='selected'"}}@endif>{{$role->display_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <!--<i class="entypo-login"></i>-->
                    Add
                </button>
            </div>
        </form>
    </div>
    @stop
