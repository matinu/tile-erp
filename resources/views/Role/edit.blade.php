@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}
            </div>
        <?php } ?>
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>

        <form action="{{URL::to('edit-role/'.$role->id)}}" method="post" role="form" id="form_addrole">
            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <input type="text" class="form-control" disabled="disabled" placeholder="Role Name" autocomplete="off" data-mask="name" value="@if(isset($role->name)){{$role->name}}@endif"/>
                </div>
            </div>


            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <input type="text" class="form-control" name="display_name" id="display_name" placeholder="Display Role Name" autocomplete="off" data-mask="displayrole"  value="@if(isset($role->display_name)){{$role->display_name}}@else{{old('display_name')}}@endif"/>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <textarea name="description" id="description" placeholder="Set Role Discription" class="form-control" rows="3">@if(isset($role->description)){{$role->description}}@else{{old('description')}}@endif</textarea>

                </div>
            </div>

            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <!--<i class="entypo-login"></i>-->
                    Update
                </button>
            </div>
        </form>
    </div>
    @stop
