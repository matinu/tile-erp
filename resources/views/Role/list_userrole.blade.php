@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>User Email</th>
                    <th>Role Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($list as $role)
                <?php if (isset($role['roles'][0])) { ?>
                    <tr>
                        <td>{{ $role->email }}</td>
                        <td>{{ $role['roles'][0]->display_name }}</td>
                        <td><a href="{{ URL::to('edit-user-role/'.$role->id) }}" class="btn btn-primary">Edit</a> <a href="{{ URL::to('delete-user-role/'.$role->id) }}" class="btn btn-danger" onclick="if (confirm('Do you want to delete this client ?')) {
                                return;
                            } else {
                                return false;
                            }
                            ;">Delete</a></td>

                    </tr>
                <?php } ?>
                @endforeach

            </tbody>
        </table>

    </div>

</div>
@stop

