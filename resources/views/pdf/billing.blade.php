<html>
<head>
    <title>Billing Details Pdf</title>
    <style>
        body{
            font-family: "Open Sans", sans-serif;
            font-size:12px;
        }
        table {
            border-collapse: collapse;
            text-align:left;
        }
        p{margin-bottom:-7px;}

        table, td, th {
            border: 1px solid #e6e6e6;
            padding:10px 5px;
        }
        th{background:#d8d8d8;padding:7px 5px;}
    </style>
</head>
<body>
<div style="width:100%;overflow: hidden;margin: 0 0 10px 0;">
    <div style="margin-bottom:20px;width: 35%;float: left">
        <p style="font-size:15px;font-weight:bold;">Til ERP</p>
        <p>kha-51,khilkhet(west),Nikunjo-2</p>
        <p>Road-14, House-16(1st floor), Nikunjo-2</p>
        <p>Dhaka 1229</p>
        <p>Bangladesh</p>
    </div>
    <div style="width: 200px;margin-left: 256px;"><img style="width:200px;height: 100px;" src="{{URL::to('public/images/company_logo/')."/".$clientInfo->company_logo}}"></div>
</div>

<div style="width:100%;overflow: visible;margin: 0 0 10px 0;position: relative;min-height: 100px;padding-bottom: 25px;">
    <div style="width: 49%;display: inline-block;">
        <p style="font-size:15px;font-weight:bold;margin-top: 5px;">{{-- */ echo $clientInfo->company_name; /* --}}</p>
        <p style="line-height:10px;">{{-- */ echo $clientInfo->company_new_address; /* --}}</p>
        <p>Contact Person Name : {{-- */ echo $clientInfo->person_name; /* --}}</p>
        <p>Phone : {{-- */ echo $clientInfo->person_contact_number; /* --}}</p>
    </div>
<div style="width: 40%; display: inline-block;float: right;">
        <table border="0" style="width: 100%; padding: 0;margin: 0">
            <tr>
                <td style="padding: 4px 0;">Issue Date</td>
                <td style="text-align: right; padding: 4px 0;">{{-- */ echo date('d-m-Y', time()); /* --}}</td>
            </tr>
            <tr>
                <td style="padding: 4px 0;">Due Date</td>
                <td style="text-align: right; padding: 4px 0;">{{-- */ echo date('d-m-Y', strtotime("+30 day")); /* --}}</td>
            </tr>
            <tr style="background-color: #d8d8d8;font-weight: bold;padding:10px 5px;border:1px solid #e6e6e6;">
                <td>Amount Due</td>
                <td style="text-align: right;">$ {{-- */ echo $grandTotal; /* --}}</td>
            </tr>
        </table>
    </div>
</div>

<div style="width:100%;overflow: hidden;margin: 0 0 10px 0;position: relative;">
    <table style="font-size:12px;width:100%;margin: 0px;padding: 0px; text-align: center;">
        <tr>
            <th style="width: 15%;">Id</th>
            <th >Title</th>
            <th style="width: 10%;">Hour(s)</th>
            <th style="width: 17%;">Rate / Hour(s)</th>
            <th style="width: 17%;">Total</th>
            <th style="width: 19%;">Date</th>
        </tr>
        {{-- */ echo $items; /* --}}
    </table>
</div>
<div style="width: 100%;display: block;overflow: visible;">
<div style="width:320px;float:right;">
    <table border="0" style="width: 100%; padding: 0;margin: 0">
        <tr>
            <td style="padding: 4px 0;">Grand Total</td>
            <td style="text-align: right; padding: 4px 0;">${{-- */ echo $grandTotal; /* --}}</td>
        </tr>
        <tr>
            <td style="padding: 4px 0;">Average Rate</td>
            <td style="text-align: right; padding: 4px 0;">${{-- */ echo round($averageRate, 4); /* --}}</td>
        </tr>
        <tr>
            <td style="padding: 4px 0;">Amount Paid</td>
            <td style="text-align: right; padding: 4px 0;">-${{-- */ echo 0; /* --}}</td>
        </tr>
        <tr style="background-color: #d8d8d8;font-weight: bold;padding:10px 5px;border:1px solid #e6e6e6;">
            <td>Amount Due</td>
            <td style="text-align: right;">${{-- */ echo $grandTotal; /* --}}</td>
        </tr>
    </table>
</div>
</div>
<div style="display: block; width: 100%; background-color: #d8d8d8; font-weight: bold; padding: 8px 5px; position: fixed; bottom: -10px; text-align: center;"><span>Powered by Workspace Infotech</span></div>
</body>
</html>