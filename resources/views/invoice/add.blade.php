@extends('layout.admintamplate')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <div class="box-title">
                <h3>
                    {{$pageTitle}}
                </h3>
            </div>
            <div class="box-content">
                <form action="{{URL::to('create-invoice')}}" method="POST" class='form-horizontal form-validate' id="create-invoice" >
                    <div class="step" id="firstStep">

                        <div class="step-forms">

                            <div class="form-group">
                                <label for="client" class="control-label col-sm-2">Client*</label>
                                <div class="col-sm-10">
                                    <select name="client" id="client" required="requried" class='form-control' data-rule-required="true">
                                        <option value="">Select Client</option>
                                        
                                        @foreach($clients as $client)
                                        <option value="{{$client->company_id}}">{{$client->company_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has())
                                        <div class="alert-danger">
                                            {{$errors->first('client')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

<!--                            <div class="form-group">
                                <label for="project" class="control-label col-sm-2">Project</label>
                                <div class="col-sm-10">
                                    <select name="project_id" id="project" class='form-control' data-rule-required="true">
                                        <option value="">Select Project</option>
                                    </select>
                                    @if ($errors->has())
                                        <div class="alert-danger">
                                            {{$errors->first('project_id')}}
                                        </div>
                                    @endif
                                </div>
                            </div>-->
<!--                            <div id="invoiceExistShow">
                                <div id="budgetDiv">Project Budget: <span id="budget"></span></div>
                                <div id="totalInvoiceDiv">Invoice Paid: <span id="totalInvoice"></span></div>
                            </div>-->
                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Date of Issue*</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="doi" id="doi" required="requried" class="form-control datepicker" data-rule-required="true">
                                    @if ($errors->has())
                                        <div class="alert-danger">
                                            {{$errors->first('doi')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="poNumber" class="control-label col-sm-2">P.O Number*</label>
                                <div class="col-sm-10">
                                    <input type="number" name="poNumber" id="poNumber" required="requried" class="form-control" data-rule-required="true">
                                    @if ($errors->has())
                                        <div class="alert-danger">
                                            {{$errors->first('poNumber')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="discount" class="control-label col-sm-2">Discount</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text" name="discount" id="discount" class="form-control" data-rule-minlength="1">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="terms" class="control-label col-sm-2">Terms</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <textarea name="terms" id="terms" rows="5" cols="80" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="notes" class="control-label col-sm-2">Notes</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <textarea name="notes" id="notes" rows="5" cols="80" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-table"></i>
                                        Items
                                    </h3>
                                </div>
                                <div class="box-content nopadding">
                                    <span id="errmsg"></span>
                                    <table class="table table-nomargin table-bordered table-condensed" id="item-table">
                                        <thead>
                                            <tr>
                                                <th>Item(s)</th>
                                                <th>Description</th>
                                                <th>Unit / Per Hour Cost</th>
                                                <th>Qty / Hour(s)</th>
                                                <!--<th>Tax</th>-->
                                                <!--<th>Tax</th>-->

                                                <th width="15%">Line Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for($i=1; $i<=4; $i++)
                                            <tr class="item-row">
                                                <td><input class="form-control text-center item" type="text" name="item[]"></td>
                                                <td><input class="form-control text-center desc" type="text" name="desc[]"></td>
                                                <td><input type="text" name="unit[]"  class="form-control unit text-center unit"></td>
                                                <td><input type="text" name="qty[]" class="form-control qty text-center qty"></td>
                                                <!--<td><input type="text" name="tax1[]" class="form-control text-center tax1"></td>-->
                                                <!--<td><input type="text" name="tax2[]" class="form-control text-center tax2"></td>-->
                                                <td><span class ="total text-center"></span><i class="glyphicon glyphicon-remove pull-right crs-icn"></i><i class ="glyphicon glyphicon-plus pull-right add-icn"></i></td>
                                            </tr>
                                            @endfor
                                            <tr>
                                                <td colspan="4" class="text-right">Sub-Total</td>
                                                <td class="text-center"><div id="subTotal"></div></td>

                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-right">Discount</td>
                                                <td class="text-center"><input id="discount-table" name="discount-amount" class="text-center"></td>

                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-right">Total</td>
                                                <td id="grandTotal" class="text-center"></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <span id="amounterror" style="color: red;" class="control-label col-sm-10"></span>
                    <div class="form-actions">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="reset" class="btn" value="Back" id="back">
                        <input type="submit" class="btn btn-primary" value="Submit" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $(document).on('keypress', '#item-table tr .unit', function(e) {
            //$('#item-table tr .unit').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        $(document).on('focusout', '#item-table tr .unit', function(e) {
            //$('#item-table tr .unit').focusout(function() {
            calculate();
        });
        $(document).on('keyup', '#item-table tr .unit', function(e) {
            //$('#item-table tr .unit').keyup(function() {
            calculate();
        });


        $(document).on('keypress', '#item-table tr .qty', function(e) {
            //$('#item-table tr .qty').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

        $(document).on('focusout', '#item-table tr .qty', function() {
            //$('#item-table tr .qty').focusout(function() {
            calculate();
        });

        $(document).on('keyup', '#item-table tr .qty', function() {
            //$('#item-table tr .qty').keyup(function() {
            calculate();
        });

        $('#discount').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

        $('#discount-table').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        $('#discount').keyup(function() {
            var discount = $("#discount").val();
            var subTotal = $("#subTotal").text();

            $("#discount-table").val(((discount / 100) * subTotal).toFixed(2));
            $("#grandTotal").html((subTotal - ((discount / 100) * subTotal)).toFixed(2));

            //calculate();
        });

        $('#discount-table').keyup(function() {
            var manualDiscount = $("#discount-table").val();
            var subTotal = $("#subTotal").text();
            var totalDiscount = (manualDiscount / subTotal) * 100;
            $('#discount').val((totalDiscount).toFixed(2));
            $("#grandTotal").html(subTotal - manualDiscount);
            console.log(manualDiscount);
            console.log(subTotal);
            console.log((manualDiscount / subTotal) * 100);

            //calculate();
        });

        function calculate() {
            var unit = [], qty = [], total = [], subTotal = 0, grandTotal = 0, i = 0;
            $('#item-table tr').each(function() {
                unit[i] = $(this).find(".unit").val();
                qty[i] = $(this).find(".qty").val();
                total[i] = unit[i] * qty[i];
                if (total[i] >= 0.000001) {
                    subTotal += total[i];
                    $(this).find(".total").html(total[i]);
                }
                else
                    $(this).find(".total").html('');
                i++;

                //alert($(this.value));
                //$(this.find(".unit").val())
            });
            var discount = $("#discount").val();

            grandTotal = subTotal - (discount / 100) * subTotal;
            $("#subTotal").html(subTotal.toFixed(2));
            $("#discount-table").val(((discount / 100) * subTotal).toFixed(2));
            $("#grandTotal").html(grandTotal.toFixed(2));
            console.log(unit);
            console.log(qty);
            console.log(total);
            console.log(subTotal);
        }

        $(document).on('click', '#item-table tr .add-icn', function() {
            //$('#item-table tr .add-icn').click(function() {
            var data = "<tr class='item-row'>";
            data += "<td><input class='form-control text-center' type='text' name='item[]'></td>";
            data += "<td><input class='form-control text-center' type='text' name='desc[]'></td>";
            data += "<td><input type='text' name='unit[]'  class='form-control unit text-center'></td>";
            data += "<td><input type='text' name='qty[]' class='form-control qty text-center'></td>";
            data += "<td><input type='text' name='tax1[]' class='form-control text-center'></td>";
            data += "<td><input type='text' name='tax2[]' class='form-control text-center'></td>";
            data += "<td><span class ='total text-center'></span><i class='glyphicon glyphicon-remove pull-right crs-icn'></i><i class ='glyphicon glyphicon-plus pull-right add-icn'></i></td>";
            data += "</tr>";
            $(this).closest('tr').after(data);
        });
        $(document).on('click', '#item-table tr .crs-icn', function() {
            //$('#item-table tr .crs-icn').click(function() {
            $(this).closest('tr').remove();
            calculate();
        });


        $("#create-invoice").submit(function(eventObj) {
            $('<input />').attr('type', 'hidden')
                    .attr('name', "grandTotal")
                    .attr('value', $("#grandTotal").text())
                    .appendTo('#create-invoice');
            return true;
        });

    });

    function checkValidation() {
        var item = $('.item');
        var unit = $('.unit');
        var qty = $('.qty');
        var valid = false;
        for(i=0;i<item.length;i++){
            if($.trim(item[i].value) != '' && $.trim(unit[i].value) != '' && $.trim(qty[i].value) != ''){
                valid = true;
            }
        }
        if($('#client').val() == ""){
            $('#client').focus();
            valid = false;
        }else if($('#doi').val() == ""){
            $('#doi').focus();
            valid = false;
        }else if($('#poNumber').val() == ""){
            $('#poNumber').focus();
            valid = false;
        }
        return valid;

    }

</script>

<script>
    $(document).ready(function () {
        
        $('#submit').click(function () {
//            alert('submitted');
            var subTotal = $("#subTotal").text();
            var manualDiscount = $("#discount-table").val();
            var budget = $("#budget").text();
            var totalInvoice = $("#totalInvoice").text();
            console.log(subTotal);
            console.log(manualDiscount);
            console.log(budget);
            console.log(totalInvoice);
            var invoiceProposed = parseFloat(subTotal) +  parseFloat(manualDiscount);
            var invoiceUsed = parseFloat(budget) -  parseFloat(totalInvoice);
            
            if (invoiceProposed > invoiceUsed) {
                    $('#amounterror').html("Amount "+invoiceProposed+" is larger than Due Amount "+invoiceUsed);
//                    $('#amount').val($('#amountmax').val());
                    return false;
                }
            return true;
        });
        
        $('#client').change(function () {

            if ($('#client').val() == ""){
                alert("Please Select Client first !");
            }else{
                var url = '{{url('project/list')}}/' + $('#client').val();
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        console.log(data);
                        var temp = jQuery.parseJSON(data);
                        console.log(temp);
                        $('#project').html(temp['projects']);
                        clientwiseProjectLoad();
                    }
                }, "json");
            }
        });
        
        $('#project').change(function(){
            clientwiseProjectLoad();
        });
        
        function clientwiseProjectLoad(){
//            alert('hellooooo');
//            alert($('#project').val());
            var project = $('#project').val();
            if(project != null){
                var projectId = $('#project').val();
                var url = '{{url('project/getInfo/')}}/'+projectId;
    //            alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
//                        console.log(data);
                        var temp = jQuery.parseJSON(data);
//                        console.log(temp);
    //                    var response = 'Project Budget: '+temp['budget']+' Invoice Paid: '+temp['totalInvoice'];
                        var str = temp['budget'];
                        $('#budget').html(str);
                        str = temp['totalInvoice'];
                        $('#totalInvoice').html(str);
                    }
                }, "json");
            }else{
                $('#budget').html(null);
                $('#totalInvoice').html(null);
            }
        }

        if ($('#client').val() != ""){
            var url = '{{url('project/list')}}/' + $('#client').val();
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
//                    alert(data);
                    $('#project').html(data);
                }
            }, "json");
        }

    });
</script>

@stop