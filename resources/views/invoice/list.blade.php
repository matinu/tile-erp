@extends('layout.admintamplate')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <div class="box-title">
                <h3>
                    {{$pageTitle}}
                </h3>
            </div>
            <div class="box-content nopadding">
                <?php if (Session::get('msg') != "") { ?>
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ Session::get('msg') }}
                    </div>
                <?php } ?>
                <table class="table table-hover table-nomargin dataTable table-bordered usertable">
                    <thead>
                        <tr>
                            <th class='with-checkbox'>SL</th>
                            <th>Client Name</th>
                            <th>Invoice Number</th>
                            <th>Issue Date</th>
                            <th>Invoice Total</th>
                            <th>Payment Status</th>
                            <th>Note</th>
                            <th style="min-width: 104px">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php //echo "<pre>";print_r($invoices);die; ?>
                        {{-- */ $sl = 1; /* --}}
                        @foreach($invoices as $invoice)

                        <?php
                        $condition = true;
                        $con1 = \App\Models\Invoice\Custom::getPaymentStatus($invoice->invoice_id);
                        $con2 = \App\Models\Invoice\Custom::getPaymentStatus($invoice->invoice_id);
                        $con3 = \App\Models\Invoice\Custom::getPaymentStatus($invoice->invoice_id);

                        if ($status != null) {
                            $condition = ($invoice->invoice_total <= $con3);
                        } else {
                            $condition = ($con1 == 'null' || $con1 == '') || $invoice->invoice_total > $con3;
                        }
                        ?>

                        @if($condition)<?php //die;  ?>
                        <tr>
                            <td class="with-checkbox">{{$sl}}</td>
                            <td>{{$invoice->company_name}}</td>
                            <td>{{$invoice->invoice_number}}</td>
                            <td>
                                {{$invoice->issue_date}}
                            </td>
                            <td>{{$invoice->invoice_total}}</td>
                            <td>
                                @if($con1 == 'null' || $con2 == '')
                                Draft
                                @elseif($invoice->invoice_total > $con3)
                                Partial
                                @else
                                Paid
                                @endif
                            </td>
                            <td>{{-- */ echo $invoice->note; /* --}}</td>
                            <td>
                                <a href="{{URL::to('/printInvoice/'.$invoice->invoice_number.'.pdf')}}" class="btn" target="_blank" rel="tooltip" title="View">
                                    <i class="fa fa-search"></i>
                                </a>
                                <a href="invoice-edit/{{$invoice->invoice_id}}" class="btn" rel="tooltip" title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="invoice-delete/{{$invoice->invoice_id}}" class="btn" rel="tooltip" title="Delete" onclick="if (confirm('Do you want to delete this client ?')) {
                                                        return;
                                                    } else {
                                                        return false;
                                                    }
                                                    ;">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                        {{-- */ $sl++; /* --}}
                        @endif
                        @endforeach

                    </tbody>
                </table>
                @include('CompanyDetails.pagination')
            </div>
        </div>
    </div>
</div>

@stop