
<form method="post" action="{{URL::to('/billing-pdf')}}">
    <input type="hidden" name="company_id" value="{{$company_id}}">
    <table class="table table-bordered table-hover table-striped">
        <tr>
            <th></th>
            <th>Id</th>
            <th>Title</th>
            <th>Hour</th>
            <th>Rate</th>
            <th>Total</th>
            <th>Status</th>
        </tr>
        <?php
//echo '<pre>';
//print_r($invoice_data);
        foreach ($invoice_data as $inv):
            ?>
            <tr>
                <td>
                    <input type="checkbox" name="<?php echo $inv['id'] ?>" value="<?php echo $inv['id'] ?>">
                </td>
                <td>
                    <?php echo $inv['id'] ?>
                </td>
                <td>
                    <?php echo $inv['invoice_title'] ?>
                </td>
                <td>
                    <?php echo $inv['invoice_hours'] ?>
                </td>
                <td>
                    <?php echo $inv['rate'] ?>
                </td>
                <td>
                    <?php echo $inv['rate'] * $inv['invoice_hours']; ?>
                </td>
                <td>
                    <?php echo $inv['invoice_status'] ?>
                </td>
            </tr>
            <?php
        endforeach;
        ?>
    </table>
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

    <div class="modal-footer">
        <button type="button" class="btn btn-default custom-close"  id="close_invoice_credit" onclick="$.fancybox.close();">Close</button>
        <input type="submit" value="Generate" class="btn btn-primary">
    </div>
</form>