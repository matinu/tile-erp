@extends('layout.admintamplate')
@section('content')




    <div class="row">
        <div class="col-sm-12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="fa fa-magic"></i>
                        Wizard with step overview
                    </h3>
                </div>
                <div class="box-content">
                    <form action="post.php" method="POST" class='form-horizontal form-validate' id="ss">
                        <div class="step" id="firstStep">
                            <ul class="wizard-steps steps-4">
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">
                                            1</span>
                                        <span class="circle">
                                            <span class="active"></span>
                                        </span>
                                        <span class="description">
                                            Basic information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            2</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Advanced information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            3</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Additional information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            4</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Check again
                                        </span>
                                    </div>
                                </li>
                            </ul>
                            <div class="step-forms">

                                <div class="form-group">
                                    <label for="firstname" class="control-label col-sm-2">First name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="firstname" id="firstname" class="form-control" data-rule-required="true">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="anotherelem" class="control-label col-sm-2">Last name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="anotherelem" id="anotherelem" class="form-control" data-rule-required="true">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="additionalfield" class="control-label col-sm-2">Additional information</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="additionalfield" id="additionalfield" class="form-control" data-rule-required="true" data-rule-minlength="10">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="secondStep">
                            <ul class="wizard-steps steps-4">
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            1</span>
                                        <span class="circle">

                                        </span>
                                        <span class="description">
                                            Basic information
                                        </span>
                                    </div>
                                </li>
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">
                                            2</span>
                                        <span class="circle">
                                            <span class="active"></span>
                                        </span>
                                        <span class="description">
                                            Advanced information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            3</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Additional information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            4</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Check again
                                        </span>
                                    </div>
                                </li>
                            </ul>
                            <div class="form-group">
                                <label for="text" class="control-label col-sm-2">Gender</label>
                                <div class="col-sm-10">
                                    <select name="gend" id="gend" data-rule-required="true">
                                        <option value="">-- Chose one --</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text" class="control-label col-sm-2">Accept policy</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="policy" value="agree" data-rule-required="true">I agree the policy.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="thirdStep">
                            <ul class="wizard-steps steps-4">
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            1</span>
                                        <span class="circle">

                                        </span>
                                        <span class="description">
                                            Basic information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            2</span>
                                        <span class="circle">

                                        </span>
                                        <span class="description">
                                            Advanced information
                                        </span>
                                    </div>
                                </li>
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">
                                            3</span>
                                        <span class="circle">
                                            <span class="active"></span>
                                        </span>
                                        <span class="description">
                                            Additional information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            4</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Check again
                                        </span>
                                    </div>
                                </li>
                            </ul>
                            <div class="form-group">
                                <label for="text" class="control-label col-sm-2">Additional information</label>
                                <div class="col-sm-10">
                                    <textarea name="textare" id="tt333" class="form-control" rows="7" placeholder="You can provide additional information in here..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="fourthstep">
                            <ul class="wizard-steps steps-4">
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            1</span>
                                        <span class="circle">

                                        </span>
                                        <span class="description">
                                            Basic information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            2</span>
                                        <span class="circle">

                                        </span>
                                        <span class="description">
                                            Advanced information
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">
                                            3</span>
                                        <span class="circle">
                                        </span>
                                        <span class="description">
                                            Additional information
                                        </span>
                                    </div>
                                </li>
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">
                                            4</span>
                                        <span class="circle">
                                            <span class="active"></span>
                                        </span>
                                        <span class="description">
                                            Check again
                                        </span>
                                    </div>
                                </li>
                            </ul>
                            <div class="form-group">
                                <label for="text" class="control-label col-sm-2">Check again</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="okokok" value="agree" data-rule-required="true">Everything is ok?</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="reset" class="btn" value="Back" id="back">
                            <input type="submit" class="btn btn-primary" value="Submit" id="next">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>






@stop