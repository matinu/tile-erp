<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />


    <title>{{$pageTitle}}</title>


    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/theme/css/bootstrap.min.css')}}">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/theme/css/plugins/jquery-ui/smoothness/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/theme/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/theme/css/style.css')}}">
    <!-- Color CSS -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/theme/css/themes.css')}}">

    <!-- Datepicker -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/theme/css/plugins/datepicker/datepicker.css')}}">

    <!-- jQuery -->
    <script src="{{URL::to('resources/assets/theme/js/jquery.min.js')}}"></script>

    <!-- Nice Scroll -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <!-- imagesLoaded -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/imagesLoaded/jquery.imagesloaded.min.js')}}"></script>
    <!-- jQuery UI -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/jquery-ui/jquery.ui.core.min.js')}}"></script>
    <script src="{{URL::to('resources/assets/theme/js/plugins/jquery-ui/jquery.ui.widget.min.js')}}"></script>
    <script src="{{URL::to('resources/assets/theme/js/plugins/jquery-ui/jquery.ui.mouse.min.js')}}"></script>
    <script src="{{URL::to('resources/assets/theme/js/plugins/jquery-ui/jquery.ui.resizable.min.js')}}"></script>
    <script src="{{URL::to('resources/assets/theme/js/plugins/jquery-ui/jquery.ui.sortable.min.js')}}"></script>
    <!-- slimScroll -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{URL::to('resources/assets/theme/js/bootstrap.min.js')}}"></script>
    <!-- Bootbox -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/bootbox/jquery.bootbox.js')}}"></script>
    <!-- Bootbox -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/form/jquery.form.min.js')}}"></script>
    <!-- Validation -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/validation/jquery.validate.min.js')}}"></script>
    <script src="{{URL::to('resources/assets/theme/js/plugins/validation/additional-methods.min.js')}}"></script>


    <!-- Datepicker -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

    <!-- Wizard -->
    <script src="{{URL::to('resources/assets/theme/js/plugins/wizard/jquery.form.wizard.min.js')}}"></script>
    <script src="{{URL::to('resources/assets/theme/js/plugins/mockjax/jquery.mockjax.js')}}"></script>

    <!-- Theme framework -->
    <script src="{{URL::to('resources/assets/theme/js/eakroko.min.js')}}"></script>
    <!-- Theme scripts -->
    <script src="{{URL::to('resources/assets/theme/js/application.min.js')}}"></script>
    <!-- Just for demonstration -->
    <script src="{{URL::to('resources/assets/theme/js/demonstration.min.js')}}"></script>


    <!--[if lte IE 9]>
            <script src="{{URL::to('js/plugins/placeholder/jquery.placeholder.min.js')}}"></script>
            <script>
                    $(document).ready(function() {
                            $('input, textarea').placeholder();
                    });
            </script>
    <![endif]-->



    <link rel="shortcut icon" href="{{URL::to('resources/assets/theme/img/favicon.ico')}}" />
    <!-- Apple devices Homescreen icon -->
    <link rel="apple-touch-icon-precomposed" href="{{URL::to('resources/assets/theme/img/apple-touch-icon-precomposed.png')}}" />

</head>
<style>
    .tableback th{
        background-color: #ccc;
    }
</style>
<body>
    <div class="row">
        <div class="col-md-6">
            <p>Workspace Infotech</p>
            <p>kha-51,khilkhet(west),Nikunjo-2</p>
            <p>Road-14, House-16(1st floor), Nikunjo-2</p>
            <p>Dhaka 1229</p>
            <p>Bangladesh</p>
        </div>
        <div class="col-md-6"><img style="height: 200px;" src="{{URL::to('resources/assets/theme/img/forPdf.jpg')}}"></div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Coast Internet Publishing</p>
            <p>Mark Pratley</p>
            <p>Canada</p>
            <p>6244 Greenside Drive West Surrey, BC V3S 5M8</p>
            <p>MB</p>
            <p>Canada</p>
        </div>
        <div class="col-md-6">
            <p><span>Invoice #</span><span style="float: right;padding-right: 10px;">0000003</span></p>
            <p><span>Invoice Date</span><span style="float: right;padding-right: 10px;">December 4,2012</span></p>
            <p style="background-color: #ccc;font-weight: bold;"><span>Amount Due</span><span style="float: right;padding-right: 10px;">$0.00 USD</span></p>
        </div>
    </div>
    <div class="row">
        <table class="col-md-12 tableback">
            <th>Item</th>
            <th>Description</th>
            <th>Unit Cost</th>
            <th>Quantity</th>
            <th>Line Total</th>
            <tr>
                <td>planet lazer admin</td>
                <td>admin part design done uploaded into client server</td>
                <td>250.00</td>
                <td>1</td>
                <td>250.00</td>
            </tr>
        </table>
    </div>
    <div class="col-md-6" style="float:right">
        <p style="font-weight: bold;"><span>Total</span><span style="float: right;">250.00</span></p>
        <p ><span>Amount Paid</span><span style="float: right;">-250.00</span></p>
        <p style="background-color: #ccc;font-weight: bold;"><span>Amount Due</span><span style="float: right;">$0.00 USD</span></p>
    </div>
</body>