@extends('layout.admintamplate')
@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <div class="box-title">
                <h3>
                    {{$pageTitle}}
                </h3>
            </div>
            <div class="box-content">
                <?php if (Session::get('msg') != "") { ?>
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ Session::get('msg') }}
                    </div>
                <?php } ?>
                <form action="{{URL::to('invoice-update')}}" method="POST" class='form-horizontal form-validate' id="create-invoice" >
                    <div class="step" id="firstStep">
                        <input type="hidden" name="invoice_id" value="{{$invoice->invoice_id}}">
                        <div class="step-forms">

                            <div class="form-group">
                                <label for="client" class="control-label col-sm-2">Client</label>
                                <div class="col-sm-10">
                                    <select name="client" id="client" required="requried" class='form-control' disabled>
                                        @foreach($allClients as $eachClient)
                                        @if($eachClient->client_id==$invoice->client)
                                        <option value="{{$eachClient->company_id}}" selected="selected">{{$eachClient->company_name}}</option>
                                        @else
                                        <option value="{{$eachClient->company_id}}">{{$eachClient->company_name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!--                            <div class="form-group">
                                                            <label for="projectName" class="control-label col-sm-2">Project</label>
                                                            <div class="col-sm-10">
                                                                <select name="project_name" id="projectName" class='form-control' disabled>
                                                                    <option value="{{$invoice->project_name}}">{{$invoice->project_name}}</option>
                                                                </select>
                                                            </div>
                                                        </div>-->

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Invoice Number</label>
                                <div class="col-sm-10">
                                    {{$invoice->invoice_number}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="doi" class="control-label col-sm-2">Date of Issue</label>
                                <div class="col-sm-10">
                                    <input type="text" required="requried"  name="doi" id="doi" class="form-control" value="{{$invoice->issue_date}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="poNumber" class="control-label col-sm-2">P.O Number</label>
                                <div class="col-sm-10">
                                    <input type="number" required="requried" name="poNumber" id="poNumber" class="form-control" data-rule-required="true" value="{{$invoice->po_number}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="discount" class="control-label col-sm-2">Discount</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text" name="discount" id="discount" class="form-control" data-rule-required="true" value="{{$invoice->discount}}">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="terms" class="control-label col-sm-2">Terms</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <textarea name="terms" id="terms" rows="5" cols="80" class="form-control">{{$invoice->terms}}</textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="notes" class="control-label col-sm-2">Notes</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <textarea name="notes" id="notes" rows="5" cols="80" class="form-control">{{$invoice->note}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-table"></i>
                                        Items
                                    </h3>
                                </div>
                                <div class="box-content nopadding">
                                    <span id="errmsg"></span>
                                    <table class="table table-nomargin table-bordered table-condensed" id="item-table">
                                        <thead>
                                            <tr>
                                                <th>Item(s)</th>
                                                <th>Description</th>
                                                <th>Unit / Per Hour Cost</th>
                                                <th>Qty / Hour(s)</th>
                                                <!--<th>Tax</th>-->
                                                <!--<th>Tax</th>-->

                                                <th width="15%">Line Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($items as $item)

                                            <tr class="item-row">
                                                <td><input class="form-control text-center" type="text" name="old_item[]" value="{{$item->item}}"></td>
                                                <td><input class="form-control text-center" type="text" name="old_desc[]" value="{{$item->description}}"></td>
                                                <td><input type="text" name="old_unit[]"  class="form-control unit text-center" value="{{$item->unit_cost}}"></td>
                                                <td><input type="text" name="old_qty[]" class="form-control qty text-center" value="{{$item->quantity}}"></td>
                                                <!--<td><input type="text" name="old_tax1[]" class="form-control text-center" ></td>-->
                                                <!--<td><input type="text" name="old_tax2[]" class="form-control text-center" ></td>-->
                                                <td><input type="hidden" name="old_id[]" value="{{$item->invoice_details_id}}"><span class ="total text-center">{{$item->total}}</span><i class="glyphicon glyphicon-remove pull-right crs-icn" old="{{$item->invoice_details_id}}"></i><i class ="glyphicon glyphicon-plus pull-right add-icn"></i></td>
                                            </tr>

                                            @endforeach

                                            @for($i=1; $i<=2; $i++)
                                            <tr class="item-row">
                                                <td><input class="form-control text-center" type="text" name="item[]"></td>
                                                <td><input class="form-control text-center" type="text" name="desc[]"></td>
                                                <td><input type="text" name="unit[]"  class="form-control unit text-center"></td>
                                                <td><input type="text" name="qty[]" class="form-control qty text-center"></td>
                                                <!--<td><input type="text" name="tax1[]" class="form-control text-center"></td>-->
                                                <!--<td><input type="text" name="tax2[]" class="form-control text-center"></td>-->
                                                <td><span class ="total text-center"></span><i class="glyphicon glyphicon-remove pull-right crs-icn"></i><i class ="glyphicon glyphicon-plus pull-right add-icn"></i></td>
                                            </tr>
                                            @endfor
                                            <tr>
                                                <td colspan="4" class="text-right">Sub-Total</td>
                                                <td class="text-center"><div id="subTotal"></div></td>

                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-right">Discount</td>
                                                <td class="text-center"><input id="discount-table" class="text-center"></td>

                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-right">Total</td>
                                                <td id="grandTotal" class="text-center"></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="reset" class="btn" value="Back" id="back">
                        <input type="submit" class="btn btn-primary" value="Update" >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function () {

        calculate();

        $(document).on('keypress', '#item-table tr .unit', function (e) {
            //$('#item-table tr .unit').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        $(document).on('focusout', '#item-table tr .unit', function (e) {
            //$('#item-table tr .unit').focusout(function() {
            calculate();
        });
        $(document).on('keyup', '#item-table tr .unit', function (e) {
            //$('#item-table tr .unit').keyup(function() {
            calculate();
        });


        $(document).on('keypress', '#item-table tr .qty', function (e) {
            //$('#item-table tr .qty').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

        $(document).on('focusout', '#item-table tr .qty', function () {
            //$('#item-table tr .qty').focusout(function() {
            calculate();
        });

        $(document).on('keyup', '#item-table tr .qty', function () {
            //$('#item-table tr .qty').keyup(function() {
            calculate();
        });

        $('#discount').keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
//        $('#discount').keyup(function() {
//            calculate();
//        });

        $('#discount-table').keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
//            else {
//                var manualDiscount = $("#discount-table").val();
//                var subTotal = $("#subTotal").text();
//                $('#discount').val((manualDiscount / subTotal) * 100);
//                console.log(manualDiscount);
//                console.log(subTotal);
//                console.log((manualDiscount / subTotal) * 100);
//            }
            calculate();
        });
        $('#discount').keyup(function () {
            var discount = $("#discount").val();
            var subTotal = $("#subTotal").text();
            $("#discount-table").val((discount / 100) * subTotal);
            $("#grandTotal").html(subTotal - ((discount / 100) * subTotal));

            //calculate();
        });

        $('#discount-table').keyup(function () {
            var manualDiscount = $("#discount-table").val();
            var subTotal = $("#subTotal").text();
            $('#discount').val((manualDiscount / subTotal) * 100);
            $("#grandTotal").html(subTotal - manualDiscount);
            console.log(manualDiscount);
            console.log(subTotal);
            console.log((manualDiscount / subTotal) * 100);

            //calculate();
        });

        function calculate() {
            var unit = [], qty = [], total = [], subTotal = 0, grandTotal = 0, i = 0;
            $('#item-table tr').each(function () {
                unit[i] = $(this).find(".unit").val();
                qty[i] = $(this).find(".qty").val();
                total[i] = unit[i] * qty[i];
                if (total[i] >= 0.000001) {
                    subTotal += total[i];
                    $(this).find(".total").html(total[i]);
                }
                else
                    $(this).find(".total").html('');
                i++;

                //alert($(this.value));
                //$(this.find(".unit").val())
            });
            var discount = $("#discount").val();

//            if (discount < 1)
//                discount = 0;
            //grandTotal = subTotal - (discount / 100) * subTotal;
            grandTotal = subTotal - (discount / 100) * subTotal;
            $("#subTotal").html(subTotal);
            $("#discount-table").val((discount / 100) * subTotal);
            $("#grandTotal").html(grandTotal);
            console.log(unit);
            console.log(qty);
            console.log(total);
            console.log(subTotal);
        }

        $(document).on('click', '#item-table tr .add-icn', function () {
            //$('#item-table tr .add-icn').click(function() {
            var data = "<tr class='item-row'>";
            data += "<td><input class='form-control text-center' type='text' name='item[]'></td>";
            data += "<td><input class='form-control text-center' type='text' name='desc[]'></td>";
            data += "<td><input type='text' name='unit[]'  class='form-control unit text-center'></td>";
            data += "<td><input type='text' name='qty[]' class='form-control qty text-center'></td>";
            data += "<td><input type='text' name='tax1[]' class='form-control text-center'></td>";
            data += "<td><input type='text' name='tax2[]' class='form-control text-center'></td>";
            data += "<td><span class ='total text-center'></span><i class='glyphicon glyphicon-remove pull-right crs-icn'></i><i class ='glyphicon glyphicon-plus pull-right add-icn'></i></td>";
            data += "</tr>";
            $(this).closest('tr').after(data);
        });
        $(document).on('click', '#item-table tr .crs-icn', function () {
            if ($(this).attr('old')) {
                alert($("input[name='_token']").val());
                $.ajax({
                    type: "POST",
                    url: "/invoice/invoice-item-delete",
                    data: "id=" + $(this).attr('old') + "&_token=" + $("input[name='_token']").val(),
                    success: function (data) {
                        alert(data);
                    }
                });
            }
            $(this).closest('tr').remove();
            calculate();
        });


        $("#create-invoice").submit(function (eventObj) {
            $('<input />').attr('type', 'hidden')
                    .attr('name', "grandTotal")
                    .attr('value', $("#grandTotal").text())
                    .appendTo('#create-invoice');
            return true;
        });

    });

</script>


@stop