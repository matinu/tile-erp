@extends('layout.admintamplate')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Know your Customer(KYC)</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if (Session::has('success_add_msg'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::pull('success_add_msg') }}
                </div>
                @endif
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Client Name</th>
                            <th>Country</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list as $key=>$doc)
                        <tr>
                            <td>{{date("jS F, Y", strtotime($doc->created_at))}}</td>
                            <td>{{$doc->name}}</td>
                            <td>{{$doc->country}}</td>
                            <td>{{$doc->description}}</td>
                            <td>
                                <?php if(file_exists('public/images/kyc/'.$doc->filename)){ ?>
                                <a href="{{URL::to('public/images/kyc').'/'.$doc->filename}}" download="{{$doc->filename}}" class="btn btn-primary">Download</a>&nbsp
                                <a data-toggle="lightbox" href="{{URL::to('public/images/kyc').'/'.$doc->filename}}" class="btn btn-primary">View</a>
                                <?php } ?>
                                <a onClick="javascript: return confirm('Are you ready for delete?');" href="{{URL::to('delete-kyc').'/'.$doc->id}}" class="btn btn-danger">Delete</a>&nbsp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @include('admin.pagination')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script>
                                    $(function() {
                                        $("#example1").DataTable({
                                            "paging": false,
                                            "info": false,
                                            "searching": false,
                                        });


                                        $('#example2').DataTable({
                                            "paging": true,
                                            "lengthChange": false,
                                            "searching": false,
                                            "ordering": true,
                                            "info": true,
                                            "autoWidth": false
                                        });
                                    });
</script>
@stop