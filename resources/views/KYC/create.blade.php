@extends('layout.admintamplate')

@section('content')
<div class="col-md-12">
    <div class="col-md-12">
        <div class="modal-header">
            <h4 class="modal-title">Add Know Your Customer(KYC)</h4>

        </div>
        <div class="modal-body" style="height: auto;">

            <form id="bankaccountForm" method="post"
                  action="{{URL::to('create-kyc')}}" enctype="multipart/form-data">
                <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                <div class="form-group">
                    <label for="recipient-name" class="control-label">Client Name:</label>
                    <input type="text" name="name" data-toggle="tooltip" class="form-control"
                           id="name" title="Enter Client Name">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Client Country:</label>
                    <select name="country" class="form-control" data-toggle="tooltip" id="country" title="Select Country">
                        <option value="">*Client Country</option>
                        @foreach($countries as $country)
                        <option value="{{$country->name}}">{{$country->name}}</option>
                        @endforeach
                    </select>

                </div>
                <div  class="form-group">
                    <label for="description" class="control-label">Description:</label>
                    <textarea name="description" class="form-control" id="description" data-toggle="tooltip" title="Add Description"></textarea>

                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Files:</label>
                    <input type="file" class="form-control" name="kyc_file" id="kyc_file">
                </div>


                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" id="kyc" value="Save changes"/>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(
            function() {
                $('#kyc').click(
                        function() {
                            var name = $('#name');
                            var country = $('#country');
                            var description = $('#description');
                            if (name.val() == "") {
                                name.focus().mouseover();
                                return false;
                            }
                            if (country.val() == "") {
                                country.focus().mouseover();
                                return false;
                            }
                            if (description.val() == "") {
                                description.focus().mouseover();
                                return false;
                            }
                        }
                );


            });
</script>
@stop
