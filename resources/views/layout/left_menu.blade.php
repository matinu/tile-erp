
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{URL::to('resources/assets/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Session::get('user_name')}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview <?php if ($_SERVER['QUERY_STRING'] == "dashboard") echo "active"; ?>">
                <a href="{{URL::to('dashboard')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span> 
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-odnoklassniki"></i>
                    <span>User management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-user")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-user')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>List Users</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "create-user") echo "active"; ?>">
                        <a href="{{URL::to('create-user')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Add User</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-plus"></i>
                    <span>Role</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-role")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-role')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>Role List</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "create-role") echo "active"; ?>">
                        <a href="{{URL::to('create-role')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Add Role</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "view-user-role") echo "active"; ?>">
                        <a href="{{URL::to('view-user-role')}}">
                            <i class="fa fa-fw fa-list"></i><span>View User Role</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "set-role") echo "active"; ?>">
                        <a href="{{URL::to('set-role')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Set Role</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-expeditedssl"></i>
                    <span>Permission</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-permission")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-permission')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>Permission List</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "view-role-permission") echo "active"; ?>">
                        <a href="{{URL::to('view-role-permission')}}">
                            <i class="fa fa-fw fa-list"></i> <span>Role Permission List</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "set-permission") echo "active"; ?>">
                        <a href="{{URL::to('set-permission')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Set Role Permission</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-building-o"></i>
                    <span>Company</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-company")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-company')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>List Company</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "create-company") echo "active"; ?>">
                        <a href="{{URL::to('create-company')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Create Company</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-dollar"></i>
                    <span>Billing Configuration</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-billing")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-billing')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>List Billing</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "create-billing") echo "active"; ?>">
                        <a href="{{URL::to('create-billing')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Create Billing</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-fw fa-openid"></i>
                    <span>Bank Account Opening</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-bank-account")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-bank-account')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>List Bank Account</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "bank-account-open") echo "active"; ?>">
                        <a href="{{URL::to('bank-account-open')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Create Bank Account</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Invoice</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "invoice-list")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('invoice-list')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>Invoice List</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "invoice-add") echo "active"; ?>">
                        <a href="{{URL::to('invoice-add')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Create Invoice</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "payment-list") echo "active"; ?>">
                        <a href="{{URL::to('payment-list')}}">
                            <i class="fa fa-fw fa-list"></i><span><i class="entypo-minus"></i>Payment List</span>
                        </a>
                    </li>


                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-heartbeat"></i>
                    <span>Know your Customer(KYC)</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php
                    $parts = explode('/', $_SERVER['QUERY_STRING']);
                    foreach ($parts as $part):
                        if ($part == "list-kyc")
                            echo "active";
                    endforeach;
                    ?>">
                        <a href="{{URL::to('list-kyc')}}/1">
                            <i class="fa fa-fw fa-list"></i><span>List KYC</span>
                        </a>
                    </li>
                    <li class="<?php if ($_SERVER['QUERY_STRING'] == "create-kyc") echo "active"; ?>">
                        <a href="{{URL::to('create-kyc')}}">
                            <i class="fa fa-fw fa-plus"></i><span>Create KYC</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
