
<!--new tamplate-->

<script src="{{URL::to('resources/assets/js/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{URL::to('resources/assets/js/bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{URL::to('resources/assets/js/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{URL::to('resources/assets/js/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::to('resources/assets/js/app.min.js')}}"></script>
<!-- Date Picker -->
<script src="{{URL::to('resources/assets/js/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{URL::to('resources/assets/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{URL::to('resources/assets/js/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('resources/assets/js/datatables/dataTables.bootstrap.min.js')}}"></script>
<!--Add fancyBox main JS and CSS files--> 
<script type="text/javascript" src="{{URL::to('resources/assets/js/fancybox/source/jquery.fancybox.js?v=2.1.5')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::to('resources/assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5')}}" media="screen"/>

<!--Add Button helper (this is optional)--> 
<link rel="stylesheet" type="text/css" href="{{URL::to('resources/assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}}"/>
<script type="text/javascript" src="{{URL::to('resources/assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}}"></script>

<!--Add Thumbnail helper (this is optional)--> 
<link rel="stylesheet" type="text/css" href="{{URL::to('resources/assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')}}"/>
<script type="text/javascript" src="{{URL::to('resources/assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')}}"></script>

<!--Add Media helper (this is optional)--> 
<script type="text/javascript" src="{{URL::to('resources/assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6')}}"></script>

<script type="text/javascript">
$(document).ready(function () {
    /*
     *  Simple image gallery. Uses default settings
     */

    $('.fancybox').fancybox(
            {
                padding: 5,
                afterLoad: function ()
                {
                    tinymce.init({
                        selector: "#correspondence_content"
                    });
                }
            }
    );
    $('.fancybox').fancybox(
            {
                padding: 5,
                afterLoad: function ()
                {
                    tinymce.init({
                        selector: "#incorporation_description"
                    });
                }
            }
    );
    $('.fancybox').fancybox(
            {
                padding: 5,
                afterLoad: function ()
                {
                    tinymce.init({
                        selector: "#compliance_content"
                    });
                }
            }
    );
  
});
</script>
