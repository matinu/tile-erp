<!-- Latest compiled and minified JavaScript -->

<link rel="stylesheet" href="{{URL::to('resources/assets/css/bootstrap.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{URL::to('resources/assets/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{URL::to('resources/assets/css/skins/_all-skins.min.css')}}">
<!-- DataTables -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/js/datatables/dataTables.bootstrap.css')}}">
<style>
    .fancybox-outer, .fancybox-inner {
        height: 555px !important;
        overflow-x: hidden !important;
        overflow-y: scroll !important;
        position: relative !important;
    }
</style>