 <!doctype html>
<html lang="en" >

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- Apple devices fullscreen -->
        <meta name="til-erp-{{$pageTitle}}" content="yes" />
        <!-- Apple devices fullscreen -->
        <meta names="til-erp-{{$pageTitle}}" content="black-translucent" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>TIL-ERP-{{$pageTitle}}</title>
        @include('layout.style')
        @include('layout.script')


        <!--[if lte IE 9]>
                <script src="{{URL::to('js/plugins/placeholder/jquery.placeholder.min.js')}}"></script>
                <script>
                        $(document).ready(function() {
                                $('input, textarea').placeholder();
                        });
                </script>
            <![endif]-->
    <input type="hidden" id="base_url" value="{{URL::to('/')}}" />
</head>
<body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        @include('layout.header')

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        @include('layout.left_menu')

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 615px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <h1>
                    <b>TIL</b>
                    <small style="color: #3C8DBC;"> <b>Enterprise Resource Planning</b></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> dashboard</a></li>
                    @foreach($breadcamps as $key => $link)

                    <li>
                        <a href="{{ url($link) }}">{{$key}}</a>
                    </li>

                    @endforeach
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section> 
            <!--/.content--> 
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; <?php date('Y'); ?> <a href="#">Til ERP</a>.</strong> All rights reserved.
        </footer>

        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script>
        $('document').ready(function () {
            $(".breadcrumb li:last-child").addClass("active");
            $('li.active').parent().parent().addClass('active');
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".datepicker").datepicker();
        });
    </script>
</body>
</html>