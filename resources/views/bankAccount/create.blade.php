@extends('layout.admintamplate')

@section('content')
<div class="col-md-12">
    <div class="col-md-12">
        <div class="modal-header">
            <h4 class="modal-title">Add Bank Account Opening Templates</h4>

        </div>
        <div class="modal-body">

            <form id="bankaccountForm" method="post"
                  action="{{URL::to('bank-account-open')}}" enctype="multipart/form-data">
                <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                <div class="form-group">
                    <label for="recipient-name" class="control-label">Bank Name:</label>
                    <input type="text" name="bank_name" data-toggle="tooltip" class="form-control"
                           id="name" title="Enter Bank Name">
                </div>

                <div class="form-group">
                    <label for="message-text" class="control-label">Files:</label>
                    <input type="file" data-toggle="tooltip" class="form-control" title="Enter Bank Account Form" name="attached" id="attached">
                </div>


                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" id="add_bank_account" value="Save changes"/>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(
            function () {
                $('#add_bank_account').click(
                        function () {
                            var title = $('#name');
                            var file = $('#attached');
                            if (title.val() == "") {
                                title.focus().mouseover();
                                return false;
                            } else if (file.val() == "" || !ValidateSingleInput($('#attached')[0])) {
                                file.focus().mouseover();
                                return false;
                            }
                        }
                );

                var _validFileExtensions = [".pdf"];
                function ValidateSingleInput(oInput) {
                    if (oInput.type == "file") {
                        var sFileName = oInput.value;
                        if (sFileName.length > 0) {
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensions.length; j++) {
                                var sCurExtension = _validFileExtensions[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }

                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions is: " + _validFileExtensions.join(", "));
                                oInput.value = "";
                                return false;
                            }
                        }
                    }
                    return true;
                }
            });
</script>
@stop