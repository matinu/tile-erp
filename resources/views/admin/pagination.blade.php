<?php $perpage = 20; ?>
<nav style="text-align: right;">
    <ul class="pagination">
        <li class="<?php if ($page == 1) echo 'disabled'; ?>"><a aria-label="Previous" href="<?php
            if ($page == 1)
                echo '#';
            else {
                echo url() . "/" . Request::segment(1) . "/";
                // echo "?page=";
                echo $page - 1;
            }
            ?>"><span aria-hidden="true">«</span></a></li>
            <?php
            if (ceil($count / $perpage) <= 7) {
                for ($i = 1; $i < ceil($count / $perpage) + 1; $i++) {
                    if ($page == $i) {
                        ?> 
                    <li class="active"><a href="#"><?php echo $i; ?> <span class="sr-only">(current)</span></a></li>
                    <?php
                } else {
                    ?>
                    <li><a href="<?php
                        echo url() . "/" . Request::segment(1) . "/";
                        echo $i;
                        ?>"><?php echo $i; ?></a></li>
                        <?php
                    }
                }
            } else {
                if ($page == 1) {
                    ?>
                <li class="active"><a href="#">1<span class="sr-only">(current)</span></a></li>
            <?php } else { ?>
                <li><a href="<?php echo url() . "/" . Request::segment(1) . "/1"; ?>">1</a></li>
                <?php
            }
            if ($page == 2) {
                ?>
                <li class="active"><a href="#">2<span class="sr-only">(current)</span></a></li>
            <?php } else { ?>
                <li><a href="<?php echo url() . "/" . Request::segment(1) . "/2"; ?>">2</a></li>
            <?php } 
            $dotprev = $page - 1;
            if ($dotprev >= 4) {
                ?>
                <li><a href="javascript:void(0)">...</a></li>
                <?php
            }
            for ($i = $page - 1; $i < $page + 2; $i++) {
                if ($i <= 2 || $i >= ceil($count / $perpage) - 1) {
                    continue;
                }
                if ($page == $i) {
                    ?> 
                    <li class="active"><a href="#"><?php echo $i; ?> <span class="sr-only">(current)</span></a></li>
                    <?php
                } else {
                    ?>
                    <li><a href="<?php echo url() . "/" . Request::segment(1) . "/" . $i; ?>"><?php echo $i; ?></a></li>
                    <?php
                }
            }
            $dotlast = $page + 1;
            if ($dotlast <= ceil($count / $perpage)-3) {
                ?>
                <li><a href="javascript:void(0)">...</a></li>
                <?php
            }
            if ($page == ceil($count / $perpage) - 1) {
                ?>
                <li class="active"><a href="#"><?php echo ceil($count / $perpage) - 1 ?><span class="sr-only">(current)</span></a></li>
                <?php } else { ?>
                <li><a href="<?php
                       echo url() . "/" . Request::segment(1) . "/";
                       echo ceil($count / $perpage) - 1;
                       ?>"><?php echo ceil($count / $perpage) - 1 ?></a></li>
                    <?php
                }
                if ($page == ceil($count / $perpage)) {
                    ?>
                <li class="active"><a href="#"><?php echo ceil($count / $perpage) ?><span class="sr-only">(current)</span></a></li>
                <?php } else { ?>
                <li><a href="<?php
                    echo url() . "/" . Request::segment(1) . "/";
                    echo ceil($count / $perpage);
                    ?>"><?php echo ceil($count / $perpage) ?></a></li>
                <?php } ?> 
            <?php }
            ?>
        <li class="<?php if ($page == ceil($count / $perpage)) echo 'disabled'; ?>"><a aria-label="Next" href="<?php
            if ($page == ceil($count / $perpage))
                echo '#';
            else {
                echo url() . "/" . Request::segment(1) . "/";
                echo $page + 1;
            }
            ?>"><span aria-hidden="true">»</span></a></li>
<!--<li><a aria-label="Next" href="#"><span aria-hidden="true">»</span></a></li>-->
    </ul>
</nav>