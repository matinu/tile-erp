@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if ($errors->first() != "") { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ $errors->first() }}
            </div>
        <?php } ?>
        <form action="{{URL::to('/create-user')}}" method="post" role="form" id="form_adduser">
            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" autocomplete="off" data-mask="email" value="{{old('name')}}" required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" data-mask="email"  value="{{old('email')}}" required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group  col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-key"></i>
                                    </div>-->

                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off"  value="{{old('password')}}" required="required"/>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group  col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-key"></i>
                                    </div>-->

                    <input type="password" class="form-control" name="confirmation_password" id="confirmation_password" placeholder="Confirm Password" autocomplete="off"  value="{{old('confirmation_password')}}" required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <input type="text" class="form-control" name="cnumber" id="cnumber" placeholder="Contact Number" autocomplete="off" data-mask="email"  value="{{old('cnumber')}}" required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group col-md-8">
                    <!--                <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>-->

                    <input type="text" class="form-control" name="dob" id="dob" placeholder="Select DOB" autocomplete="off" data-mask="email"  value="{{old('dob')}}" required="required"/>
                </div>
            </div>
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <!--<i class="entypo-login"></i>-->
                    Add
                </button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#dob").datepicker({endDate: new Date()});
    });
</script>
@stop
