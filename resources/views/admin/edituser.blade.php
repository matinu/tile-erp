@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
<?php if ($errors->first() != "") { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>

            {{ $errors->first() }}
        </div>
<?php } ?>
    <form action="{{URL::to('/edit-user')}}" method="post" role="form" id="form_adduser">
        <input type="hidden" name="id" id="id" value="<?php if (Input::old('id'))echo Input::old('id');else echo $userdata[0]['users_id']; ?>">
        <div class="form-group">
            <div class="input-group col-md-8">
                <input type="text" class="form-control" name="name" id="name" placeholder="Name" autocomplete="off" data-mask="email" value="<?php if (Input::old('name'))echo Input::old('name');else echo $userdata[0]['name']; ?>"/>
            </div>

        </div>

        <div class="form-group">
            <div class="input-group col-md-8">
                <!--                <div class="input-group-addon">
                                    <i class="entypo-user"></i>
                                </div>-->

                <input type="text" class="form-control" name="cnumber" id="cnumber" placeholder="Contact Number" autocomplete="off" data-mask="email"  value="<?php if (Input::old('cnumber'))echo Input::old('cnumber');else echo $userdata[0]['cnumber']; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group col-md-8">
                <!--                <div class="input-group-addon">
                                    <i class="entypo-user"></i>
                                </div>-->

                <input type="text" class="form-control datepicker" name="dob" id="dob" placeholder="Select DOB" autocomplete="off" data-mask="email" value="<?php if (Input::old('dob'))echo Input::old('dob');else echo date("m/d/Y", strtotime($userdata[0]['dob'])); ?>" />
            </div>
        </div>
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                <!--<i class="entypo-login"></i>-->
                Update
            </button>
        </div>
    </form>
</div>
@stop