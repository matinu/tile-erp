@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>

    <div class="row">
        <div class="col-md-6">
            <!-- AREA CHART -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Organizations-All</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart" style="height:340px;">
                        <span style="float: right;">
                            <a href="{{URL::to('list-company')}}/1">
                                <span>List Company</span> <i class="fa fa-fw fa-list"></i>
                            </a>
                        </span>
                        <span style="float: left;">
                            <a href="{{URL::to('create-company')}}">
                                <i class="fa fa-fw fa-plus"></i><span>Create Company</span>
                            </a>
                        </span>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Company Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($companies as $company)
                                <tr>
                                    <td>{{ $company->company_name }}</td>
                                    <td>{{ $company->company_type }}</td>
                                    <td>{{ $company->company_status }}</td>
                                    <td><a href="{{URL::to('details-company')."/".$company->company_id}}" class="btn btn-primary">Details</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <!-- DONUT CHART -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Invoices-All</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body" >
                    <div class="chart" style="height:340px">
                        <span style="float: left;">
                            <a href="{{URL::to('invoice-list')}}/1">
                                <i class="fa fa-fw fa-list"></i><span>Invoice List</span>
                            </a>
                        </span>
                        <span style="margin: 19%;">
                            <a href="{{URL::to('invoice-add')}}">
                                <i class="fa fa-fw fa-plus"></i><span>Create Invoice</span>
                            </a>
                        </span>
                        <span style="float: right;">
                            <a href="{{URL::to('payment-list')}}">
                                <span><i class="entypo-minus"></i>Payment List</span><i class="fa fa-fw fa-list"></i>
                            </a>
                        </span>
                        <!--<canvas id="pieChart" style="height:340px"></canvas>-->
                        <table class="table table-hover table-nomargin dataTable table-bordered usertable">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Invoice Number</th>
                                    <th>Invoice Total</th>
                                    <th>Payment Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //echo "<pre>";print_r($invoices);die; 
                                $status = NULL
                                ?>
                                {{-- */ $sl = 1; /* --}}
                                @foreach($invoices as $invoice)

                                <?php
                                $condition = true;
                                $con1 = \App\Models\Invoice\Custom::getPaymentStatus($invoice->invoice_id);
                                $con2 = \App\Models\Invoice\Custom::getPaymentStatus($invoice->invoice_id);
                                $con3 = \App\Models\Invoice\Custom::getPaymentStatus($invoice->invoice_id);

                                if ($status != null) {
                                    $condition = ($invoice->invoice_total <= $con3);
                                } else {
                                    $condition = ($con1 == 'null' || $con1 == '') || $invoice->invoice_total > $con3;
                                }
                                ?>

                                @if($condition)<?php //die;         ?>
                                <tr>
                                    <td>{{$invoice->company_name}}</td>
                                    <td>{{$invoice->invoice_number}}</td>

                                    <td>{{$invoice->invoice_total}}</td>
                                    <td>
                                        @if($con1 == 'null' || $con2 == '')
                                        Draft
                                        @elseif($invoice->invoice_total > $con3)
                                        Partial
                                        @else
                                        Paid
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{URL::to('/printInvoice/'.$invoice->invoice_number.'.pdf')}}" class="btn" target="_blank" rel="tooltip" title="View">
                                            <i class="fa fa-search"></i>
                                        </a>

                                    </td>
                                </tr>
                                {{-- */ $sl++; /* --}}
                                @endif
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col (LEFT) -->
        <div class="col-md-6">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">My Notes</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="lineChart" style="height:340px"></canvas>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Billings All</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart" style="height: 340px;">
                        <span style="float: right;">
                            <a href="{{URL::to('list-billing')}}/1">
                                <span>List Billing</span><i class="fa fa-fw fa-list"></i>
                            </a>
                        </span>
                        <span style="float: left">
                            <a href="{{URL::to('create-billing')}}">
                                <i class="fa fa-fw fa-plus"></i><span>Create Billing</span>
                            </a>
                        </span>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Billing Period</th>
                                    <th>Hourly Rate</th>
                                    <th>Cycle Start Date</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($billings as $bill)
                                <tr>
                                    <td>{{ $bill->company->company_name}}</td>
                                    <td>{{ $bill->billing_period }}</td>
                                    <td>{{ $bill->hourly_rate }}</td>
                                    <td>{{ date("jS F, Y", strtotime($bill->cycle_start_date)) }}</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col (RIGHT) -->
    </div><!-- /.row -->

</div>
@stop