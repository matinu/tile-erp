@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">
        <?php if (Session::get('msg') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('msg') }}
            </div>
        <?php } ?>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th>Name</th>
                    <td>{{$user[0]->name}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{$user[0]->email}}</td>

                </tr>
                <tr>
                    <th>Access Power</th>
                    <td>
                        <span style="color:green">
                            @foreach($roles as $role)
                            @if($role->id==$user[0]->id)
                            @if (count($role['roles']) != 0)
                            {{$role['roles'][0]->display_name}}
                            @endif
                            @endif
                            @endforeach
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>Contact Number</th>
                    <td>{{$user[0]->cnumber}}</td>
                </tr>
                <tr>
                    <th>DOB</th>
                    <td>{{$user[0]->dob}}</td>
                </tr>
                <tr>
                    <th>Remember Token</th>
                    <td>{{$user[0]->remember_token}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td><span style='color:@if($user[0]->status==1){{"green"}}@elseif($user[0]->status==2){{"red"}}@endif'>@if($user[0]->status==1){{"Active"}}@elseif($user[0]->status==2){{"Banned"}}@endif</span></td>
                </tr>
                <tr>
                    <th>Created At</th>
                    <td>{{$user[0]->created_at}}</td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
@stop