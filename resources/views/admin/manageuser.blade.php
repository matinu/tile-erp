@extends('layout.admintamplate')

@section('content')
<div class="well">
    <div class="box-title">
        <h2>
            {{$pageTitle}}
        </h2>
    </div>
    <div class="box-content nopadding">

        <?php if (Session::get('success_message') != "") { ?>
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>

                {{ Session::get('success_message') }}
            </div>
        <?php } ?>         
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact Number</th>
                    <th>DOB</th>
                    <th>Remember Token</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th style='width:206px'>Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($userdata as $user):
                    ?>
                    <tr>
                        <td><?php echo $user['users_id']; ?></td>
                        <td><?php echo $user['name']; ?></td>
                        <td><?php echo $user['email']; ?></td>
                        <td><?php echo $user['cnumber']; ?></td>
                        <td><?php echo $user['dob']; ?></td>
                        <td><?php echo $user['remember_token']; ?></td>
                        <td><?php
                            if ($user['status'] == 0) {
                                echo 'Registered';
                            } else if ($user['status'] == 1) {
                                echo 'Active';
                            } else if ($user['status'] == 2) {
                                echo 'Banned';
                            }
                            ?></td>
                        <td><?php echo $user['created_at']; ?></td>
                        <td>
                            <a href="{{URL::to('edit-user')}}/<?php echo $user['users_id']; ?>"><button type="submit" class="btn btn-success" id="banuser">Edit</button></a>
                            <a onClick="javascript: return confirm('Please confirm deletion');" href="{{URL::to('delete-user')}}/<?php echo $user['users_id']; ?>"><button type="submit" class="btn btn-danger">Delete</button></a>
                            <?php if ($user['status'] == 2) { ?>
                                <a onClick="javascript: return confirm('Please confirm unban');" href="{{URL::to('unban-user')}}/<?php echo $user['id']; ?>"><button type="submit" class="btn btn-primary">Un-Ban</button></a>
                            <?php } else { ?>
                                <a onClick="javascript: return confirm('Please confirm ban');" href="{{URL::to('ban-user')}}/<?php echo $user['id']; ?>"><button type="submit" class="btn btn-primary">Ban</button></a>
                            <?php } ?>
                            <!--<a onClick=\"javascript: return confirm('Please confirm deletion');\" href='delete.php?id=".$userid."'>x</a>-->
                            <!--<a onClick=\"javascript: return confirm('Please confirm deletion');\" href="delete-user/<?php // echo $user['id'];      ?>"><a href="delete-user/<?php // echo $user['id'];      ?>"><button type="submit" class="btn btn-danger">Delete</button></a>-->
                            <!--<button type="submit" class="btn btn-primary" id="banuser">Ban</button>-->
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
            </tbody>
        </table>
        @include('admin.pagination')
    </div>
    <input type="hidden" id="base_url" value="<?php echo url(); ?>">
    @stop