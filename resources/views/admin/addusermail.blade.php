<html>
    <head>
        <title>Validate Email</title>
    </head>
    <body>
        <p>Please follow the next link to activate your account:</p>
        <p><a href='{{$link}}' target='_blank'>Click Here</a></p>
        <p>If you haven't registered here please ignore this email.</p>
    </body>
</html>