ALTER TABLE `company` ADD `previous_name` VARCHAR( 255 ) NOT NULL COMMENT 'this will contain company previous name' AFTER `company_name` ;
ALTER TABLE `company_contact_person` ADD `contact_person_type` VARCHAR( 100 ) NOT NULL AFTER `person_email` ,
ADD `doappointment` DATE NOT NULL AFTER `contact_person_type` ,
ADD `doresignation` DATE NOT NULL AFTER `doappointment` ,
ADD `shareholder` VARCHAR( 100 ) NOT NULL AFTER `doresignation` ,
ADD `director` VARCHAR( 100 ) NOT NULL AFTER `shareholder` ,
ADD `secretary` VARCHAR( 100 ) NOT NULL AFTER `director` ;
