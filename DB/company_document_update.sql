-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2015 at 08:45 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_document`
--

CREATE TABLE IF NOT EXISTS `company_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `document_title` text NOT NULL,
  `document_description` text NOT NULL,
  `document_type` varchar(100) NOT NULL,
  `document_attached` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `company_document`
--

INSERT INTO `company_document` (`id`, `company_id`, `document_title`, `document_description`, `document_type`, `document_attached`) VALUES
(5, 1, 'deqwe', 'qweqwe', '', '1786Abshr.docx'),
(6, 2, 'deqwe', 'qweqwe', '', '1786Abshr.docx'),
(7, 1, 'da', 'ada', '', ''),
(8, 1, 'da', 'ada', '', ''),
(9, 1, 'da', 'ada', '', ''),
(10, 1, 'qeq', 'qwe', '', ''),
(11, 1, 'qeq', 'qwe', '', ''),
(12, 1, 'adad', 'adad', '', ''),
(13, 1, 'sf', 'sff', 'KYC', '23852grandc.jpeg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
