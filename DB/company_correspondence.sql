-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2015 at 06:57 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_correspondence`
--

CREATE TABLE IF NOT EXISTS `company_correspondence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `date_of_correspondence` datetime NOT NULL,
  `correspondence_title` varchar(100) NOT NULL,
  `correspondence_description` text NOT NULL,
  `correspondence_type` varchar(50) NOT NULL,
  `correspondence_content` text NOT NULL,
  `correspondence_document` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `company_correspondence`
--

INSERT INTO `company_correspondence` (`id`, `company_id`, `date_of_correspondence`, `correspondence_title`, `correspondence_description`, `correspondence_type`, `correspondence_content`, `correspondence_document`) VALUES
(1, 1, '2015-08-06 00:00:00', 'adad', 'adad', 'email', '<p>adad</p>', ''),
(2, 1, '2015-10-02 00:00:00', 'test', 'stt', 'document', '', '8820Abshr.docx'),
(3, 1, '2015-08-06 00:00:00', 'dad', 'adad', 'document', '', '25102Abshr.docx'),
(4, 1, '2015-08-20 00:00:00', 'teste', 'ad', 'email', '<p>Hi ponir,<br /> <br /> thanks for registering for an API authentication token. Please modify your client to use<br /> a header-field named "X-Auth-Token" with the underneath personal token as value.<br /> <br /> Your API token: 729b0444acea4c159afe980cd32445<wbr />de<br /> <br /> Check the code samples on <a href="http://www.football-data.org/code_samples" target="_blank" rel="noreferrer">http://www.football-data.org/<wbr />code_samples</a> to see how to implement that in several programming languages.<br /> <br /> Please verify your mail address by visiting the following link and tell me if you like to get notified for API updates via mail or not:<br /> <br /> <a href="http://www.football-data.org/client/home?confirmEmail=true&amp;urlToken=9c99a20e582d89b4f70860e9b38e9db3BCA729b0444acea4c159afe980cd32445de" target="_blank" rel="noreferrer">http://www.football-data.org/<wbr />client/home?confirmEmail=true&amp;<wbr />urlToken=<wbr />9c99a20e582d89b4f70860e9b38e9d<wbr />b3BCA729b0444acea4c159afe980cd<wbr />32445de</a><br /> <br /> If you face bugs, problems or any question concerning the API in general, feel free to ask!<br /> <br /> cheers,<br /> daniel</p>', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
