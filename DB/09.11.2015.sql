-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2015 at 09:00 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_account_opening`
--

CREATE TABLE IF NOT EXISTS `bank_account_opening` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_account_opening`
--

INSERT INTO `bank_account_opening` (`id`, `user_id`, `bank_name`, `filename`, `created_at`) VALUES
(3, 3, 'asia bank ltd', '1142680573F19.pdf', '2015-09-22 11:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `previous_name` varchar(255) NOT NULL COMMENT 'this will contain company previous name',
  `company_type` varchar(50) NOT NULL,
  `date_of_incorporation` datetime NOT NULL,
  `company_status` varchar(50) NOT NULL,
  `company_country` varchar(50) NOT NULL,
  `company_new_address` text NOT NULL,
  `company_old_address` text NOT NULL,
  `company_logo` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `previous_name`, `company_type`, `date_of_incorporation`, `company_status`, `company_country`, `company_new_address`, `company_old_address`, `company_logo`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(4, 'workspace infotech', 'workspace infotech', 'LLC', '2015-09-09 00:00:00', 'refurbished', 'Afghanistan', 'afdslp', 'dmkl', '1827245278logo.png', 3, '2015-09-14 03:50:06', 0, '0000-00-00 00:00:00'),
(5, 'Byte Raid', 'byte raid', 'Domestic', '2015-09-23 00:00:00', 'new', 'Bangladesh', 'dhaka', 'dhaka', '288798708laravel.png', 3, '2015-09-07 09:20:47', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_bank_statements`
--

CREATE TABLE IF NOT EXISTS `company_bank_statements` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `date_of_issue` date NOT NULL,
  `statement` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `uploaded_by` int(11) NOT NULL COMMENT 'user id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_bank_statements`
--

INSERT INTO `company_bank_statements` (`id`, `company_id`, `title`, `date_of_issue`, `statement`, `status`, `uploaded_by`, `created_at`) VALUES
(1, 4, 'test', '2015-09-17', '10621657386. Shareholders'' resolution on FS.docx', 1, 3, '2015-09-09 08:39:02'),
(2, 4, 'aiub', '2015-09-22', '12172269437. Board minutes Transfer of shares.doc', 1, 3, '2015-09-09 10:37:01'),
(3, 4, 'dfddf', '2015-09-09', '9164907154. Special Resolution Company Ceasing to ', 1, 3, '2015-09-11 06:17:17'),
(4, 4, 'dfddf', '2015-09-09', '21053719554. Special Resolution Company Ceasing to', 1, 3, '2015-09-11 06:17:27'),
(5, 4, 'dfddf', '2015-09-09', '5899400264. Special Resolution Company Ceasing to ', 1, 3, '2015-09-11 06:17:34'),
(6, 4, 'dfddf', '2015-09-09', '16712178474. Special Resolution Company Ceasing to', 1, 3, '2015-09-11 06:17:39'),
(7, 4, 'dfddf', '2015-09-09', '11870290554. Special Resolution Company Ceasing to', 1, 3, '2015-09-11 06:17:44'),
(8, 4, 'sda', '2015-09-08', '17697958326. Shareholders'' resolution on FS.docx', 1, 3, '2015-09-11 08:16:01'),
(9, 4, 'sda', '2015-09-08', '10611385716. Shareholders'' resolution on FS.docx', 1, 3, '2015-09-11 08:16:37'),
(10, 4, 'sda', '2015-09-08', '8100956366. Shareholders'' resolution on FS.docx', 1, 3, '2015-09-11 08:16:43'),
(11, 4, 'gfh', '2015-09-03', '340166327. Board minutes Transfer of shares.doc', 1, 3, '2015-09-12 10:34:41');

-- --------------------------------------------------------

--
-- Table structure for table `company_billing_configuration`
--

CREATE TABLE IF NOT EXISTS `company_billing_configuration` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `billing_period` varchar(100) NOT NULL,
  `hourly_rate` double NOT NULL,
  `cycle_start_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_billing_configuration`
--

INSERT INTO `company_billing_configuration` (`id`, `company_id`, `billing_period`, `hourly_rate`, `cycle_start_date`) VALUES
(2, 4, 'monthly', 123, '2015-07-28 00:00:00'),
(3, 5, 'monthly', 45, '2015-08-30 00:00:00'),
(4, 4, 'weekly', 12, '2015-09-03 00:00:00'),
(5, 4, 'monthly', 16, '2015-09-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_compliance_report`
--

CREATE TABLE IF NOT EXISTS `company_compliance_report` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `document_attachment` varchar(100) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_compliance_report`
--

INSERT INTO `company_compliance_report` (`id`, `company_id`, `title`, `description`, `document_attachment`, `uploaded_by`, `created_at`) VALUES
(1, 5, 'test1', '<p>this <strong>is <em>testing </em></strong><em>for </em>the first time</p>', '28202Tile-ErpRequirement (1).docx', 3, '2015-09-09 07:54:30'),
(2, 4, 'abc', '<p>test discription</p>', '16166144406. Shareholders'' resolution on FS.docx', 3, '2015-09-09 10:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_contact_person`
--

CREATE TABLE IF NOT EXISTS `company_contact_person` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `person_designation` varchar(50) NOT NULL,
  `contact_person_type` varchar(100) NOT NULL,
  `person_contact_number` varchar(50) NOT NULL,
  `person_email` varchar(50) NOT NULL,
  `doresignation` date NOT NULL,
  `doappointment` date NOT NULL,
  `director` varchar(100) NOT NULL,
  `shareholder` varchar(100) NOT NULL,
  `secretary` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_contact_person`
--

INSERT INTO `company_contact_person` (`id`, `company_id`, `person_name`, `person_designation`, `contact_person_type`, `person_contact_number`, `person_email`, `doresignation`, `doappointment`, `director`, `shareholder`, `secretary`) VALUES
(3, 2, 'ponir bhi', 'software engin', '', '016555889566', 'ponir@workspaceit.com', '0000-00-00', '0000-00-00', '', '', ''),
(4, 3, 'shahdat', 'se', '', '01614142424', 'ratonshahadat', '0000-00-00', '0000-00-00', '', '', ''),
(9, 5, 'jamil uddin', 'se', '5', '0161414242424', 'jamil@gmail.com', '2015-09-09', '2015-09-10', 'sdf', 'sdf', 'dsaf');

-- --------------------------------------------------------

--
-- Table structure for table `company_correspondence`
--

CREATE TABLE IF NOT EXISTS `company_correspondence` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `date_of_correspondence` datetime NOT NULL,
  `correspondence_title` varchar(100) NOT NULL,
  `correspondence_description` text NOT NULL,
  `correspondence_type` varchar(50) NOT NULL,
  `received_by` varchar(100) NOT NULL,
  `correspondence_content` text NOT NULL,
  `correspondence_document` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_credit_invoice`
--

CREATE TABLE IF NOT EXISTS `company_credit_invoice` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `invoice_date` datetime NOT NULL,
  `invoice_title` varchar(100) NOT NULL,
  `invoice_description` text NOT NULL,
  `invoice_hours` int(11) NOT NULL,
  `invoice_status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_credit_invoice`
--

INSERT INTO `company_credit_invoice` (`id`, `company_id`, `invoice_date`, `invoice_title`, `invoice_description`, `invoice_hours`, `invoice_status`) VALUES
(4, 5, '2015-09-04 00:00:00', 'fds', 'sdf', 5, 'pending'),
(5, 5, '2015-09-02 00:00:00', 'fds', 'sdf', 5, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `company_document`
--

CREATE TABLE IF NOT EXISTS `company_document` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `document_title` text NOT NULL,
  `document_description` text NOT NULL,
  `document_type` varchar(100) NOT NULL,
  `document_attached` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_document`
--

INSERT INTO `company_document` (`id`, `company_id`, `document_title`, `document_description`, `document_type`, `document_attached`, `created_at`) VALUES
(3, 2, 'title ', 'lorem ipsome dolor', '', '787084282default_language.sql', '0000-00-00 00:00:00'),
(4, 3, 'nai', 'nai', '', '17346909GLG.txt', '0000-00-00 00:00:00'),
(5, 3, 'dfd', 'dsfdfs', '', '114550853348s.jpg', '0000-00-00 00:00:00'),
(6, 3, 'dfd', 'dsfdfs', '', '1994467317348s.jpg', '0000-00-00 00:00:00'),
(8, 5, 'test', 'test', '', '1608731085event_manager_database.pdf', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_financial_statements`
--

CREATE TABLE IF NOT EXISTS `company_financial_statements` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `issue_date` date NOT NULL,
  `filename` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_financial_statements`
--

INSERT INTO `company_financial_statements` (`id`, `company_id`, `uploaded_by`, `client_name`, `issue_date`, `filename`, `bank_name`, `created_at`) VALUES
(1, 4, 3, 'shahadat', '2015-09-16', '32663Tile-ErpRequirement (1).docx', 'bangk', '2015-09-29 09:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `company_incorporation`
--

CREATE TABLE IF NOT EXISTS `company_incorporation` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_incorporation`
--

INSERT INTO `company_incorporation` (`id`, `company_id`, `title`, `description`, `filename`, `uploaded_by`, `created_at`) VALUES
(2, 4, 'tilete', '<p>test test</p>', '3361516335. Director''s resolution on FS.docx', 3, '2015-09-09 10:52:07'),
(3, 4, 'ddffd', '<p>dfsdfsfds</p>', '15498786873. Board minutes Restoration of Company.docx', 3, '2015-09-11 04:54:27'),
(4, 4, 'ddffd', '<p>dfsdfsfds</p>', '20361277523. Board minutes Restoration of Company.docx', 3, '2015-09-11 04:54:38'),
(5, 4, 'ddffd', '<p>dfsdfsfds</p>', '772869253. Board minutes Restoration of Company.docx', 3, '2015-09-11 04:54:45'),
(6, 4, 'ddffd', '<p>dfsdfsfds</p>', '4258718253. Board minutes Restoration of Company.docx', 3, '2015-09-11 04:55:31'),
(7, 4, 'xddf', '<p>fdfdf</p>', '15876840145. Director''s resolution on FS.docx', 3, '2015-09-11 04:56:05');

-- --------------------------------------------------------

--
-- Table structure for table `company_tax_return`
--

CREATE TABLE IF NOT EXISTS `company_tax_return` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `taxation_date` date NOT NULL,
  `filename` varchar(255) NOT NULL,
  `a_c_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_tax_return`
--

INSERT INTO `company_tax_return` (`id`, `company_id`, `uploaded_by`, `client_name`, `taxation_date`, `filename`, `a_c_name`, `created_at`) VALUES
(1, 4, 3, 'df', '2015-08-31', '20635Tile-ErpRequirement (1).docx', 'df', '2015-09-29 09:30:55');

-- --------------------------------------------------------

--
-- Table structure for table `company_template`
--

CREATE TABLE IF NOT EXISTS `company_template` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `type` int(11) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=260 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `invoice_id` int(11) NOT NULL,
  `client` int(11) NOT NULL COMMENT 'clients table id ',
  `invoice_number` varchar(45) NOT NULL,
  `issue_date` date NOT NULL,
  `po_number` varchar(45) DEFAULT NULL,
  `invoice_total` double NOT NULL,
  `discount` double NOT NULL,
  `terms` text,
  `note` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 for delete 1 for active'
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `client`, `invoice_number`, `issue_date`, `po_number`, `invoice_total`, `discount`, `terms`, `note`, `created_at`, `updated_at`, `status`) VALUES
(44, 2, '002-31082015-001', '2015-08-29', '45654', 503864, 0, 'dalfk;;', 'dfajkl', '2015-08-31 08:15:42', '2015-08-31 08:19:54', 1),
(45, 2, '002-31082015-045', '2015-08-12', '45654', 144000, 0, 'dfasdfafda', 'dfsadfasfdsa', '2015-08-31 08:21:46', '2015-08-31 08:21:46', 1),
(46, 2, '002-31082015-046', '2015-07-10', '52412', 2405430, 0, 'addaf', 'fdsdsfsdf', '2015-08-31 08:23:33', '2015-08-31 08:23:33', 1),
(48, 3, '003-02092015-047', '2015-09-10', '324', 186792, 0, 'fgfd', 'ffg', '2015-09-02 04:03:01', '2015-09-02 04:03:01', 1),
(49, 3, '003-02092015-049', '2015-09-17', '7887', 13572, 0, 'jk', 'jk', '2015-09-02 04:57:41', '2015-09-02 04:57:41', 1),
(50, 4, '004-02092015-050', '2015-09-10', '4561', 32000, 0, 'dfdfs', 'dsfdfs', '2015-09-02 06:43:36', '2015-09-02 06:47:56', 1),
(51, 4, '004-02092015-051', '2015-09-05', '5454', 252, 0, '', '', '2015-09-02 06:46:38', '2015-09-02 06:46:38', 1),
(52, 4, '004-02092015-052', '2015-09-11', '65', 96.48, 305.52, '6', '6767', '2015-09-02 06:54:38', '2015-09-02 06:54:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE IF NOT EXISTS `invoice_items` (
  `invoice_details_id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL COMMENT 'Invocie table id',
  `item` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `unit_cost` double NOT NULL,
  `quantity` double NOT NULL,
  `total` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_items`
--

INSERT INTO `invoice_items` (`invoice_details_id`, `invoice`, `item`, `description`, `unit_cost`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(75, 44, 'sadf', 'dsaf', 12, 1200, 14400, '2015-08-31 08:15:42', '2015-08-31 08:15:42'),
(76, 44, 'sddfs', 'dfsdfs', 12, 122, 1464, '2015-08-31 08:15:42', '2015-08-31 08:15:42'),
(77, 44, 'dfdf', 'dfg', 122, 4000, 488000, '2015-08-31 08:16:10', '2015-08-31 08:19:54'),
(78, 45, 'dfasadfs', 'dfasfd', 120, 1200, 144000, '2015-08-31 08:21:46', '2015-08-31 08:21:46'),
(79, 46, 'dfsfdsdfs', 'dfsdfs', 54, 44545, 2405430, '2015-08-31 08:23:33', '2015-08-31 08:23:33'),
(81, 48, 'fdf', 'ffg', 43, 4344, 186792, '2015-09-02 04:03:02', '2015-09-02 04:03:02'),
(82, 49, 'jh', 'jk', 78, 87, 6786, '2015-09-02 04:57:41', '2015-09-02 04:57:41'),
(83, 49, 'hj', 'jh', 78, 87, 6786, '2015-09-02 04:57:41', '2015-09-02 04:57:41'),
(84, 50, 'sdds', 'dsdfs', 32, 1000, 32000, '2015-09-02 06:43:36', '2015-09-02 06:43:36'),
(85, 51, 'ff', 'fgfg', 12, 21, 252, '2015-09-02 06:46:38', '2015-09-02 06:46:38'),
(86, 52, '65', '67', 6, 67, 402, '2015-09-02 06:54:38', '2015-09-02 06:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `kyc`
--

CREATE TABLE IF NOT EXISTS `kyc` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `filename` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kyc`
--

INSERT INTO `kyc` (`id`, `name`, `country`, `description`, `filename`, `created_at`) VALUES
(7, 'test', 'Algeria', 'test', '8876newupdate for company and contact.sql', '2015-11-09 07:43:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_26_051513_entrust_setup_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `payment_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `method` varchar(100) NOT NULL,
  `payment_date` date NOT NULL,
  `note` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`payment_id`, `invoice_id`, `amount`, `method`, `payment_date`, `note`, `created_at`, `updated_at`) VALUES
(3, 44, 5000, 'Check', '2015-10-28', 'ghhghghg', '2015-09-01 04:05:21', '2015-09-01 04:05:21'),
(4, 48, 400, 'Cash', '2015-09-02', 'uuyuyuy', '2015-09-02 05:07:31', '2015-09-02 05:07:31'),
(5, 51, 252, 'Cash', '2015-10-02', 'hjhg', '2015-09-02 06:51:37', '2015-09-02 06:51:37'),
(6, 50, 32000, 'Cash', '2015-10-02', 'ffggf', '2015-09-02 06:52:17', '2015-09-02 06:52:17'),
(8, 52, 2, 'Cash', '2015-01-10', '', '2015-09-02 06:55:53', '2015-09-02 07:08:33'),
(10, 52, 3, 'Cash', '1970-01-01', 'hh', '2015-09-02 06:58:33', '2015-09-02 07:05:26'),
(11, 52, 43, 'Cash', '2015-09-10', 'fsdfdsfdsa', '2015-09-02 07:00:04', '2015-09-02 07:00:04'),
(12, 52, 12, 'Cash', '2015-09-01', '', '2015-09-02 07:01:06', '2015-09-02 07:01:06'),
(13, 52, 1, 'Cash', '2015-09-02', 'vbn', '2015-09-02 07:02:53', '2015-09-02 07:02:53');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-post', 'Create Posts', 'create new blog posts', '2015-08-26 07:19:40', '2015-08-26 07:19:40'),
(2, 'edit-user', 'Edit Users', 'edit existing users', '2015-08-26 07:19:41', '2015-08-26 07:19:41'),
(3, 'create-company', 'Create Company', '', '2015-08-28 05:08:29', '2015-08-28 05:08:29'),
(4, 'list-company', 'List Company', '', '2015-08-28 05:08:30', '2015-08-28 05:08:30'),
(5, 'create-user', 'Create User', '', '2015-08-28 05:12:16', '2015-08-28 05:12:16'),
(6, 'login', 'Login', '', '2015-08-28 06:10:04', '2015-08-28 06:10:04'),
(7, 'login-check', 'Login Check', 'fddf', '2015-08-28 06:10:21', '2015-08-28 06:48:52'),
(9, 'list-user', 'List User', '', '2015-08-28 06:35:16', '2015-08-28 06:35:16'),
(11, 'save-company', 'Save Company', '', '2015-08-28 06:40:47', '2015-08-28 06:40:47'),
(19, 'validateemail', 'Validateemail', 'test', '2015-08-28 07:16:37', '2015-08-28 07:22:14'),
(21, 'details-company', 'Details Company', '', '2015-08-31 23:16:50', '2015-08-31 23:16:50'),
(22, 'delete-company', 'Delete Company', '', '2015-09-01 04:20:49', '2015-09-01 04:20:49'),
(23, 'list-permission', 'List Permission', '', '2015-09-01 06:06:04', '2015-09-01 06:06:04'),
(24, 'list-role', 'List Role', '', '2015-09-01 06:06:19', '2015-09-01 06:06:19'),
(25, 'set-permission', 'Set Permission', '', '2015-09-01 06:08:27', '2015-09-01 06:08:27'),
(26, 'dashboard', 'Dashboard', '', '2015-09-01 06:13:28', '2015-09-01 06:13:28'),
(27, 'create-role', 'Create Role', '', '2015-09-01 22:59:09', '2015-09-01 22:59:09'),
(28, 'view-role-permission', 'View Role Permission', '', '2015-09-01 22:59:27', '2015-09-01 22:59:27'),
(29, 'set-role', 'Set Role', '', '2015-09-01 23:00:53', '2015-09-01 23:00:53'),
(30, 'view-user-role', 'View User Role', '', '2015-09-01 23:00:58', '2015-09-01 23:00:58'),
(32, 'edit-role', 'Edit Role', '', '2015-09-01 23:50:38', '2015-09-01 23:50:38'),
(33, 'delete-role-permission', 'Delete Role Permission', '', '2015-09-01 23:57:17', '2015-09-01 23:57:17'),
(34, 'add-document', 'Add Document', '', '2015-09-02 00:36:36', '2015-09-02 00:36:36'),
(35, '', '', '', '2015-09-02 06:00:58', '2015-09-02 06:00:58'),
(36, 'edit-company', 'Edit Company', '', '2015-09-02 06:02:00', '2015-09-02 06:02:00'),
(37, 'delete-person', 'Delete Person', '', '2015-09-02 06:03:43', '2015-09-02 06:03:43'),
(38, 'add-company-contact-person', 'Add Company Contact Person', '', '2015-09-02 06:04:32', '2015-09-02 06:04:32'),
(39, 'update-company', 'Update Company', '', '2015-09-02 06:07:15', '2015-09-02 06:07:15'),
(40, 'delete-role', 'Delete Role', '', '2015-09-04 04:42:36', '2015-09-04 04:42:36'),
(41, 'create-billing', 'Create Billing', '', '2015-09-04 05:51:15', '2015-09-04 05:51:15'),
(42, 'list-billing', 'List Billing', '', '2015-09-04 05:51:22', '2015-09-04 05:51:22'),
(43, 'ban-user', 'Ban User', '', '2015-09-04 06:12:51', '2015-09-04 06:12:51'),
(44, 'billing-info-details', 'Billing Info Details', '', '2015-09-06 22:18:41', '2015-09-06 22:18:41'),
(45, 'edit-billing', 'Edit Billing', '', '2015-09-06 22:19:14', '2015-09-06 22:19:14'),
(46, 'unban-user', 'Unban User', '', '2015-09-06 23:05:28', '2015-09-06 23:05:28'),
(47, 'edit-user-role', 'Edit User Role', '', '2015-09-06 23:06:05', '2015-09-06 23:06:05'),
(48, 'edit-credit-invoice', 'Edit Credit Invoice', '', '2015-09-07 08:11:05', '2015-09-07 08:11:05'),
(49, 'update-credit-invoice', 'Update Credit Invoice', '', '2015-09-07 08:11:10', '2015-09-07 08:11:10'),
(50, 'update-billing', 'Update Billing', '', '2015-09-07 08:11:30', '2015-09-07 08:11:30'),
(51, 'add-company-credit-invoice', 'Add Company Credit Invoice', '', '2015-09-07 08:12:11', '2015-09-07 08:12:11'),
(52, 'save-billing', 'Save Billing', '', '2015-09-07 08:13:46', '2015-09-07 08:13:46'),
(53, 'billing-pdf', 'Billing Pdf', '', '2015-09-07 08:19:08', '2015-09-07 08:19:08'),
(54, 'edit-role-permission', 'Edit Role Permission', '', '2015-09-08 01:21:08', '2015-09-08 01:21:08'),
(55, 'add-tamplate', 'Add Tamplate', '', '2015-09-08 01:46:03', '2015-09-08 01:46:03'),
(56, 'add-statement', 'Add Statement', '', '2015-09-08 06:27:11', '2015-09-08 06:27:11'),
(57, 'delete-template', 'Delete Template', '', '2015-09-08 06:41:44', '2015-09-08 06:41:44'),
(58, 'delete-statement', 'Delete Statement', '', '2015-09-08 06:45:37', '2015-09-08 06:45:37'),
(59, 'add-incorporation', 'Add Incorporation', '', '2015-09-09 04:24:18', '2015-09-09 04:24:18'),
(60, 'delete-incorporation', 'Delete Incorporation', '', '2015-09-09 04:26:53', '2015-09-09 04:26:53'),
(61, 'add-compliance', 'Add Compliance', '', '2015-09-09 04:31:00', '2015-09-09 04:31:00'),
(62, 'add-correspondence', 'Add Correspondence', '', '2015-09-09 04:40:16', '2015-09-09 04:40:16'),
(63, 'delete-correspondence', 'Delete Correspondence', '', '2015-09-09 04:40:35', '2015-09-09 04:40:35'),
(64, 'list-user/', 'List-user:', '', '2015-09-10 04:54:41', '2015-09-10 04:54:41'),
(65, 'statements-view', 'Statements View', '', '2015-09-10 06:22:01', '2015-09-10 06:22:01'),
(66, 'incorporation-view', 'Incorporation View', '', '2015-09-10 23:19:25', '2015-09-10 23:19:25'),
(67, 'compliance-view', 'Compliance View', '', '2015-09-10 23:26:08', '2015-09-10 23:26:08'),
(68, 'invoices-view', 'Invoices View', '', '2015-09-10 23:26:28', '2015-09-10 23:26:28'),
(69, 'contact-person-view', 'Contact Person View', '', '2015-09-10 23:26:38', '2015-09-10 23:26:38'),
(70, 'document-view', 'Document View', '', '2015-09-10 23:26:43', '2015-09-10 23:26:43'),
(71, 'correspondance-view', 'Correspondance View', '', '2015-09-10 23:26:48', '2015-09-10 23:26:48'),
(72, 'templates-view', 'Templates View', '', '2015-09-10 23:27:31', '2015-09-10 23:27:31'),
(73, 'statements-view/4/', 'Statements-view:4:', '', '2015-09-11 00:56:46', '2015-09-11 00:56:46'),
(74, 'statements-view/4', 'Statements-view:4', '', '2015-09-11 01:57:18', '2015-09-11 01:57:18'),
(75, 'correspondance-view/4', 'Correspondance-view:4', '', '2015-09-11 22:50:21', '2015-09-11 22:50:21'),
(76, 'document-view/4', 'Document-view:4', '', '2015-09-11 22:50:26', '2015-09-11 22:50:26'),
(77, 'contact-person-view/4', 'Contact-person-view:4', '', '2015-09-11 22:50:34', '2015-09-11 22:50:34'),
(78, 'invoices-view/4', 'Invoices-view:4', '', '2015-09-11 22:50:40', '2015-09-11 22:50:40'),
(79, 'incorporation-view/4', 'Incorporation-view:4', '', '2015-09-11 22:50:51', '2015-09-11 22:50:51'),
(80, 'templates-view/4', 'Templates-view:4', '', '2015-09-11 23:07:26', '2015-09-11 23:07:26'),
(81, 'compliance-view/4', 'Compliance-view:4', '', '2015-09-11 23:07:33', '2015-09-11 23:07:33'),
(82, 'delete-document', 'Delete Document', '', '2015-09-12 04:17:47', '2015-09-12 04:17:47'),
(83, 'profile', 'Profile', '', '2015-09-15 05:31:21', '2015-09-15 05:31:21'),
(84, 'bank-account-open', 'Bank Account Open', '', '2015-09-21 23:38:18', '2015-09-21 23:38:18'),
(85, 'list-bank-account', 'List Bank Account', '', '2015-09-22 00:36:25', '2015-09-22 00:36:25'),
(86, 'create-kyc', 'Create Kyc', '', '2015-09-22 02:06:15', '2015-09-22 02:06:15'),
(87, 'list-kyc', 'List Kyc', '', '2015-09-22 02:09:31', '2015-09-22 02:09:31'),
(88, 'delete-kyc', 'Delete Kyc', '', '2015-09-22 02:18:32', '2015-09-22 02:18:32'),
(89, 'delete-bank-account', 'Delete Bank Account', '', '2015-09-22 02:34:38', '2015-09-22 02:34:38'),
(90, 'add-tax-return', 'Add Tax Return', '', '2015-09-22 04:43:48', '2015-09-22 04:43:48'),
(91, 'delete-tax-return', 'Delete Tax Return', '', '2015-09-22 04:58:26', '2015-09-22 04:58:26'),
(92, 'tax-return-view/4', 'Tax-return-view:4', '', '2015-09-22 05:05:25', '2015-09-22 05:05:25'),
(93, 'financial-statements-view/4', 'Financial-statements-view:4', '', '2015-09-29 02:30:08', '2015-09-29 02:30:08'),
(94, 'add-financial-statements', 'Add Financial Statements', '', '2015-09-29 03:39:23', '2015-09-29 03:39:23'),
(95, 'delete-credit-invoice', 'Delete Credit Invoice', '', '2015-11-09 01:36:22', '2015-11-09 01:36:22');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(9, 6),
(1, 10),
(2, 10),
(3, 10),
(4, 10),
(5, 10),
(6, 10),
(7, 10),
(9, 10),
(11, 10),
(19, 10),
(21, 10),
(22, 10),
(23, 10),
(24, 10),
(25, 10),
(26, 10),
(27, 10),
(28, 10),
(29, 10),
(30, 10),
(32, 10),
(33, 10),
(34, 10),
(35, 10),
(36, 10),
(37, 10),
(38, 10),
(39, 10),
(40, 10),
(41, 10),
(42, 10),
(43, 10),
(44, 10),
(45, 10),
(46, 10),
(47, 10),
(55, 10);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(6, 'project managers', 'Project manager', 'this is project managers', '2015-08-27 05:15:01', '2015-08-27 06:21:18'),
(10, 'owner', 'Owners', 'test', '2015-09-06 23:06:54', '2015-09-06 23:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(3, 10),
(4, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'shahadat Hossain', 'ratonshahadat@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2015-08-28 07:16:29', '2015-08-28 07:16:29'),
(4, 'ghf', 'ghgff@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2015-09-06 23:04:53', '2015-09-06 23:04:53');

-- --------------------------------------------------------

--
-- Table structure for table `users_details`
--

CREATE TABLE IF NOT EXISTS `users_details` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL COMMENT 'users table id',
  `cnumber` varchar(20) NOT NULL COMMENT 'users contact number',
  `dob` date NOT NULL COMMENT 'users date of birth',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_details`
--

INSERT INTO `users_details` (`id`, `users_id`, `cnumber`, `dob`, `status`) VALUES
(3, 3, '01614142424', '2015-09-04', 1),
(4, 4, '530402', '2015-09-03', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank_account_opening`
--
ALTER TABLE `bank_account_opening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `company_bank_statements`
--
ALTER TABLE `company_bank_statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_billing_configuration`
--
ALTER TABLE `company_billing_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_compliance_report`
--
ALTER TABLE `company_compliance_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_contact_person`
--
ALTER TABLE `company_contact_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_correspondence`
--
ALTER TABLE `company_correspondence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_credit_invoice`
--
ALTER TABLE `company_credit_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_document`
--
ALTER TABLE `company_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_financial_statements`
--
ALTER TABLE `company_financial_statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_incorporation`
--
ALTER TABLE `company_incorporation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_tax_return`
--
ALTER TABLE `company_tax_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_template`
--
ALTER TABLE `company_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`invoice_details_id`),
  ADD KEY `fk_invoice_details_invoice1` (`invoice`);

--
-- Indexes for table `kyc`
--
ALTER TABLE `kyc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_details`
--
ALTER TABLE `users_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank_account_opening`
--
ALTER TABLE `bank_account_opening`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_bank_statements`
--
ALTER TABLE `company_bank_statements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `company_billing_configuration`
--
ALTER TABLE `company_billing_configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_compliance_report`
--
ALTER TABLE `company_compliance_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company_contact_person`
--
ALTER TABLE `company_contact_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `company_correspondence`
--
ALTER TABLE `company_correspondence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company_credit_invoice`
--
ALTER TABLE `company_credit_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_document`
--
ALTER TABLE `company_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `company_financial_statements`
--
ALTER TABLE `company_financial_statements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company_incorporation`
--
ALTER TABLE `company_incorporation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `company_tax_return`
--
ALTER TABLE `company_tax_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company_template`
--
ALTER TABLE `company_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `invoice_details_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `kyc`
--
ALTER TABLE `kyc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_details`
--
ALTER TABLE `users_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
